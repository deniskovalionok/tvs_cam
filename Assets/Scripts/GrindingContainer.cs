﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using Assets.Scripts.DataStructures;
using Assets.Scripts.Interfaces;
using System.Collections;


namespace Assets.Scripts
{
    public class GrindingContainer : MonoBehaviour, IController
    {
        private ContainerData previousData;

        public Transform containerObject;        

        private Vector3 originPos;

        private bool IsTranslationSelfControlling = true;
        private bool IsRotationSelfControlling = true;

        private bool IsAttachedToManipulator;
        private bool IsAttachedToTitler;

        private float previousRealTime;

        private float rotateValue = 0.1f;
        private Quaternion target;
        private Quaternion originRotation;


        private void Start()
        {
            originPos = containerObject.localPosition;
            previousRealTime = Time.realtimeSinceStartup;
            targetPos = containerObject.localPosition;

            if (containerObject != null)
                originRotation = containerObject.GetChild(0).localRotation;

            //_test = new ContainerData("", "", "", "", "", "", "", "", -10);

            //StartCoroutine(UpdateData());
        }

        private IEnumerator UpdateData()
        {

            //Debug.Log(this.name);

            _test.YCoord = 0;
            _test.YVelocity = 0;
            //_test.ZVelocity = 20;
            _test.XCoord = 0;
            _test.XVelocity = 0;
            _test.AxePosition = 0;
            _test.ZCoord = 0;
            _test.ZVelocity = 0;
            SetData(_test);
            //_test.RotatingVelocity = 0.1f;            
            while (true)
            {

                Debug.Log(this.name);

                //if (_test.YCoord < 1728)
                //{
                //    _test.YCoord += _test.YVelocity/10;
                //}
                //else
                //{
                //    _test.ZVelocity = 0;
                //}

                if (_test.AxePosition <= 1)
                {
                    _test.AxePosition += 0.01f;
                }
                else
                {
                    _test.RotatingVelocity = 0;
                    DataUpdatedHandler();
                    break;
                }


                DataUpdatedHandler();
                yield return new WaitForSecondsRealtime(0.1f);
            }
        }
       


        private ContainerData _test;

        private void Update()
        {


            //UpdateControllingParameters();



            //if (Input.GetKeyDown(KeyCode.Space))
            //{

            //    _test.XVelocity *= (-1);
            //    //_test.GrabAxePosition = 0.99f;
            //    //b = true;
            //    //_fromGrabToHorizOffset = HorizontalDrive.localPosition - GrabDrive.localPosition;
            //    //Debug.Log(_test.RotateVel);
            //}

            //if (Input.GetKeyDown(KeyCode.K))
            //{
            //    _test.XVelocity = 0;
            //}
        }

        private Vector3 targetPos;

        public void SetData(Data dataToSet)
        {
            ContainerData actualData = (ContainerData)dataToSet;
            //Debug.Log($"{actualData.YCoord}");
            previousData = actualData;
            //firstContainerObj.localPosition = new Vector3((float)previousData.XCoord / 1000, firstContainerObj.localPosition.y, firstContainerObj.localPosition.z);
            //target = originRotation * Quaternion.AngleAxis(180 * (float)previousData.AxePosition, Vector3.down);
            //previousRealTime = Time.realtimeSinceStartup;
            //targetPos = firstContainerObj.localPosition + new Vector3((float)previousData.XVelocity/10000, 0, 0);
        }



        public void UpdateControllingParameters()
        {

            float r = Time.realtimeSinceStartup;

            //Translate and rotate object by speed

            if (containerObject != null)
            {
                //Debug.Log(this.name);

                TranslateObject(containerObject, r, target);
            }

            previousRealTime = r;

        }

        private void TranslateObject(Transform container, float r, Quaternion target)
        {

            //Debug.Log(previousData.AxePosition);
            //Debug.Log(_test.AxePosition);
            //Debug.Log(_test == previousData);

            //firstContainerObj.localPosition = new Vector3(Mathf.Lerp(firstContainerObj.localPosition.x, previousData.XCoord / 1000, Time.deltaTime), Mathf.Lerp(firstContainerObj.localPosition.y, previousData.ZCoord / 1000, Time.deltaTime), Mathf.Lerp(firstContainerObj.localPosition.z, previousData.YCoord / 1000, Time.deltaTime));
            //if(Vector3.Distance(targetPos, firstContainerObj.localPosition)>0.001)
            //{

            //firstContainerObj.localPosition = Vector3.MoveTowards(firstContainerObj.localPosition, new Vector3((float)previousData.XCoord/1000, firstContainerObj.localPosition.y, firstContainerObj.localPosition.z), (Mathf.Abs((float)previousData.XVelocity) / 1000) * (r - previousRealTime));

            if (previousData == null)
                return;

            //Debug.Log($"{this.name} has not null data");

            if (IsTranslationSelfControlling)
            {

                if (GameCore.Instance.IsLerp)
                {
                    container.localPosition = new Vector3(Mathf.Lerp(container.localPosition.x, (float)previousData.XCoord / 1000, Time.deltaTime), Mathf.Lerp(container.localPosition.y, originPos.y + (float)previousData.ZCoord / 1000, Time.deltaTime), Mathf.Lerp(container.localPosition.z, (float)previousData.YCoord / 1000, Time.deltaTime));
                }
                else
                {
                    container.localPosition = new Vector3(Mathf.MoveTowards(container.localPosition.x, (float)previousData.XCoord / 1000, (Mathf.Abs((float)previousData.XVelocity) / 1000) * (r - previousRealTime)),
                            Mathf.MoveTowards(container.localPosition.y, originPos.y + (float)previousData.ZCoord / 1000, (Mathf.Abs((float)previousData.ZVelocity) / 1000) * (r - previousRealTime)),
                            Mathf.MoveTowards(container.localPosition.z, (float)previousData.YCoord / 1000, (Mathf.Abs((float)previousData.YVelocity) / 1000) * (r - previousRealTime)));
                }

            }


            if (IsRotationSelfControlling)
            {
                //Debug.Log("Self controlling");

                if (container.GetChild(0).localRotation != target)
                {
                    container.GetChild(0).localRotation = Quaternion.RotateTowards(container.GetChild(0).localRotation, target, 180 * /*rotateValue*/ Mathf.Abs((float)previousData.RotatingVelocity) * (r - previousRealTime));
                }

            }

            if (previousData.XVelocity == 0)
            {
                //Debug.Log("kfkfk");
                container.localPosition = new Vector3((float)previousData.XCoord / 1000, container.localPosition.y, container.localPosition.z);
            }
            if (previousData.YVelocity == 0)
            {
                container.localPosition = new Vector3(container.localPosition.x, container.localPosition.y, (float)previousData.YCoord / 1000);
            }
            if (previousData.ZVelocity == 0)
            {
                container.localPosition = new Vector3(container.localPosition.x, originPos.y + (float)previousData.ZCoord / 1000, container.localPosition.z);
            }

            if (previousData.RotatingVelocity == 0)
            {
                //Debug.Log("Rot valocity is 0: " + this.name);

                container.GetChild(0).localRotation = target;
            }
            //if (secondContainerObj != null)
            //{
            //    secondContainerObj.localPosition = new Vector3(Mathf.Lerp(secondContainerObj.localPosition.x, previousData.XCoord / 1000, Time.deltaTime), Mathf.Lerp(secondContainerObj.localPosition.y, previousData.ZCoord / 1000, Time.deltaTime), Mathf.Lerp(secondContainerObj.localPosition.z, previousData.YCoord / 1000, Time.deltaTime));
            //}
            //if (thirdContainerObj != null)
            //{
            //    thirdContainerObj.localPosition = new Vector3(Mathf.Lerp(thirdContainerObj.localPosition.x, previousData.XCoord / 1000, Time.deltaTime), Mathf.Lerp(thirdContainerObj.localPosition.y, previousData.ZCoord / 1000, Time.deltaTime), Mathf.Lerp(thirdContainerObj.localPosition.z, previousData.YCoord / 1000, Time.deltaTime));
            //}


        }

        private void RotateObject()
        {

            if (IsRotationSelfControlling)
            {
                //Rotating logic
            }



        }

        public void DataUpdatedHandler()
        {            
            target = originRotation * Quaternion.AngleAxis(180 * (float)previousData.AxePosition, Vector3.down);
            previousRealTime = Time.realtimeSinceStartup;
        }
    }
}
