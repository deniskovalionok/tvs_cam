﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Assets.Scripts.Interfaces;

namespace Assets.Scripts
{
    public class TVSMapper
    {
        public static Dictionary<string, int> entitiesNumbers = new Dictionary<string, int>
        {
            {"View", 1},
            {"DockingNode", 2 },
            {"Door", 3 },
            {"LiveRoll", 4 },
            {"MagazinDisassemble", 5 },
            {"TvelPositioning", 6 },
            {"TvelBeamAssemble", 7 },
            {"Autooperator", 8 },
            {"Titler", 9 },
            {"HeadShorer", 10 },
            {"CoordManipulator", 11 },
            {"TailiDettaching", 12 },
            {"CoordFacility", 13 },
            {"TvelPusher", 14 },
            {"TvelPuller", 15 },
            {"TailiTransporter", 16 },
            {"TransportFacility", 17 },
            {"TruckTransmiting", 18 },
            {"CraneManipulator", 19 },

        };

        public static Dictionary<int, IController> numberTypes = new Dictionary<int, IController>();
    }
}
