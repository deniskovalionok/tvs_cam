﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using Assets.Scripts.Interfaces;

namespace Assets.Scripts
{
    public class StorageUnitObjRefsHandler : RefsHandler
    {

        public StorageTruck storageTruck;
        public ReloadingDevice reloadingUnit;
        public ViewSelector viewController;
        public LightController lightController;


        public override IController GetControllerByEntityID(int entityID)
        {
            return StorageUnitMapper.numberTypes[entityID];
        }

        public override IEnumerable<IController> GetControllers()
        {
            List<IController> controllers = new List<IController>();

            controllers.Add(storageTruck);
            controllers.Add(reloadingUnit);
            controllers.Add(viewController);
            controllers.Add(lightController);

            StorageUnitMapper.numberTypes.Add(1, storageTruck);
            StorageUnitMapper.numberTypes.Add(2, reloadingUnit);
            StorageUnitMapper.numberTypes.Add(3, viewController);
            StorageUnitMapper.numberTypes.Add(4, lightController);

            return controllers;
        }
    }
}
