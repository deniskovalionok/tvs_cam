﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Assets.Scripts.Interfaces;

namespace Assets.Scripts
{
    public class DataStructuresHandler
    {

        private RefsHandler refsHandler;
        private IDataProvider DataProvider;
        private Dictionary<string, Data> dataStructuresByParams;
        private List<IController> controllers;
        

        public DataStructuresHandler(RefsHandler refsHandler, IDataProvider dataProvider)
        {
            dataStructuresByParams = new Dictionary<string, Data>();
            controllers = new List<IController>();
            this.refsHandler = refsHandler;
            DataProvider = dataProvider;
            CoupleDataWithControllers();
        }

        //Связывает каждый объект, представляющий данные с его контроллером
        private void CoupleDataWithControllers()
        {
            foreach (var dataObj in DataProvider.GetDataObjects())
            {
                RegisterData(dataObj);
            }
        }          

        //Регистрирует объект-данные в системе
        private void RegisterData(Data structure)
        {
            ConnectToStructureDictionary(structure);
            ConnectControllerToDataStructure(structure);
        }

        //Заполняет словарь уникальными ключами - названиями переменных и значениями - объектами-данными
        private void ConnectToStructureDictionary(Data structure)
        {
            foreach (var parameterName in structure.GetAllDataParameterNames())
            {
                //Исключает добавление мусорных ключей в словарь
                if (parameterName != null && parameterName != String.Empty)
                {
                    dataStructuresByParams.Add(parameterName, structure);
                }
            }
        }

        //Передает объект-данные контроллеру
        private void ConnectControllerToDataStructure(Data structure)
        {                   
            IController contr = refsHandler.GetControllerByEntityID(structure.GetDataId());
            if(contr!=null)
            {
                controllers.Add(contr);
                contr.SetData(structure);
            }            
        }

        //Устанавливает значение параметра по имени paramName в paramValue
        public void SetDataValue(string paramName, object paramValue)
        {
            if (dataStructuresByParams.ContainsKey(paramName))
            {
                dataStructuresByParams[paramName].SetParameterValue(paramName, paramValue);
            }
        }
        

        //Обновляет данные для всех контроллеров объектов в сцене
        public void NotifyDataChanged()
        {
            foreach (var item in controllers)
            {
                item.DataUpdatedHandler();
            }
            DataAccepted?.Invoke();
        }

        //Уведомление о том, что данные установлены
        public event Action DataAccepted;
    }
}
