﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Assets.Scripts.Interfaces;
using Assets.Scripts.DataStructures;
using UnityEngine;

namespace Assets.Scripts
{
    public class LightController : MonoBehaviour, IController
    {
        private LightData previousData;
        public GameObject lightHandler;

        public void SetData(Data dataToSet)
        {
            LightData data = dataToSet as LightData;
            previousData = data;
        }

        public void UpdateControllingParameters()
        {
            if (previousData == null)
            {
                return;
            }

            if (previousData.LightTrigger == 0)
            {
                lightHandler.SetActive(false);
            }

            if(previousData.LightTrigger == 1)
            {
                lightHandler.SetActive(true);
            }
        }

        public void DataUpdatedHandler()
        {
            
        }
    }
}
