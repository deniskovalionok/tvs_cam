﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Assets.Scripts.Interfaces;

namespace Assets.Scripts
{
    public class PowderStorageMapper
    {
        public static Dictionary<string, int> entitiesNumbers = new Dictionary<string, int>
        {
            {"Truck", 1},
            {"GranulaClamps", 2 },
            {"SamplingClamps", 3 },
            {"Container1", 4 },
            {"NestGrabHand1", 5 },
            {"NestGrabHand2", 6 },
            {"NestGrabHand3", 7 },
            {"NestGrabHand4", 8 },
            {"Gate", 9 },
            {"Light", 10 },
            {"ViewSelector", 11 },
            {"GrabHand", 12 },
            {"TableRod", 13 },
            {"RotatingTable", 14 },
            {"ZinkLifter", 15 },
            {"SamplingCarriage", 16 },
            {"BucksCont", 17 },
            {"Container2", 18},
            {"Container3", 19}

        };

        public static Dictionary<int, IController> numberTypes = new Dictionary<int, IController>();
    }
}
