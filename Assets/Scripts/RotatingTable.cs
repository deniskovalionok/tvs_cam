﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using Assets.Scripts.Interfaces;
using Assets.Scripts.DataStructures;

namespace Assets.Scripts
{
    public class RotatingTable : MonoBehaviour, IController
    {


        private RotatingTableData previousData;
        private float previousRealTime;
        private Vector3 rotatingTableOriginPos;
        private Quaternion target;
        private Quaternion originRotation;
        private float rotateValue = 0.1f;

        public Transform rotatingTable;

        private void Start()
        {
            rotatingTableOriginPos = rotatingTable.localPosition;
            originRotation = rotatingTable.localRotation;
            //target = originRotation;

            //_prData = new RotatingTableData("", "", "", "", -1);
            //_prData.ZCoord = 0;
            //_prData.AxisPosition = 0;
            //SetData(_prData);
            //StartCoroutine(Process());
        }


        private IEnumerator Process()
        {
            //_prData.Zvelocity = 20;
            //SetData(_prData);
            //UpdateControllingParameters();
            //while (_prData.ZCoord < 50)
            //{
            //    _prData.ZCoord += _prData.Zvelocity * 0.1f;
            //    yield return new WaitForSeconds(0.1f);
            //}
            //_prData.ZCoord= 50;
            //_prData.Zvelocity = 0;
            _prData.RotatingVelocity = 0.1f;          
            SetData(_prData);
            UpdateControllingParameters();
            while (_prData.AxisPosition < 1)
            {
                _prData.AxisPosition += _prData.RotatingVelocity * 0.1f;
                SetData(_prData);
                yield return new WaitForSeconds(0.1f);
            }
            _prData.AxisPosition = 1;
            _prData.RotatingVelocity = 0;
            SetData(_prData);
        }

        private void Update()
        {
            //UpdateControllingParameters();
        }

        private RotatingTableData _prData;

        public void SetData(Data dataToSet)
        {
            previousData = (RotatingTableData)dataToSet;
            //target = originRotation * Quaternion.AngleAxis(130 * (float)previousData.AxisPosition, Vector3.down);
            //previousRealTime = Time.realtimeSinceStartup;
        }

        public void UpdateControllingParameters()
        {
            float r = Time.realtimeSinceStartup;
            Moving(r);
            Rotating(r);
            previousRealTime = r;
        }

        private void Moving(float currentTime)
        {

            if (GameCore.Instance.IsLerp)
            {
                rotatingTable.localPosition = new Vector3(rotatingTable.localPosition.x, Mathf.Lerp(rotatingTable.localPosition.y, rotatingTableOriginPos.y + (float)previousData.ZCoord / 1000, Time.deltaTime), rotatingTable.localPosition.z);
            }
            else
            {
                rotatingTable.localPosition = new Vector3(rotatingTable.localPosition.x,
                    Mathf.MoveTowards(rotatingTable.localPosition.z, rotatingTableOriginPos.y + (float)previousData.ZCoord / 1000, (Mathf.Abs((float)previousData.Zvelocity) / 1000) * (currentTime - previousRealTime)),
                    rotatingTable.localPosition.z);
            }

            if(previousData.Zvelocity == 0)
            {
                rotatingTable.localPosition = new Vector3(rotatingTable.localPosition.x, rotatingTableOriginPos.y + (float)previousData.ZCoord / 1000, rotatingTable.localPosition.z);
            }
        }

        private void Rotating(float currentTime)
        {
            if (rotatingTable.localRotation != target)
            {                
                rotatingTable.localRotation = Quaternion.RotateTowards(rotatingTable.localRotation, target, 130 * /*rotateValue*/ Mathf.Abs((float)previousData.RotatingVelocity) * (currentTime - previousRealTime));
            }
            
            if(previousData.RotatingVelocity == 0)
            {
                rotatingTable.localRotation = target;
            }
        }

        public void DataUpdatedHandler()
        {
            target = originRotation * Quaternion.AngleAxis(130 * (float)previousData.AxisPosition, Vector3.down);
            previousRealTime = Time.realtimeSinceStartup;
        }
    }
}
