﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Assets.Scripts;
using System.IO;
using System;
using Assets.Scripts.Interfaces;

public class GameCore : MonoBehaviour
{
    //События анимации
    public AnimationEventsHandler animEventHandler;
    //Сетевой контроллер
    public NetClient netClient;
    //Родительский объект для все объектов движения
    public Transform Center;
    //Флаг, меняющий методику перемещения объектов
    public bool IsLerp;    
    //Держатель все ссылок
    public RefsHandler ObjRefsHandler;
    //Флаг, с установкой которого в значение true начинается перемещение всех объектов в сцене
    private bool IsFirstDataSetted = false;    

    //Статический доступ
    public static GameCore Instance;

    private void Awake()
    {
        if(Instance == null)
        {
            Instance = this;
        }                
    }

    // Start is called before the first frame update
    void Start()
    {
        //return;

        //Область изменений
        DataStructuresHandler dataHandler = new DataStructuresHandler(ObjRefsHandler, new TVSDataHandler());
        //Область изменений

        DataParser dataParser = new DataParser(dataHandler);
        netClient.DataParser = dataParser;
        dataHandler.DataAccepted += DataHandler_DataAccepted;        

        ReadConfig();        
        netClient.Connect();       
        
    }
    
    private void ReadConfig()
    {

        Dictionary<string, string> configValues = new Dictionary<string, string>(){
            {"Port", "9080" },
            {"IP-Address", "127.0.0.1"}
        };

        try
        {

            string configKey = String.Empty;

            using (FileStream fHandler = File.OpenRead("config.cfg"))
            {
                using (StreamReader reader = new StreamReader(fHandler))
                {
                    while(!reader.EndOfStream)
                    {
                        string val = reader.ReadLine();                        

                        if (val.StartsWith("["))
                        {
                            configKey = val.Substring(1, val.Length - 2);                            
                            continue;
                        }
                        else
                        {
                            if(configKey!=String.Empty && configKey!=null)
                            {
                                configValues[configKey] = val;
                                configKey = String.Empty;
                            }
                            
                        }
                    }
                }
            }

            WSOptions.ServerAddress = $"ws://{configValues["IP-Address"]}:{configValues["Port"]}/ws";
        }
        catch(Exception e)
        {
            Debug.Log($"Exception {e.Message} occured during config file reading!");
            WSOptions.ServerAddress = "ws://127.0.0.1:8080/ws";
        }
        
        
    }

    //Обработчик прихода сообщения от сервера
    private void DataHandler_DataAccepted()
    {
        IsFirstDataSetted = true;   
    }




    private void Update()
    {
        if (IsFirstDataSetted)
        {
            ObjRefsHandler.UpdateControllingObjectsPositions();
        }

       

    }
    


}
