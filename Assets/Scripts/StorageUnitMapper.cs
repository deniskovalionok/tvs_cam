﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Assets.Scripts.Interfaces;

namespace Assets.Scripts
{
    public class StorageUnitMapper
    {

        public static Dictionary<string, int> entitiesNumbers = new Dictionary<string, int>
        {
            {"Truck", 1},
            {"ReloadingUnit", 2},
            {"Camera", 3},
            {"Lighting", 4}
        };

        public static Dictionary<int, IController> numberTypes = new Dictionary<int, IController>();
    }
}
