﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Assets.Scripts.DataStructures;
using Assets.Scripts.Interfaces;

namespace Assets.Scripts
{
    public class StorageUnitDataHandler : IDataProvider
    {
        public IEnumerable<Data> GetDataObjects()
        {
            List<Data> dataObjects = new List<Data>();

            dataObjects.Add(new TruckData("ManipulatorXCoord", "ManipulatorYCoord", "ManipulatorXVelocity", "ManipulatorYVelocity", StorageUnitMapper.entitiesNumbers["Truck"], "GrabRotateVel", "GrabRotateAxis"));
            dataObjects.Add(new DoorData("ReloadingCoord", "ReloadingVelocity", StorageUnitMapper.entitiesNumbers["ReloadingUnit"]));
            dataObjects.Add(new ViewData("CameraNumber", StorageUnitMapper.entitiesNumbers["Camera"]));
            dataObjects.Add(new LightData("Light", StorageUnitMapper.entitiesNumbers["Lighting"]));

            return dataObjects;
        }
    }
}
