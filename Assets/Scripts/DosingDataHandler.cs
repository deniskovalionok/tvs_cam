﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using Assets.Scripts.DataStructures;
using Assets.Scripts.Interfaces;

namespace Assets.Scripts
{
    public class DosingDataHandler: IDataProvider
    {

        //private Dictionary<string, Data> dataStructuresByParams;
        
        //private Dictionary<Data, IController> controllersByDataStructures;

        //private RefsHandler refsHandler;

        //public event Action DataAccepted;

        public DosingDataHandler(/*RefsHandler refsHandler*/)/*:base(refsHandler, null)*/
        {
            //dataStructuresByParams = new Dictionary<string, Data>();            
            //controllersByDataStructures = new Dictionary<Data, IController>();
            //this.refsHandler = refsHandler;            
        }

        public IEnumerable<Data> GetDataObjects()
        {

            List<Data> dataStructures = new List<Data>();

            //Big container 01
            ContainerData bigCont01Data = new ContainerData("BigContainer01XVelocity", "BigContainer01YVelocity", "BigContainer01ZVelocity",
                "BigContainer01XCoord", "BigContainer01YCoord", "BigContainer01ZCoord", "BigContainer01RotateVel", "BigContainer01AxePosition", DosingMapper.entitiesNumbers["HugeContainer"]);
            //Big container 02
            ContainerData bigCont02Data = new ContainerData("BigContainer02XVelocity", "BigContainer02YVelocity", "BigContainer02ZVelocity",
                "BigContainer02XCoord", "BigContainer02YCoord", "BigContainer02ZCoord", "BigContainer02RotateVel", "BigContainer02AxePosition", DosingMapper.entitiesNumbers["HugeContainer"]);
            //Big container 03
            ContainerData bigCont03Data = new ContainerData("BigContainer03XVelocity", "BigContainer03YVelocity", "BigContainer03ZVelocity",
                "BigContainer03XCoord", "BigContainer03YCoord", "BigContainer03ZCoord", "BigContainer03RotateVel", "BigContainer03AxePosition", DosingMapper.entitiesNumbers["HugeContainer"]);
            //Autooperator
            ManipulatorData manipData = new ManipulatorData("AutoOperatorXVelocity", "AutoOperatorYVelocity", "AutoOperatorZVelocity", "AutoOperatorXCoord", "AutoOperatorYCoord",
                "AutoOperatorZCoord", "AutoOperatorGrab", DosingMapper.entitiesNumbers["Manipulator"]);
            //Titler for big containers
            TitlerData bigTitlerData = new TitlerData("TitlerRotateVel", "TitlerAxePosition", DosingMapper.entitiesNumbers["HugeTitler"]);
            //Titler for small containers
            TitlerData smallTitlerData = new TitlerData("SmallTitlerRotateVel", "SmallTitlerAxePosition", DosingMapper.entitiesNumbers["SmallTitler"]);
            //Door for big containers
            DoorData door01Data = new DoorData("DoorCoord", "DoorVelocity", DosingMapper.entitiesNumbers["Door"]);
            //Small container 01
            ContainerData smallContainer01Data = new ContainerData("SmallContainer01XVelocity", "SmallContainer01YVelocity", "SmallContainer01ZVelocity",
                "SmallContainer01XCoord", "SmallContainer01YCoord", "SmallContainer01ZCoord", "SmallContainer01RotateVel", "SmallContainer01AxePosition",
                DosingMapper.entitiesNumbers["SmallContainer"]);
            //Small container 02
            ContainerData smallContainer02Data = new ContainerData("SmallContainer02XVelocity", "SmallContainer02YVelocity", "SmallContainer02ZVelocity",
                "SmallContainer02XCoord", "SmallContainer02YCoord", "SmallContainer02ZCoord", "SmallContainer02RotateVel", "SmallContainer02AxePosition",
                DosingMapper.entitiesNumbers["SmallContainer"]);
            //Small container 03
            ContainerData smallContainer03Data = new ContainerData("SmallContainer03XVelocity", "SmallContainer03YVelocity", "SmallContainer03ZVelocity",
                "SmallContainer03XCoord", "SmallContainer03YCoord", "SmallContainer03ZCoord", "SmallContainer03RotateVel", "SmallContainer03AxePosition",
                DosingMapper.entitiesNumbers["SmallContainer"]);
            //Pluto gateway
            PlutoGatewayData plutoGatewayData = new PlutoGatewayData("PlutoGatewayZVelocity", "PlutoGatewayZCoord", "PlutoGatewayRotateVel", "PlutoGatewayAxePosition",
                DosingMapper.entitiesNumbers["PlutoGatewey"]);
            //Glass cap capture
            CapHandlerData capHandler01Data = new CapHandlerData("GlassCapCaptureZCoord", "GlassCapCaptureZVelocity", "GlassCapCaptureRotateVel", "GlassCapCaptureAxePosition",
                "GlassCapCaptureState", DosingMapper.entitiesNumbers["GlassCapCapturer"]);
            //Funnel drive
            CapHandlerData capHandler02Data = new CapHandlerData("FunnelDriveZCoord", "FunnelDriveZVelocity", "FunnelDriveRotateVel", "FunnelDriveAxePosition",
                "FunnelDriveState", DosingMapper.entitiesNumbers["FunnelDrive"]);
            //Camera
            ViewData viewData = new ViewData("CameraNumber", DosingMapper.entitiesNumbers["ViewController"]);
            //Light
            LightData lightData = new LightData("LightState", DosingMapper.entitiesNumbers["LightController"]);

            dataStructures.Add(bigCont01Data);
            dataStructures.Add(bigCont02Data);
            dataStructures.Add(bigCont03Data);
            dataStructures.Add(smallContainer01Data);
            dataStructures.Add(smallContainer02Data);
            dataStructures.Add(smallContainer03Data);
            dataStructures.Add(manipData);
            dataStructures.Add(bigTitlerData);
            dataStructures.Add(smallTitlerData);
            dataStructures.Add(capHandler01Data);
            dataStructures.Add(capHandler02Data);
            dataStructures.Add(viewData);
            dataStructures.Add(lightData);
            dataStructures.Add(plutoGatewayData);
            dataStructures.Add(door01Data);

            return dataStructures;
        }

        //public void SetDataValue(string paramName, object paramValue)
        //{
        //    if (dataStructuresByParams.ContainsKey(paramName))
        //    {                
        //        dataStructuresByParams[paramName].SetParameterValue(paramName, paramValue);
        //    }
        //}

        //public void ApplyDataChanges()
        //{
        //    foreach (var item in controllersByDataStructures)
        //    {
        //        item.Value.SetData(item.Key);
        //    }
        //    DataAccepted?.Invoke();
        //}


        ////Инициирует заполнение словарей
        //private void SetDictionaries()
        //{
        //    foreach (var item in dataStructures)
        //    {
        //        ConnectToDictionary(item);
        //        ConnectControllerToDataStructure(item);
        //    }
        //}

        ////Заполняет словарь уникальными ключами - названиями переменных и значениями - объектами-данными
        //private void ConnectToDictionary(Data structure)
        //{
        //    foreach (var item in structure.GetNextParamName())
        //    {
        //        dataStructuresByParams.Add(item, structure);
        //    }
        //}

        ////Заполняет словарь ключами - объектами-данными и значениями - контроллерами объектов на сцене
        //private void ConnectControllerToDataStructure(Data structure)
        //{        
        //    controllersByDataStructures.Add(structure, refsHandler.GetControllerByEntityID(structure.GetDataId()));
        //}



    }
}
