﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Scripts
{
    public class GrabProcessor: MonoBehaviour
    {

        public Transform Socket;        

        private Transform currentlyControllingBox;       
        private Collider grabCollider;

        private Vector3 previousPosition = Vector3.zero;

        private void Start()
        {
            grabCollider = this.transform.GetComponent<Collider>();                               
        }

                       

        private void Update()
        {

            float direction = this.transform.position.y - previousPosition.y;

            RaycastHit hit;

            //Debug.DrawRay(Socket.transform.position, Socket.transform.forward * (-10));

            if(currentlyControllingBox == null && Physics.Raycast(Socket.transform.position, (Socket.transform.forward*(-10)), out hit, 1000))
            {
                if(hit.transform.CompareTag("Box"))
                {
                    
                    float distance = (hit.point - grabCollider.bounds.center).y;                    
                    if(distance - grabCollider.bounds.extents.y < 0.001f && direction>0)
                    {
                        currentlyControllingBox = hit.transform;
                        currentlyControllingBox.parent = Socket;
                    }
                    
                }
            }
            
            if(currentlyControllingBox!=null && Physics.Raycast(Socket.transform.position, (Socket.transform.forward * (10)), out hit, 1000))
            {
                if (hit.transform.CompareTag("Socket"))
                {

                    float distance = (grabCollider.bounds.center - hit.point).y;
                    if (distance - grabCollider.bounds.extents.y < 0 && direction<0)
                    {
                        currentlyControllingBox.parent = hit.transform;
                        currentlyControllingBox = null;
                    }

                }
            }

            previousPosition = this.transform.position;


            //float distance = Vector3.Distance(other.bounds.center, other.bounds.center - new Vector3(0, other.bounds.size.y / 2, 0));
            //float triggerDist = other.bounds.center.y - (grabCollider.bounds.center + new Vector3(0, grabCollider.bounds.size.y / 2, 0)).y;

            //Debug.Log($"BoxDistance: {distance}");
            //Debug.Log($"BoxTriggerDist: {triggerDist}");

            //if (triggerDist > 0 && Mathf.Abs(distance - triggerDist) < 0.003f)
            //{
            //    Debug.Log("Captured!");
            //    currentlyControllingBox = other.transform;
            //    //currentlyControllingBox.parent = this.transform;
            //    //currentlyControllingBox.parent.LookAt(Socket.TransformPoint(Socket.right * (-5)), Vector3.up);
            //}


            //Debug.Log(Vector3.SignedAngle(this.transform.TransformPoint(this.transform.up * (1f)) - this.transform.position, Vector3.up, Vector3.right));
            //Vector3 one = this.transform.TransformPoint(this.transform.up * (15f)) - this.transform.position;            
            //Vector3 one = cube1.position- cube.position;
            //Vector3 two = Vector3.up;
            ////Debug.DrawRay(cube.transform.position, two, Color.green);
            //Debug.DrawRay(cube.transform.position, Vector3.Cross(one, two), Color.green);

            //cube.transform.position = this.transform.TransformPoint(this.transform.right * (-15f));
            //cube.transform.LookAt(this.transform.TransformPoint(this.transform.right * (-20f)));

            //if (currentlyControllingBox!=null)
            //{
            //    //Debug.Log("Box transporting!");                
            //    //Vector3 v = GameCore.Instance.Center.TransformVector(GameCore.Instance.Center.InverseTransformVector(Socket.position) + offset);               
            //    //currentlyControllingBox.localPosition = currentlyControllingBox.parent.InverseTransformVector(v);
            //    //currentlyControllingBox.localRotation = originRot * Quaternion.AngleAxis(Vector3.SignedAngle(Socket.position - this.transform.position, boxUpDirection, currentlyControllingBox.forward), Vector3.back);

            //    Debug.DrawLine(this.transform.position, this.transform.parent.TransformPoint(this.transform.up * (-10)));                
            //    cube.transform.localPosition = Socket.up*(-10);
            //    currentlyControllingBox.parent.LookAt(Socket.TransformPoint(Socket.up * (-10)), Vector3.up);
            //}
            //else
            //{
            //    Debug.Log("Currently contolling box is null!");
            //}



        }

    }
}
