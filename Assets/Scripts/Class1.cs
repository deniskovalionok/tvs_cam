﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Scripts
{
    public class Class1: MonoBehaviour
    {
        public Animator anim1;
        public Animator anim2;

        public AnimationClip cl2;
        public AnimationClip cl1;


        public void Start()
        {
            
        }

        public void Trigger()
        {
            anim2.SetTrigger("trig");            
        }

        private int count = 0;

        public void Reverse(int a)
        {            
            if(a == 1)
            {
                anim2.SetTrigger("rev");
                count = 1;
            }

            if(count == 1)
            {
                if(a==2)
                {
                    anim1.SetTrigger("rev");
                    this.transform.GetChild(0).gameObject.SetActive(false);
                    count = 0;
                }
            }
            
        }
    }
}
