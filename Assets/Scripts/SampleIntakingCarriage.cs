﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using Assets.Scripts.Interfaces;
using Assets.Scripts.DataStructures;

namespace Assets.Scripts
{
    public class SampleIntakingCarriage : MonoBehaviour, IController
    {

        private SamplingCarriageData previousData;
        private float previousRealTime;

        public Transform carriage;
        public Transform sampleDriver;
        public Transform smallSampleDriver;

        public Transform UploadingPoint;
        public Transform DownloadingPoint;
        public Transform HighPoint;
        public Transform DownPoint;
        public Transform SampleDownloadingPoint;
        public Transform TransportingPoint;
        public float rod5MaxDistance;
        public float rod6MaxDistance;
        public float rod7MaxDistance;

        private Vector3 carriageOriginPos;
        private Vector3 sampleDriverOriginPos;
        private Vector3 smallSampleDriverOriginPos;
        private float horOffsetFromSampleToCarriage;
        private float horOffsetFromSmallSampleToCarriage;
        private float vertOffsetFromSmallSampleToVerticalSample;
        private float rod5Koeff;
        private float rod6Koeff;
        private float rod7Koeff;

        private void Start()
        {
            carriageOriginPos = carriage.localPosition;
            sampleDriverOriginPos = sampleDriver.localPosition;
            smallSampleDriverOriginPos = smallSampleDriver.localPosition;

            horOffsetFromSampleToCarriage = (sampleDriver.localPosition - carriage.localPosition).z;
            horOffsetFromSmallSampleToCarriage = (smallSampleDriver.localPosition - carriage.localPosition).z;
            vertOffsetFromSmallSampleToVerticalSample = (smallSampleDriver.localPosition - sampleDriver.localPosition).y;
            rod5Koeff = Math.Abs(UploadingPoint.localPosition.z - DownloadingPoint.localPosition.z)*1000 / rod5MaxDistance;
            rod6Koeff = Math.Abs(HighPoint.localPosition.y - DownPoint.localPosition.y)*1000 / rod6MaxDistance;
            rod7Koeff = Math.Abs(TransportingPoint.localPosition.y - SampleDownloadingPoint.localPosition.y)*1000 / rod7MaxDistance;
            previousRealTime = Time.realtimeSinceStartup;
        }


        private SamplingCarriageData _prData;

        public void SetData(Data dataToSet)
        {
            previousData = (SamplingCarriageData)dataToSet;
            //previousRealTime = Time.realtimeSinceStartup;
        }


        private IEnumerator Process()
        {
            _prData.XVelocity = 20;
            SetData(_prData);
            UpdateControllingParameters();
            while(_prData.XCoord<250)
            {
                _prData.XCoord += _prData.XVelocity * 0.1f;
                yield return new WaitForSeconds(0.1f);
            }
            _prData.XCoord = 250;
            _prData.XVelocity = 0;
            SetData(_prData);
        }

        private void Update()
        {
            
        }

        public void UpdateControllingParameters()
        {
            float r = Time.realtimeSinceStartup;
            HorizontalMoving(r);
            VerticalMoving(r);
            previousRealTime = r;
        }

        private void HorizontalMoving(float currentTime)
        {
            if (previousData == null)
            {
                return;
            }


            if (GameCore.Instance.IsLerp)
            {
                carriage.localPosition = new Vector3(carriage.localPosition.x, carriage.localPosition.y, Mathf.Lerp(carriage.localPosition.z, carriageOriginPos.z + rod5Koeff*(float)previousData.XCoord / 1000, Time.deltaTime));
            }
            else
            {

                carriage.localPosition = new Vector3(carriage.localPosition.x, carriage.localPosition.y,
                    Mathf.MoveTowards(carriage.localPosition.z, carriageOriginPos.z + rod5Koeff*(float)previousData.XCoord/ 1000, (Mathf.Abs((float)previousData.XVelocity) / 1000) * (currentTime- previousRealTime)));
            }

            sampleDriver.localPosition = new Vector3(sampleDriver.localPosition.x, sampleDriver.localPosition.y, carriage.localPosition.z + horOffsetFromSampleToCarriage);
            smallSampleDriver.localPosition = new Vector3(smallSampleDriver.localPosition.x, smallSampleDriver.localPosition.y, carriage.localPosition.z + horOffsetFromSmallSampleToCarriage);

            if (previousData.XVelocity == 0)
            {
                carriage.localPosition = new Vector3(carriage.localPosition.x, carriage.localPosition.y, carriageOriginPos.z + rod5Koeff*(float)previousData.XCoord / 1000);
            }
        }

        private void VerticalMoving(float currentTime)
        {
            if (previousData == null)
            {   
                return;
            }            

            if (GameCore.Instance.IsLerp)
            {
                sampleDriver.localPosition = new Vector3(sampleDriver.localPosition.x, Mathf.Lerp(sampleDriver.localPosition.y, sampleDriverOriginPos.y + rod6Koeff*(float)previousData.RodYCoord / 1000, Time.deltaTime), sampleDriver.localPosition.z);                
                //smallSampleDriver.localPosition = new Vector3(smallSampleDriver.localPosition.x, Mathf.Lerp(smallSampleDriver.localPosition.y, smallSampleDriverOriginPos.y + rod7Koeff*(float)previousData.IntakeYCoord / 1000, Time.deltaTime), smallSampleDriver.localPosition.z);
            }
            else
            {
                sampleDriver.localPosition = new Vector3(sampleDriver.localPosition.x, Mathf.MoveTowards(sampleDriver.localPosition.y, sampleDriverOriginPos.y + rod6Koeff * (float)previousData.RodYCoord / 1000, (Mathf.Abs((float)previousData.RodYVelocity) / 1000) * (currentTime - previousRealTime)), 
                    sampleDriver.localPosition.z);
                //smallSampleDriver.localPosition = new Vector3(smallSampleDriver.localPosition.x, Mathf.MoveTowards(smallSampleDriver.localPosition.y, smallSampleDriverOriginPos.y + rod7Koeff * (float)previousData.IntakeYCoord / 1000, (Mathf.Abs((float)previousData.IntakeYVelocity) / 1000) * (currentTime - previousRealTime)),
                //    smallSampleDriver.localPosition.z);
            }
            Vector3 smallSampleOrigin = new Vector3(smallSampleDriver.localPosition.x, sampleDriver.localPosition.y + vertOffsetFromSmallSampleToVerticalSample, smallSampleDriver.localPosition.z);

            if (GameCore.Instance.IsLerp)
            {
                smallSampleDriver.localPosition = new Vector3(smallSampleDriver.localPosition.x, Mathf.Lerp(smallSampleDriver.localPosition.y, smallSampleOrigin.y + rod7Koeff * (float)previousData.IntakeYCoord / 1000, Time.deltaTime), smallSampleDriver.localPosition.z);
            }
            else
            {
                smallSampleDriver.localPosition = new Vector3(smallSampleDriver.localPosition.x, Mathf.MoveTowards(smallSampleDriver.localPosition.y, smallSampleOrigin.y + rod7Koeff * (float)previousData.IntakeYCoord / 1000, (Mathf.Abs((float)previousData.IntakeYVelocity) / 1000) * (currentTime - previousRealTime)),
                   smallSampleDriver.localPosition.z);
            }

            //smallSampleDriver.localPosition = new Vector3(smallSampleDriver.localPosition.x, sampleDriver.localPosition.y + vertOffsetFromSmallSampleToVerticalSample, smallSampleDriver.localPosition.z) ;

            if (previousData.RodYVelocity == 0)
            {
                sampleDriver.localPosition = new Vector3(sampleDriver.localPosition.x, sampleDriverOriginPos.y + rod6Koeff * (float)previousData.RodYCoord / 1000, sampleDriver.localPosition.z);
            }   
            if(previousData.IntakeYVelocity == 0)
            {
                smallSampleDriver.localPosition = new Vector3(smallSampleDriver.localPosition.x, smallSampleOrigin.y + rod7Koeff * (float)previousData.IntakeYCoord / 1000, smallSampleDriver.localPosition.z);
            }
        }

        public void DataUpdatedHandler()
        {
            previousRealTime = Time.realtimeSinceStartup;
        }
    }
}
