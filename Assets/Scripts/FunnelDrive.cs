﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Assets.Scripts.Interfaces;
using UnityEngine;
using Assets.Scripts.DataStructures;
using System.Collections;

namespace Assets.Scripts
{
    public class FunnelDrive : MonoBehaviour, IController
    {
        private CapHandlerData previousData;
        private Vector3 originPosition;
        private Quaternion originRotation;
        private float previousRealTime;

        private Quaternion target;
        private float rotateValue = 0.1f;

        public Transform funnelDriveObj;
        public Transform funnelObj;
        public Transform titlerGlass;

        private void Start()
        {
            originPosition = funnelDriveObj.localPosition;
            originRotation = funnelDriveObj.localRotation;

            //_pr = new CapHandlerData("", "", "", "", "", -1);
            //_pr.AxePosition = 1;
            //_pr.ZCoord = 1050;
            //_pr.ZVelocity = 0;
            //_pr.State = 0;
           //SetData(_pr);

            //StartCoroutine(DelayInvoke());
        }

        public void SetData(Data dataToSet)
        {           
            CapHandlerData data = dataToSet as CapHandlerData;
            //data.State = StateValue;
            previousData = data;
            //target = originRotation * Quaternion.AngleAxis(180 * (float)previousData.AxePosition, Vector3.back);
            //previousRealTime = Time.realtimeSinceStartup;
            //IsDataSetted = true;
        }

        private void Update()
        {
            //Debug.Log(funnelObj.position);
            //Debug.Log(funnelObj.transform.parent.name);
            //UpdateControllingParameters();

            //if(Input.GetKeyDown(KeyCode.J))
            //{
            //    Debug.Log("ff1");
            //    _pr.State = 1;
            //    _pr.RotateVel = -10;
            //    SetData(_pr);
            //    StartCoroutine(DelayInvoke());
            //}

            //if (Input.GetKeyDown(KeyCode.S))
            //{
            //    _pr.ZVelocity = 0;
            //    _pr.RotateVel = 0;
            //    SetData(_pr);                
            //}

            //if (Input.GetKeyDown(KeyCode.D))
            //{
            //    _pr.ZVelocity = -20;
            //    SetData(_pr);
            //    StartCoroutine(DelayInvoke());
            //}
        }


        private IEnumerator DelayInvoke()
        {
            //_pr.RotateVel = -10;

            while(true)
            {

                //if (_pr.AxePosition >= 1)
                //{
                //    _pr.State = 0;
                //    SetData(_pr);
                //    break;
                //}

                //if(_pr.AxePosition<=0)
                //{
                //    break;
                //}
                    

                //_pr.AxePosition += _pr.RotateVel / 1000;
                //_pr.ZCoord += _pr.ZVelocity/10;
                //SetData(_pr);
                yield return new WaitForSeconds(0.1f);
            }

        }

        //private CapHandlerData _pr;
        

        private bool IsDataSetted = false;

        
        public bool IsControllingByDrive()
        {
            if (previousData == null)
                return false;
            return previousData.State==1;
        }

        public void UpdateControllingParameters()
        {

            if (!IsDataSetted)
                return;

            float r = Time.realtimeSinceStartup;


            if(GameCore.Instance.IsLerp)
            {
                funnelDriveObj.localPosition = new Vector3(funnelDriveObj.localPosition.x, Mathf.Lerp(funnelDriveObj.localPosition.y, (float)previousData.ZCoord / 1000, Time.deltaTime), funnelDriveObj.localPosition.z);
            }
            else
            {
                funnelDriveObj.localPosition = new Vector3(funnelDriveObj.localPosition.x,
                Mathf.MoveTowards(funnelDriveObj.localPosition.y, (float)previousData.ZCoord / 1000, (Mathf.Abs((float)previousData.ZVelocity) / 1000) * (r - previousRealTime)),
                funnelDriveObj.localPosition.z);
            }            


            if (funnelDriveObj.localRotation != target)
            {
                funnelDriveObj.localRotation = Quaternion.RotateTowards(funnelDriveObj.localRotation, target, 180 * /*rotateValue*/ Mathf.Abs((float)previousData.RotateVel) * (r - previousRealTime));
            }

            previousRealTime = r;

            if (previousData.ZVelocity == 0)
            {
                funnelDriveObj.localPosition = new Vector3(funnelDriveObj.localPosition.x, (float)previousData.ZCoord / 1000, funnelDriveObj.localPosition.z);
            }

            if (previousData.RotateVel == 0)
            {
                funnelDriveObj.localRotation = target;
            }

            if (previousData.State == 0)
            {
                //Debug.Log("State:0");
                funnelObj.parent = titlerGlass;            
            }
    
            if (previousData.State == 1)
            {
                //Debug.Log("State:1");
                //funnelObj.parent = funnelDriveObj;
                funnelObj.parent = GameCore.Instance.Center;
            }
        }

        public void DataUpdatedHandler()
        {
            target = originRotation * Quaternion.AngleAxis(180 * (float)previousData.AxePosition, Vector3.back);
            previousRealTime = Time.realtimeSinceStartup;
            IsDataSetted = true;
        }
    }
}
