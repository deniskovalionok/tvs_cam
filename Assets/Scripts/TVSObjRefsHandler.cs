﻿using Assets.Scripts.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Scripts
{
    public class TVSObjRefsHandler : RefsHandler
    {

        public ViewSelector viewController;
        public AnimationProcessHolder DockingNode;
        public AnimationProcessHolder Door;
        public AnimationProcessHolder LiveRoll;
        public AnimationProcessHolder MagazinDisassemble;
        public AnimationProcessHolder TvelPositioning;
        public AnimationProcessHolder TvelBeamAssemble;
        public AnimationProcessHolder Autooperator;
        public AnimationProcessHolder Titler;
        public AnimationProcessHolder HeadShorer;
        public AnimationProcessHolder CoordManipulator;
        public AnimationProcessHolder TailiDettaching;
        public AnimationProcessHolder CoordFacility;
        public AnimationProcessHolder TvelPusher;
        public AnimationProcessHolder TvelPuller;
        public AnimationProcessHolder TailiTransporter;
        public AnimationProcessHolder TransportFacility;
        public AnimationProcessHolder TruckTransmiting;
        public AnimationProcessHolder CraneManipulator;

        //public Dictionary<int, IController> controllers = new Dictionary<int, IController>();

        //public void Start()
        //{
        //    controllers.Add(1, viewController);
        //}

        public override IController GetControllerByEntityID(int entityID)
        {
            return TVSMapper.numberTypes[entityID];
        }

        public override IEnumerable<IController> GetControllers()
        {
            List<IController> allControllers = new List<IController>();
            allControllers.Add(viewController);
            allControllers.Add(DockingNode);
            allControllers.Add(Door);
            allControllers.Add(LiveRoll);
            allControllers.Add(MagazinDisassemble);
            allControllers.Add(TvelPositioning);
            allControllers.Add(TvelBeamAssemble);
            allControllers.Add(Autooperator);
            allControllers.Add(Titler);
            allControllers.Add(HeadShorer);
            allControllers.Add(CoordManipulator);
            allControllers.Add(TailiDettaching);
            allControllers.Add(CoordFacility);
            allControllers.Add(TvelPusher);
            allControllers.Add(TvelPuller);
            allControllers.Add(TailiTransporter);
            allControllers.Add(TransportFacility);
            allControllers.Add(TruckTransmiting);
            allControllers.Add(CraneManipulator);
            

            TVSMapper.numberTypes.Add(1, viewController);
            TVSMapper.numberTypes.Add(2, DockingNode);
            TVSMapper.numberTypes.Add(3, Door);
            TVSMapper.numberTypes.Add(4, LiveRoll);
            TVSMapper.numberTypes.Add(5, MagazinDisassemble);
            TVSMapper.numberTypes.Add(6, TvelPositioning);
            TVSMapper.numberTypes.Add(7, TvelBeamAssemble);
            TVSMapper.numberTypes.Add(8, Autooperator);
            TVSMapper.numberTypes.Add(9, Titler);
            TVSMapper.numberTypes.Add(10, HeadShorer);
            TVSMapper.numberTypes.Add(11, CoordManipulator);
            TVSMapper.numberTypes.Add(12, TailiDettaching);
            TVSMapper.numberTypes.Add(13, CoordFacility);
            TVSMapper.numberTypes.Add(14, TvelPusher);
            TVSMapper.numberTypes.Add(15, TvelPuller);
            TVSMapper.numberTypes.Add(16, TailiTransporter);
            TVSMapper.numberTypes.Add(17, TransportFacility);
            TVSMapper.numberTypes.Add(18, TruckTransmiting);
            TVSMapper.numberTypes.Add(19, CraneManipulator);


            return allControllers;

        }
    }
}
