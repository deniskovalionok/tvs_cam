﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Assets.Scripts.Interfaces;

namespace Assets.Scripts
{
    public class VortexGrindingMapper
    {
        public static Dictionary<string, int> entitiesNumbers = new Dictionary<string, int>
        {
            {"Container1",1},
            {"Transporter1",2},
            {"Transporter2",3},
            {"Transporter3",4},
            {"Transporter4",5},
            {"Transporter5",6},
            {"Transporter6",7},
            {"Transporter7",8},
            {"Transporter8",9},
            {"Transporter9",10},
            {"Transporter10",11},
            {"Titler",12},
            {"LightController",13},
            {"ViewController",14},
            {"Container2",15},
            {"Container3",16},
            {"Container4",17},
            {"Container5",18},
            {"Container6",19},
            {"Container7",20},
            {"Container8",21},
            {"Container9",22},

        };


        public static Dictionary<int, IController> numberTypes = new Dictionary<int, IController>();        
    }
}
