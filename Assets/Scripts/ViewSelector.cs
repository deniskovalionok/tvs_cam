﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using Assets.Scripts.Interfaces;
using Assets.Scripts.DataStructures;

namespace Assets.Scripts
{
    public class ViewSelector: MonoBehaviour, IController
    {
        //public Camera cam1211;
        //public Camera cam1212;
        //public Camera cam1213;

        //public Camera cam1221;
        //public Camera cam1222;

        //public Camera cam1301;
        //public Camera cam1302;
        //public Camera cam1303;
        //public Camera cam1304;

        //public Camera cam1331;
        //public Camera cam1332;

        //public Camera cam1501;
        //public Camera cam1502;
        //public Camera cam1503;


        public Camera cam422AL4;
        public Camera cam424AL5;
        public Camera cam426AL6;
        public Camera cam428AL8;
        public Camera cam430AL9;
        public Camera cam432AL10;
        public Camera cam434AL11;
        public Camera cam436AL12;
        public Camera cam438AL13;
        public Camera cam440AL14;
        public Camera cam442AL15;
        public Camera cam444AL16;
        public Camera cam446AL7;
        public Camera cam448AL18;
        public Camera cam450AL19;

        public Camera Узел_стыковки;
        public Camera Шибер;
        public Camera Рольганг_для_подачи_ВТУК;
        public Camera Установка_разборки_магазина_с_твэлами;
        public Camera Установка_позиционирования_твэла_по_координатам;
        public Camera Установка_сборки_пучка_твэлов;
        public Camera Автооператор;
        public Camera Кантователь;
        public Camera Установка_крепления_головок_на_каркасе_ТВС;
        public Camera Координатный_некопирующий_манипулятор;
        public Camera Установка_контроля_герметичности_твэлов_в_ТВС;
        public Camera Установка_контроля_массы_ТВС;
        public Camera Установка_контроля_геометрии_и_внешнего_вида_ТВС;
        public Camera Буферное_хранилище;
        public Camera Шибер_загрузки_ТВС_в_пенал;

        public Camera Установка_отделения_хвостовика_ТВС;
        public Camera Установка_координатная;
        public Camera Установка_выталкивания_твэла;
        public Camera Установка_вытягивания_твэла;
        public Camera Установка_перемещения_хвостовика_ТВС;
        public Camera Установка_транспортная;
        public Camera Установка_удаления_продуктов_отделения;
        public Camera Установка_контроля_поверхностной_загрязненности_ТВС;

        public Camera Установка_снятия_крышки_с_пенала;
        public Camera Установка_считывания_маркировки_и_контроля_дозиметрических_характеристик;
        public Camera Тележка_передаточная_для_транспортировки_запеналенной_ТВС;
        public Camera Кран_манипулятор_для_перегрузки_пенала_с_ТВС;

        private Dictionary<long, Camera> cameras = new Dictionary<long, Camera>();

        private void Start()
        {
           cameras.Add(422L, cam422AL4);
            cameras.Add(424L, cam424AL5);
            cameras.Add(426L, cam426AL6);
            cameras.Add(428L, cam428AL8);
            cameras.Add(430L, cam430AL9);
            cameras.Add(432L, cam432AL10);
            cameras.Add(434L, cam434AL11);
            cameras.Add(436L, cam436AL12);
            cameras.Add(438L, cam438AL13);
            cameras.Add(440L, cam440AL14);
            cameras.Add(442L, cam442AL15);
            cameras.Add(444L, cam444AL16);
            cameras.Add(446L, cam446AL7);
            cameras.Add(448L, cam448AL18);
            cameras.Add(450L, cam450AL19);

            cameras.Add(1L, Узел_стыковки);
            cameras.Add(2L, Шибер);
            cameras.Add(3L, Рольганг_для_подачи_ВТУК);
            cameras.Add(4L, Установка_разборки_магазина_с_твэлами);
            cameras.Add(5L, Установка_позиционирования_твэла_по_координатам);
            cameras.Add(6L, Установка_сборки_пучка_твэлов);
            cameras.Add(7L, Автооператор);
            cameras.Add(8L, Кантователь);
            cameras.Add(9L, Установка_крепления_головок_на_каркасе_ТВС);
            cameras.Add(10L, Координатный_некопирующий_манипулятор);
            cameras.Add(11L, Установка_контроля_герметичности_твэлов_в_ТВС);
            cameras.Add(12L, Установка_контроля_массы_ТВС);
            cameras.Add(13L, Установка_контроля_геометрии_и_внешнего_вида_ТВС);
            cameras.Add(14L, Буферное_хранилище);
            cameras.Add(15L, Шибер_загрузки_ТВС_в_пенал);

            cameras.Add(16L, Установка_отделения_хвостовика_ТВС);
            cameras.Add(17L, Установка_координатная);
            cameras.Add(18L, Установка_выталкивания_твэла);
            cameras.Add(19L, Установка_вытягивания_твэла);
            cameras.Add(20L, Установка_перемещения_хвостовика_ТВС);
            cameras.Add(21L, Установка_транспортная);
            cameras.Add(22L, Установка_удаления_продуктов_отделения);
            cameras.Add(23L, Установка_контроля_поверхностной_загрязненности_ТВС);

            cameras.Add(24L, Установка_снятия_крышки_с_пенала);
            cameras.Add(25L, Установка_считывания_маркировки_и_контроля_дозиметрических_характеристик);
            cameras.Add(26L, Тележка_передаточная_для_транспортировки_запеналенной_ТВС);
            cameras.Add(27L, Кран_манипулятор_для_перегрузки_пенала_с_ТВС);

            foreach (var item in cameras)
            {
                item.Value.enabled = false;
            }

            currentCamera = cameras[422L];
            currentCamera.enabled = true;

            //_test = new ViewData("", -1);
            //SetData(_test);
            //DataUpdatedHandler();
        }

        private ViewData previousData;

        public void DataUpdatedHandler()
        {            
        }

        public void SetData(Data dataToSet)
        {
            ViewData data = dataToSet as ViewData;
            previousData = data;
        }

        private Camera currentCamera;

    //    private List<long> values = new List<long>(new long[] {422L, 424L, 426L, 428L, 430L, 432L,
    //434L, 436L, 438L, 440L, 442L,
    //444L, 446L, 448L, 450L});

    //    int count = -1;


        //private ViewData _test;

        //private void Update()
        //{
        //    if (Time.time % 4 < 0.012f)
        //    {
        //        count++;
        //        if (count == values.Count)
        //        {
        //            count = 0;
        //        }

        //        _test.CameraNumber = values[count];


        //    }

        //    UpdateControllingParameters();
        //}

        public void UpdateControllingParameters()
        {

            if (previousData == null)
            {
                return;
            }

            if(cameras.ContainsKey(previousData.CameraNumber))
            {
                currentCamera.enabled = false;
                currentCamera = cameras[previousData.CameraNumber];
                currentCamera.enabled = true;
            }
            

            //if (previousData.CameraNumber == 1211L)
            //{
            //    cam1211.enabled = true;
            //    cam1212.enabled = false;
            //    cam1213.enabled = false;
            //}

            //if (previousData.CameraNumber == 1212L)
            //{
            //    cam1211.enabled = false;
            //    cam1212.enabled = true;
            //    cam1213.enabled = false;
            //}

            //if (previousData.CameraNumber == 1213L)
            //{
            //    cam1211.enabled = false;
            //    cam1212.enabled = false;
            //    cam1213.enabled = true;
            //}

            //if (previousData.CameraNumber == 1221L)
            //{
            //    cam1221.enabled = true;
            //    cam1222.enabled = false;                
            //}

            //if (previousData.CameraNumber == 1222L)
            //{
            //    cam1221.enabled = false;
            //    cam1222.enabled = true;
            //}



            //if (previousData.CameraNumber == 1301L)
            //{
            //    cam1301.enabled = true;
            //    cam1302.enabled = false;
            //    cam1303.enabled = false;
            //    cam1304.enabled = false;
            //    cam1331.enabled = false;
            //    cam1332.enabled = false;
            //}

            //if (previousData.CameraNumber == 1302L)
            //{
            //    cam1301.enabled = false;
            //    cam1302.enabled = true;
            //    cam1303.enabled = false;
            //    cam1304.enabled = false;
            //    cam1331.enabled = false;
            //    cam1332.enabled = false;
            //}

            //if (previousData.CameraNumber == 1303L)
            //{
            //    cam1301.enabled = false;
            //    cam1302.enabled = false;
            //    cam1303.enabled = true;
            //    cam1304.enabled = false;
            //    cam1331.enabled = false;
            //    cam1332.enabled = false;
            //}

            //if (previousData.CameraNumber == 1304L)
            //{
            //    cam1301.enabled = false;
            //    cam1302.enabled = false;
            //    cam1303.enabled = false;
            //    cam1304.enabled = true;
            //    cam1331.enabled = false;
            //    cam1332.enabled = false;
            //}

            //if (previousData.CameraNumber == 1331L)
            //{
            //    cam1301.enabled = false;
            //    cam1302.enabled = false;
            //    cam1303.enabled = false;
            //    cam1304.enabled = false;
            //    cam1331.enabled = true;
            //    cam1332.enabled = false;
            //}

            //if (previousData.CameraNumber == 1332L)
            //{
            //    cam1301.enabled = false;
            //    cam1302.enabled = false;
            //    cam1303.enabled = false;
            //    cam1304.enabled = false;
            //    cam1331.enabled = false;
            //    cam1332.enabled = true;
            //}

            //if (previousData.CameraNumber == 1501L)
            //{
            //    cam1501.enabled = true;
            //    cam1502.enabled = false;
            //    cam1503.enabled = false;                
            //}

            //if (previousData.CameraNumber == 1502L)
            //{
            //    cam1501.enabled = false;
            //    cam1502.enabled = true;
            //    cam1503.enabled = false;               
            //}

            //if (previousData.CameraNumber == 1503L)
            //{
            //    cam1501.enabled = false;
            //    cam1502.enabled = false;
            //    cam1503.enabled = true;
            //}

        }
    }
}
