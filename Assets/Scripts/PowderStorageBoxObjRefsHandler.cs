﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using Assets.Scripts.Interfaces;

namespace Assets.Scripts
{
    public class PowderStorageBoxObjRefsHandler : RefsHandler
    {

        public TruckController truck;
        public PowderStorageContainer container1;
        public PowderStorageContainer container2;
        public PowderStorageContainer container3;
        public NestGrabHand hand1;
        public NestGrabHand hand2;
        public NestGrabHand hand3;
        public NestGrabHand hand4;
        public ClampController granulaClamps;
        public ClampController samplingClamps;
        public Door gate;
        public BucksContainer bucksContainer;
        public GrabHandController grabHand;
        public TableRodController tableRod;
        public RotatingTable rotTable;
        public ZinkContainerLifter zinkLifter;
        public SampleIntakingCarriage samplingCarriage;
        public LightController lightContr;
        public ViewSelector viewContr;
     
        
        public override IController GetControllerByEntityID(int entityID)
        {
            return PowderStorageMapper.numberTypes[entityID];
        }

        public override IEnumerable<IController> GetControllers()
        {
            PowderStorageMapper.numberTypes.Add(10, lightContr);
            PowderStorageMapper.numberTypes.Add(11, viewContr);
            PowderStorageMapper.numberTypes.Add(12, grabHand);
            PowderStorageMapper.numberTypes.Add(13, tableRod);
            PowderStorageMapper.numberTypes.Add(14, rotTable);
            PowderStorageMapper.numberTypes.Add(15, zinkLifter);
            PowderStorageMapper.numberTypes.Add(16, samplingCarriage);
            PowderStorageMapper.numberTypes.Add(17, bucksContainer);
            PowderStorageMapper.numberTypes.Add(1, truck);
            PowderStorageMapper.numberTypes.Add(4, container1);
            PowderStorageMapper.numberTypes.Add(5, hand1);
            PowderStorageMapper.numberTypes.Add(6, hand2);
            PowderStorageMapper.numberTypes.Add(7, hand3);
            PowderStorageMapper.numberTypes.Add(8, hand4);
            PowderStorageMapper.numberTypes.Add(2, granulaClamps);
            PowderStorageMapper.numberTypes.Add(3, samplingClamps);
            PowderStorageMapper.numberTypes.Add(9, gate);
            PowderStorageMapper.numberTypes.Add(18, container2);
            PowderStorageMapper.numberTypes.Add(19, container3);

            List<IController> allControllers = new List<IController>();
            allControllers.Add(grabHand);
            allControllers.Add(tableRod);
            allControllers.Add(rotTable);
            allControllers.Add(zinkLifter);
            allControllers.Add(samplingCarriage);
            allControllers.Add(truck);
            allControllers.Add(container1);
            allControllers.Add(container2);
            allControllers.Add(container3);
            allControllers.Add(hand1);
            allControllers.Add(hand2);
            allControllers.Add(hand3);
            allControllers.Add(hand4);
            allControllers.Add(granulaClamps);
            allControllers.Add(samplingClamps);
            allControllers.Add(gate);
            allControllers.Add(lightContr);
            allControllers.Add(viewContr);
            allControllers.Add(bucksContainer);

             

            return allControllers;
        }
    }
}
