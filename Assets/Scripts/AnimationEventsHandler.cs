﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Scripts
{
    public class AnimationEventsHandler: MonoBehaviour
    {

        public GameObject LogementOnTvelAssembler;
        public GameObject LogementOnTitler;
        public GameObject LogementForAutoOperator;
        public GameObject LogementOnCoordinateTable;
        public GameObject LogementTaili;
        public GameObject GrabberTaili;
        public GameObject PusherTvel;
        public GameObject PullerTvel;
        public GameObject TransportingTvel;
        public GameObject RolgangTvel;
        public GameObject ManipulatorTVS;
        public GameObject TVSInPenal;

        //private AnimationProcessHolder processHolder;

        private Dictionary<string, Action<AnimationProcessHolder>> namedDelegates = new Dictionary<string, Action<AnimationProcessHolder>>();

        private void Awake()
        {
            //processHolder = this.gameObject.GetComponent<AnimationProcessHolder>();
            namedDelegates.Add("TakeTVSFromTvelBeamAssembler", TakeTVSFromTvelBeamAssembler);
            namedDelegates.Add("PutTvsOnCoordinateTable", PutTvsOnCoordinateTable);            
            namedDelegates.Add("TakeTvsFromCoordinateTable", TakeTvsFromCoordinateTable);
            namedDelegates.Add("PutTVSOnTvelBeamAssembler", PutTVSOnTvelBeamAssembler);
            namedDelegates.Add("PutTVSOnTitler", PutTVSOnTitler);
            namedDelegates.Add("TakeEmptyLogementFromTitler", TakeEmptyLogementFromTitler);
            namedDelegates.Add("TVSFirstArrivedOnAssembler", TVSFirstArrivedOnAssembler);
            namedDelegates.Add("TVSSecondArrivedOnAssembler", TVSSecondArrivedOnAssembler);
            namedDelegates.Add("TakeTailiFromLogement", TakeTailiFromLogement);
            namedDelegates.Add("PushTvelFromTVS", PushTvelFromTVS);
            namedDelegates.Add("PutTailiOnTheLogement", PutTailiOnTheLogement);
            namedDelegates.Add("GrabTvelByPullingMechanism", GrabTvelByPullingMechanism);
            namedDelegates.Add("TvelReachedTransportMechanism", TvelReachedTransportMechanism);
            namedDelegates.Add("TvelArrivedToRolgang", TvelArrivedToRolgang);
            namedDelegates.Add("GrabTVSFromLogement", GrabTVSFromLogement);
            namedDelegates.Add("TVSArrivedToPenal", TVSArrivedToPenal);
        }

        public Action<AnimationProcessHolder> GetDelegateByName(string functionName)
        {
            return namedDelegates[functionName];
        }

        //Reflection must be
        private void InitAllDelegates()
        {

        }

        //AutoOperator
        public void TakeTVSFromTvelBeamAssembler(AnimationProcessHolder animationHolder)
        {
            //Debug.Log("TakeTVSFromTvelBeamAssembler");

            if(!animationHolder.IsAnimationReverse)
            {
                //LogementOnTvelAssembler.SetActive(false);
                //LogementForAutoOperator.SetActive(true);
                //LogementForAutoOperator.transform.GetChild(0).gameObject.SetActive(true);
            }
            else
            {
                //LogementOnTvelAssembler.SetActive(true);
                LogementForAutoOperator.SetActive(true);
                LogementForAutoOperator.transform.GetChild(0).gameObject.SetActive(true);
            }
            
        }

        public void PutTVSOnTitler(AnimationProcessHolder animationHolder)
        {
            //Debug.Log($"PutTVSOnTitler:{this.gameObject.name}");

            if(!animationHolder.IsAnimationReverse)
            {
                LogementOnTitler.SetActive(true);
                LogementForAutoOperator.SetActive(false);
                LogementForAutoOperator.transform.GetChild(0).gameObject.SetActive(true);
            }
            else
            {
                LogementOnTitler.SetActive(false);
                LogementForAutoOperator.SetActive(true);
                LogementForAutoOperator.transform.GetChild(0).gameObject.SetActive(true);
            }
            
        }

        public void TakeEmptyLogementFromTitler(AnimationProcessHolder animationHolder)
        {
            //Debug.Log("TakeEmptyLogementFromTitler");

            if(!animationHolder.IsAnimationReverse)
            {
                LogementOnTitler.SetActive(false);
                LogementForAutoOperator.SetActive(true);
                LogementForAutoOperator.transform.GetChild(0).gameObject.SetActive(false);
            }
            else
            {
                LogementOnTitler.SetActive(true);
                LogementForAutoOperator.SetActive(false);
                LogementForAutoOperator.transform.GetChild(0).gameObject.SetActive(true);
            }
            
        }
        

        //Кладет ТВС на координатный стол
        public void PutTvsOnCoordinateTable(AnimationProcessHolder animationHolder)
        {
            if(!animationHolder.IsAnimationReverse)
            {
                LogementForAutoOperator.SetActive(false);
                LogementOnCoordinateTable.SetActive(true);
            }
            else
            {
                LogementForAutoOperator.SetActive(true);
                LogementOnCoordinateTable.SetActive(false);
            }
        }

        //Берет ТВС с координатного стола
        public void TakeTvsFromCoordinateTable(AnimationProcessHolder animationHolder)
        {
            if(!animationHolder.IsAnimationReverse)
            {
                LogementForAutoOperator.SetActive(true);
                LogementOnCoordinateTable.SetActive(false);
            }
            else
            {
                LogementForAutoOperator.SetActive(false);
                LogementOnCoordinateTable.SetActive(true);
            }
            
        }

        //Кладет ТВС с твелами на установку сборки пучков
        public void PutTVSOnTvelBeamAssembler(AnimationProcessHolder animationHolder)
        {
            if(!animationHolder.IsAnimationReverse)
            {
                LogementForAutoOperator.SetActive(false);
                LogementOnTvelAssembler.SetActive(true);
            }
            else
            {
                LogementForAutoOperator.SetActive(true);
                LogementOnTvelAssembler.SetActive(false);
            }
           
        }

        //Хвостовик забирается с ложемента
        public void TakeTailiFromLogement(AnimationProcessHolder animationHolder)
        {
            if(!animationHolder.IsAnimationReverse)
            {
                LogementTaili.SetActive(false);
                GrabberTaili.SetActive(true);
            }
            else
            {
                LogementTaili.SetActive(true);
                GrabberTaili.SetActive(false);
            }
            
        }

        //Толкание твела из ТВС
        public void PushTvelFromTVS(AnimationProcessHolder animationHolder)
        {
            if(!animationHolder.IsAnimationReverse)
            {
                PusherTvel.SetActive(true);
            }
            else
            {
                PusherTvel.SetActive(false);
            }            
        }

        //Хвостовик кладется на ложемент
        public void PutTailiOnTheLogement(AnimationProcessHolder animationHolder)
        {
            if(!animationHolder.IsAnimationReverse)
            {
                LogementTaili.SetActive(true);
                GrabberTaili.SetActive(false);
            }
            else
            {
                LogementTaili.SetActive(false);
                GrabberTaili.SetActive(true);
            }            
        }

        //Механизм вытягивания твелов забирает твел
        public void GrabTvelByPullingMechanism(AnimationProcessHolder animationHolder)
        {
            //PusherTvel.SetActive(false);
            if(!animationHolder.IsAnimationReverse)
            {
                PullerTvel.SetActive(true);
                PusherTvel.SetActive(false);
            }
            else
            {
                PusherTvel.SetActive(true);
                PullerTvel.SetActive(false);
            }            
        }

        //Механизм транспортировки получил твел
        public void TvelReachedTransportMechanism(AnimationProcessHolder animationHolder)
        {
            if(!animationHolder.IsAnimationReverse)
            {
                PullerTvel.SetActive(false);
                TransportingTvel.SetActive(true);
            }
            else
            {
                PullerTvel.SetActive(true);
                TransportingTvel.SetActive(false);
            }            
        }

        //Твел доехал до конца второй транспортной установки
        public void TvelArrivedToRolgang(AnimationProcessHolder animationHolder)
        {
            if(!animationHolder.IsAnimationReverse)
            {
                TransportingTvel.SetActive(false);
                RolgangTvel.SetActive(true);
            }
            else
            {
                TransportingTvel.SetActive(true);
                RolgangTvel.SetActive(false);
            }
            
        }

        //ТВС подъехала на тележке к манипулятору запеналивания
        public void TVSArrivedToPenal(AnimationProcessHolder animationHolder)
        {
            if (!animationHolder.IsAnimationReverse)
            {
                TVSInPenal.SetActive(true);
            }
            else
            {
                TVSInPenal.SetActive(false);
            }
                
        }

        //Захват ТВС из кантователя
        public void GrabTVSFromLogement(AnimationProcessHolder animationHolder)
        {
            if (!animationHolder.IsAnimationReverse)
            {
                LogementOnTitler.transform.GetChild(0).gameObject.SetActive(false);
                ManipulatorTVS.SetActive(true);
            }
            else
            {
                LogementOnTitler.transform.GetChild(0).gameObject.SetActive(true);
                ManipulatorTVS.SetActive(false);
            }
                
        }

        private bool IsEventHappened = false;
        private bool IsReversedEventHappened = false;


        public void TVSFirstArrivedOnAssembler(AnimationProcessHolder animationHolder)
        {
            if (!animationHolder.IsAnimationReverse)
            {
                LogementForAutoOperator.SetActive(true);
                LogementForAutoOperator.transform.GetChild(0).gameObject.SetActive(true);
                this.transform.GetChild(0).gameObject.SetActive(false);
            }
            else
            {
                LogementForAutoOperator.SetActive(false);
                LogementForAutoOperator.transform.GetChild(0).gameObject.SetActive(true);
                this.transform.GetChild(0).gameObject.SetActive(true);
            }
        }

        //TvelAssembler
        public void TVSSecondArrivedOnAssembler(AnimationProcessHolder animationHolder)
        {

            if(!animationHolder.IsAnimationReverse && !IsEventHappened)
            {
                IsEventHappened = true;
                IsReversedEventHappened = false;
                LogementForAutoOperator.SetActive(true);
                LogementForAutoOperator.transform.GetChild(0).gameObject.SetActive(true);
                this.transform.GetChild(0).gameObject.SetActive(false);
            }

            if(animationHolder.IsAnimationReverse && !IsReversedEventHappened)
            {
                IsReversedEventHappened = true;
                IsEventHappened = false;
                LogementForAutoOperator.SetActive(false);
                LogementForAutoOperator.transform.GetChild(0).gameObject.SetActive(true);
                this.transform.GetChild(0).gameObject.SetActive(true);
            }
            
        }


        public void InvokeByName(string handlerName, AnimationProcessHolder animationHolder)
        {
            Debug.Log(handlerName);

            if(namedDelegates.ContainsKey(handlerName))
            {
                namedDelegates[handlerName]?.Invoke(animationHolder);
            }
            else
            {
                Debug.Log("Invoke!!!");
            }
        }

        private void Update()
        {
            //if(processHolder.IsAnimationReverse)
            //{
            //    IsReversedBefore = true;

            //    if (IsEventHappened)
            //    {
            //        IsEventHappened = false;
            //    }
            //}

            //if(!processHolder.IsAnimationReverse && IsReversedBefore)
            //{
            //    IsReversedBefore = false;
            //    if(IsEventHappened)
            //    {
            //        IsEventHappened = false;
            //    }
            //}
        }
    }
}
