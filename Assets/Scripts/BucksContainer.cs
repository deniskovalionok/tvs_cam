﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using Assets.Scripts.DataStructures;
using Assets.Scripts.Interfaces;

namespace Assets.Scripts
{

    public enum ContainerPart
    {
        Container,
        ContainerCup,
        Bucks,
        BucksCup
    }

    public class BucksContainer : MonoBehaviour, IController
    {

        public GrabHandController grabHandController;
        public TableRodController tableRodController;
        private BucksContainerData previousData;

        //Transforms
        public Transform bucks;
        public Transform bucksCup;
        public Transform container;
        public Transform containerCup;
        //public Transform origin;

        private Transform GrabHandTr;        
        private Transform RotatingTable;
        public Transform Table;        

        private bool IsBucksInside = true;
        private bool IsContainerCuped = true;
        private bool IsBucksCuped = true;
        private bool IsContainerInside = true;
        private bool IsBucksInsideContainer = true;

        private Transform ControllingByGrabHand = null;

        private Dictionary<int, Dictionary<ContainerPart, Transform>> parentTransformsForContainer;

        private void Start()
        {
            parentTransformsForContainer = new Dictionary<int, Dictionary<ContainerPart, Transform>>();
            parentTransformsForContainer.Add(1, new Dictionary<ContainerPart, Transform>()
            {
                {ContainerPart.Bucks,GameCore.Instance.Center },
                {ContainerPart.BucksCup, bucks},
                {ContainerPart.Container, bucks },
                {ContainerPart.ContainerCup, container }
            });
            parentTransformsForContainer.Add(2, new Dictionary<ContainerPart, Transform>()
            {
                {ContainerPart.Bucks,grabHandController.GetGrabHandTrOrigin() },
                {ContainerPart.BucksCup, grabHandController.GetGrabHandTrOrigin()},
                {ContainerPart.Container, grabHandController.GetGrabHandTrOrigin() },
                {ContainerPart.ContainerCup, grabHandController.GetGrabHandTrOrigin() }
            });
            parentTransformsForContainer.Add(3, new Dictionary<ContainerPart, Transform>()
            {
                {ContainerPart.Bucks,Table },
                {ContainerPart.BucksCup, Table},
                {ContainerPart.Container,Table },
                {ContainerPart.ContainerCup, Table }
            });
            //grabHandController.GrabStateChanged += GrabHandController_GrabStateChanged;                        
        }

        private Transform previousParent;

        //private ContainerPart nearestPart;
        private Transform nearestToGrabHandPart;

        public void SetParentToGrabHand(ContainerPart? part)
        {

            if (ControllingByGrabHand != null)
            {
                //Debug.Log("ControllingByGrabHand is not null");
                return;
            }
            
            //Debug.Log("ControllingByGrabHand is null");

            if (part == ContainerPart.Container)
            {
                nearestToGrabHandPart = container;
            }
            if (part == ContainerPart.ContainerCup)
            {
                nearestToGrabHandPart = containerCup;
                //ControllingByGrabHand = containerCup;
                //containerCup.parent = GrabHandTr;
            }
            if (part == ContainerPart.Bucks)
            {
                if(IsBucksCuped)
                {
                    nearestToGrabHandPart = bucksCup;
                }
                else
                {
                    nearestToGrabHandPart = bucks;
                }

                
                //ControllingByGrabHand = bucks;
                //bucks.parent = GrabHandTr;
            }
            if (part == ContainerPart.BucksCup)
            {
                 nearestToGrabHandPart = bucksCup;                               
                //ControllingByGrabHand = bucksCup;
                //bucksCup.parent = GrabHandTr;
            }
            if(part == null)
            {
                nearestToGrabHandPart = null;
            }

            
        }

        public bool IsInCenter()
        {
            return GrabHandTr != null;
        }

        public bool IsCompleted()
        {
            return IsBucksCuped & IsBucksInside & IsContainerCuped & IsContainerInside & IsBucksInsideContainer;
        }

        private double grabState;

        private void GrabHandController_GrabStateChanged(double grabState)
        {
            Debug.Log(grabState);
            this.grabState = grabState;

            if(grabState == 0)  
            {
                if(GrabHandTr!=null)
                {
                    //Debug.Log("GrabTR is not null");

                    //Debug.Log($"IsBucksInside: {IsBucksInside}; IsBucksInsideContainer: {IsBucksInsideContainer}; IsBucksCuped: {IsBucksCuped};");

                    if (IsContainerCuped & IsBucksInsideContainer & IsBucksCuped)
                    {
                        this.transform.parent = GrabHandTr;
                        ControllingByGrabHand = this.transform;
                        //return;
                    }

                    if(!IsContainerCuped & IsBucksInsideContainer & IsBucksCuped)
                    {
                        bucksCup.parent = GrabHandTr;
                        ControllingByGrabHand = bucksCup;
                        //return;
                    }

                    if(!IsBucksInside & IsContainerInside)
                    {
                        container.parent = GrabHandTr;
                        ControllingByGrabHand = container;
                        //return;
                    }              
                    
                    if(IsBucksInside & !IsBucksInsideContainer & !IsBucksCuped)
                    {
                        bucks.parent = GrabHandTr;
                        ControllingByGrabHand = bucks;
                        //return;
                    }
                    
                }
                else
                {

                    //Debug.Log("GrabTR is null");
                    if (nearestToGrabHandPart!=null)
                    {
                        //Debug.Log("nearestToGrabHandPart is not null");
                        Debug.Log(nearestToGrabHandPart);
                        nearestToGrabHandPart.parent = grabHandController.GetGrabHandTrOrigin();
                        ControllingByGrabHand = nearestToGrabHandPart;
                    }
                }
            }
            else
            {
                

                if (ControllingByGrabHand == null)
                {
                    return;
                }

                if (GrabHandTr == null)
                {
                    //ControllingByGrabHand.parent = RotatingTable;
                    ControllingByGrabHand.parent = Table;

                    if (ControllingByGrabHand == containerCup)
                    {
                        IsContainerCuped = false;
                    }
                    if(ControllingByGrabHand == bucks)
                    {
                        IsBucksInside = false;
                        IsBucksInsideContainer = false;
                    }
                    if(ControllingByGrabHand == container)
                    {
                        IsContainerInside = false;
                    }
                    if(ControllingByGrabHand == bucksCup)
                    {
                        if(IsBucksCuped)
                        {
                            IsBucksInside = false;
                            IsBucksInsideContainer = false;
                        }
                    }
                    
                }
                else
                {
                    if(nearestToGrabHandPart!=null)
                    {
                        //nearestToGrabHandPart.parent = origin;

                        if(nearestToGrabHandPart == bucks)
                        {
                            IsBucksInside = true;
                            if (IsContainerInside)
                                IsBucksInsideContainer = true;
                        }
                        if (nearestToGrabHandPart == container)
                        {
                            IsContainerInside = true;
                        }
                        if (nearestToGrabHandPart == containerCup)
                        {
                            IsContainerCuped = true;
                        }
                        if (nearestToGrabHandPart == bucksCup)
                        {
                            if(IsBucksCuped)
                            {
                                IsBucksInside = true;
                                if (IsContainerInside)
                                    IsBucksInsideContainer = true;
                            }
                            else
                            {
                                IsBucksCuped = true;
                            }
                            
                        }
                    }

                    if(RotatingTable == null)
                    {
                        ControllingByGrabHand.parent = GameCore.Instance.Center;
                    }
                }

                
                ControllingByGrabHand = null;

            }
            
        }

        private bool IsSubscribed = false;

        private void Update()
        {            
            //if(GrabHandTr!=null&&RotatingTable!=null)
            //{
            //    //Debug.Log("Hand and table are not null!");
            //    if (!IsSubscribed)
            //    {
            //        //Debug.Log("Subscribe!");
            //        tableRodController.RotatedByRod += TableRodController_RotatedByRod;
            //        IsSubscribed = true;
            //    }                
            //}
            //else
            //{
            //    //Debug.Log("Hand or table are null!");
            //    if (IsSubscribed)
            //    {
            //        //Debug.Log("Unsubscribe!");
            //        tableRodController.RotatedByRod -= TableRodController_RotatedByRod;
            //        IsSubscribed = false;
            //        clockwiseRotationCount = 0;
            //        counterClockwiseRotationCount = 0;
            //    }                
            //}
        }

        private int clockwiseRotationCount;
        private int counterClockwiseRotationCount;

        private void TableRodController_RotatedByRod(double direction)
        {
            //Debug.Log(grabState);
            //if (this.grabState != 0)
            //    return;

            //if(direction == 0)
            //{
            //    clockwiseRotationCount += 1;
            //}
            //else
            //{
            //    counterClockwiseRotationCount += 1;
            //}

            //int difference = clockwiseRotationCount - counterClockwiseRotationCount;
            ////Debug.Log($"Difference:{difference}");
            //if(difference >=24)
            //{
                
            //    if(ControllingByGrabHand == bucksCup)
            //    {
            //        Debug.Log("BucksCup!!!");
            //        bucks.parent = Table;
            //        IsBucksInside = true;
            //        IsBucksCuped = false;

            //    }
            //    if(ControllingByGrabHand == this.transform)
            //    {
            //        Debug.Log("ContainerCup!!!");
            //        origin.parent = Table;
            //        ControllingByGrabHand.parent = GameCore.Instance.Center;
            //        containerCup.parent = grabHandController.GetGrabHandTrOrigin();
            //        ControllingByGrabHand = containerCup;
            //    }
                                
            //}

            //if(difference<=-24)
            //{
            //    //Логика прикручивания

            //    if (ControllingByGrabHand == bucksCup)
            //    {
            //        bucks.parent = ControllingByGrabHand;
            //        IsBucksCuped = true;
            //    }

            //    if(ControllingByGrabHand == containerCup)
            //    {
            //        containerCup.parent = origin;                    
            //        IsContainerCuped = true;
            //        origin.parent = this.transform;
            //        this.transform.parent = grabHandController.GetGrabHandTrOrigin();
            //        ControllingByGrabHand = this.transform;
            //    }

                
            //}
        }

        private void OnTriggerEnter(Collider other)
        {
            if(other.gameObject.CompareTag("Hand"))
            {
                GrabHandTr = grabHandController.GetGrabHandTrOrigin();                
            }

            if(other.gameObject.CompareTag("Table"))
            {
                Debug.Log("table!!!");
                RotatingTable = tableRodController.table;
            }
        }

        private void OnTriggerExit(Collider other)
        {
            if (other.gameObject.CompareTag("Hand"))
            {            
                GrabHandTr = null;
            }
            if (other.gameObject.CompareTag("Table"))
            {
                RotatingTable = null;
            }
        }

        public void SetData(Data dataToSet)
        {
            previousData = (BucksContainerData)dataToSet;
        }

        public void UpdateControllingParameters()
        {
            SetContainerPartsTransforms((int)previousData.BucksState, ContainerPart.Bucks);
            SetContainerPartsTransforms((int)previousData.BucksCupState, ContainerPart.BucksCup);
            SetContainerPartsTransforms((int)previousData.InnerContainerState, ContainerPart.Container);
            SetContainerPartsTransforms((int)previousData.InnerContainerCupState, ContainerPart.ContainerCup);
        }

        private void SetContainerPartsTransforms(int state, ContainerPart part)
        {
            Transform child = null;

            if (state == 0)
                return;

            if(part == ContainerPart.Bucks)
            {
                child = bucks;
            }
            if(part == ContainerPart.BucksCup)
            {
                child = bucksCup;
            }
            if(part == ContainerPart.Container)
            {
                child = container;
            }
            if(part == ContainerPart.ContainerCup)
            {
                child = containerCup;
            }

            if(child!=null)
            {
                child.parent = parentTransformsForContainer[state][part];
            }

        }

        public void DataUpdatedHandler()
        {
            
        }
    }
}
