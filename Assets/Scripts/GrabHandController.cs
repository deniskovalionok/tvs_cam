﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using Assets.Scripts.Interfaces;
using Assets.Scripts.DataStructures;

namespace Assets.Scripts
{


    public class GrabHandController: MonoBehaviour, IController
    {
        public Transform PnevmoMailOrigin;
        public Transform GrabHandRod;
        public Transform VerticalRod;
        public Transform GrabHand;

        public Transform p1;
        public Transform p2;
        public Transform p3;
        public float maxRodMoving;

        private Vector3 distance;
        private Vector3 LeftThreshold;
        private Vector3 RightThreshold;
        private Vector3 MiddleThreshold;
        private float vertcalRodOriginY;
        private float realAngle;
        private float smallAngle;
        private float bigAngle;

        private float multiple;
        private float f;
        private float s;

        private Quaternion originRotation;
        private Quaternion workRotation;
        private Quaternion targetRot;
        private float previousRealTime;

        public Transform LeftThresholdOb;

        public event Action<double> GrabStateChanged;
        

        private void Start()
        {
            
            originRotation = GrabHand.localRotation;

            vertcalRodOriginY = VerticalRod.localPosition.y;

            //LeftThresholder = GrabHandRod.localPosition;
            //For Test
            LeftThreshold = LeftThresholdOb.localPosition;
            //For Test
            Vector3 direction = (PnevmoMailOrigin.localPosition - p1.localPosition).normalized;
            RightThreshold = LeftThreshold + direction * (maxRodMoving / 1000);
            MiddleThreshold = LeftThreshold + direction * 0.105f;
            //GrabHandRod.localPosition = LeftThresholder + (direction * 0.104f);

            //var g = GameObject.CreatePrimitive(PrimitiveType.Sphere);
            //g.transform.parent = GameCore.Instance.Center;
            //g.transform.localScale = Vector3.one * 0.1f;
            //g.transform.localPosition = LeftThresholder + (direction*0.104f);
            //For Test
            //GrabHandRod.localPosition = RightThresholder;

            distance = GetDistance();
            //For Test
            //var g = GameObject.CreatePrimitive(PrimitiveType.Sphere);
            //g.transform.parent = GameCore.Instance.Center;
            //g.transform.localScale = Vector3.one * 0.1f;
            //g.transform.localPosition = RightThresholder;
            smallAngle = Vector3.Angle(p1.localPosition - GrabHand.localPosition, p2.localPosition - GrabHand.localPosition);
            bigAngle = Vector3.Angle(p2.localPosition - GrabHand.localPosition, PnevmoMailOrigin.localPosition - GrabHand.localPosition);
            //Debug.Log(bigAngle);
            f = (Vector3.Angle(p1.localPosition - GrabHand.localPosition, PnevmoMailOrigin.localPosition - GrabHand.localPosition) / 140);         
            s = (Vector3.Angle(PnevmoMailOrigin.localPosition - GrabHand.localPosition, PnevmoMailOrigin.localPosition - RightThreshold)/140);            
            multiple = f / s;

            _previousVec = GrabHandRod.localPosition;

            previousRealTime = Time.realtimeSinceStartup;

            _prData = new GrabHandData("", "", "", "", "", -1);
            _prData.RodXCoord = 140;
            _prData.RodZCoord = 200;
            _prData.RodGrabState = 1;
            SetData(_prData);
            //StartCoroutine(TransportingTo());
            //StartCoroutine(MoveRodRight(4, null, 1));
        }

        private Vector3 _previousVec;

        private bool State = false;
        private float origRodDist;
        private float rodDist;

        public float val = 0;

        private void Update()
        {

            //GrabHandRod.localPosition = (LeftThreshold + (RightThreshold - LeftThreshold).normalized * val);

            //CalculateMultipleKoeff();
            //Debug.Log(realAngle);
            //return;

            //Vector3 currentDistance = GetDistance();
            //float angle = Vector3.SignedAngle(distance, currentDistance, Vector3.down);
            //Debug.Log(angle);
            //Debug.Log(multiple);
            //distance = GetDistance();
            //GrabHand.localRotation *= Quaternion.AngleAxis(angle*multiple, Vector3.down);
            //Debug.Log(multiple);
            //Vector3 currentDistance = GetDistance();
            //float sign = Math.Sign(Vector3.SignedAngle(distance, currentDistance, Vector3.down));
            //float difference = Vector3.Distance(_previousVec, GrabHandRod.localPosition);/*(_previousVec - GrabHandRod.localPosition).magnitude;*/
            ////Debug.Log($"Diff: {difference}");
            //GrabHand.localRotation *= Quaternion.AngleAxis(difference * multiple*1000*sign, Vector3.down);
            //_previousVec = GrabHandRod.localPosition;
            //distance = GetDistance();

            //if(Input.GetKey(KeyCode.L))
            //{
            //    GrabHandRod.localPosition = LeftThresholder;
            //}

            //if (Input.GetKey(KeyCode.R))
            //{
            //    GrabHandRod.localPosition = RightThresholder;
            //}

            //if (Input.GetKey(KeyCode.M))
            //{
            //    GrabHandRod.localPosition = LeftThresholder + (RightThresholder - LeftThresholder).normalized * 0.105f;
            //}

            //if (Input.GetKey(KeyCode.D))
            //{                

            //    if(Math.Abs(Vector3.Distance(GrabHandRod.localPosition, /*LeftThresholder + (PnevmoMailOrigin.localPosition - p1.localPosition).normalized * 105 / 1000*/ RightThreshold))>0.002f)
            //    {
            //        GrabHandRod.localPosition = Vector3.MoveTowards(GrabHandRod.localPosition, /*LeftThresholder + (PnevmoMailOrigin.localPosition - p1.localPosition).normalized * 105 / 1000*/  RightThreshold, Time.deltaTime);
            //    }
            //    else
            //    {
            //        Debug.Log(Math.Abs(Vector3.Distance(GrabHandRod.localPosition, RightThreshold)));
            //    }
            //}

            //if (Input.GetKey(KeyCode.A))
            //{
            //    if (Math.Abs(Vector3.Distance(GrabHandRod.localPosition, LeftThreshold  /*LeftThresholder + (PnevmoMailOrigin.localPosition - p1.localPosition).normalized * 105 / 1000*/)) > 0.002f)
            //    {
            //        GrabHandRod.localPosition = Vector3.MoveTowards(GrabHandRod.localPosition, LeftThreshold /*LeftThresholder + (PnevmoMailOrigin.localPosition - p1.localPosition).normalized * 105 / 1000*/, Time.deltaTime);
            //    }
            //    else
            //    {
            //        Debug.Log(Math.Abs(Vector3.Distance(GrabHandRod.localPosition, LeftThreshold)));
            //    }
            //}

            //if(Input.GetKeyDown(KeyCode.U))
            //{
            //    StartCoroutine(Take(0, MoveRodRight(0, MoveDown(120, null, 3), 3), 3));
            //}

            //if(Input.GetKeyDown(KeyCode.Y))
            //{
            //    StartCoroutine(MoveUp(200, MoveRodLeft(35, Put(120, null, 3), 3), 3));
            //}
                
            //if(Input.GetKeyDown(KeyCode.T))
            //{
            //    StartCoroutine(MoveRodRight(0, Take(120, MoveRodLeft(35, Put(120, null, 3), 3), 3), 3));
            //}

            //if (Input.GetKeyDown(KeyCode.R))
            //{
            //    StartCoroutine(Take(100, MoveRodRight(0, MoveDown(120, null, 3), 3), 3));
            //}

            //if (Input.GetKeyDown(KeyCode.E))
            //{
            //    StartCoroutine(Take(100, MoveRodRight(0, Put(120, MoveRodLeft(35, null, 3), 3), 3), 3));
            //}
            //origRodDist = Vector3.Distance(GrabHandRod.localPosition, LeftThreshold);

            //if(origRodDist<0.105f)
            //{
            //    workRotation = originRotation;
            //    rodDist = origRodDist;
            //    multiple = bigAngle / 105;
            //}
            //else
            //{
            //    rodDist = origRodDist - 0.105f;
            //    workRotation = originRotation * Quaternion.AngleAxis(bigAngle, Vector3.up);
            //    multiple = smallAngle/35;
            //}



            //if (!State)
            //{


            //    if (dist < 0.105f)
            //    {
            //        multiple = bigAngle / 105;
            //    }
            //    else
            //    {
            //        State = true;
            //        originRotation = targetRot;
            //        dist = Vector3.Distance(GrabHandRod.localPosition, MiddleThreshold);
            //        multiple = smallAngle / 35;
            //    }
            //}
            //else
            //{
            //    dist = Vector3.Distance(GrabHandRod.localPosition, MiddleThreshold);


            //    if (dist < 0.105f)
            //    {
            //        multiple = bigAngle / 105;
            //    }
            //    else
            //    {
            //        State = true;
            //        originRotation = targetRot;
            //        dist = Vector3.Distance(GrabHandRod.localPosition, MiddleThreshold);
            //        multiple = smallAngle / 35;
            //    }
            //}



            //Debug.Log($"Multiple value: {multiple}");
            //Debug.Log(Vector3.Distance(GrabHandRod.localPosition, LeftThreshold));
            //float r = Time.realtimeSinceStartup;
            //targetRot = workRotation * Quaternion.AngleAxis(rodDist * multiple * 1000, Vector3.up);
            ////if(GrabHand.localRotation!=targetRot)
            ////{
            ////    GrabHand.localRotation = Quaternion.RotateTowards(GrabHand.localRotation, targetRot, 20f*(r - previousRealTime));
            ////}
            //GrabHand.localRotation = targetRot;
            //previousRealTime = r;
            //UpdateControllingParameters();
        }

        private GrabHandData _prData;

        private IEnumerator MoveDown(int height, IEnumerator process = null, int koef = 1)
        {            
            _prData.RodXVelocity = 0;
            _prData.RodZVelocity = -20 * Mathf.Abs(koef);            
            SetData(_prData);
            UpdateControllingParameters();
            while (_prData.RodZCoord > height)
            {
                _prData.RodZCoord += _prData.RodZVelocity * 0.1f;
                SetData(_prData);
                yield return new WaitForSeconds(0.1f);
            }
            _prData.RodZVelocity = 0;      
            SetData(_prData);

            if (process != null)
                StartCoroutine(process);
        }

        private IEnumerator MoveUp(int height, IEnumerator process = null, int koef = 1)
        {
            _prData.RodZVelocity = 20 * Mathf.Abs(koef);
            SetData(_prData);
            yield return new WaitForSeconds(0.1f);

            while (_prData.RodZCoord < height)
            {
                _prData.RodZCoord += _prData.RodZVelocity * 0.1f;
                SetData(_prData);
                yield return new WaitForSeconds(0.1f);
            }
            _prData.RodZVelocity = 0;
            SetData(_prData);
            yield return new WaitForSeconds(0.1f);

            if (process != null)
                StartCoroutine(process);
        }

        private IEnumerator Take(int height, IEnumerator process = null, int koef = 1)
        {
            int previousHeight = (int)_prData.RodZCoord;
            _prData.RodXVelocity = 0;
            _prData.RodZVelocity = -20 * Mathf.Abs(koef);
            //_prData.RodGrabState = 1;
            SetData(_prData);
            UpdateControllingParameters();
            while(_prData.RodZCoord > height)
            {
                _prData.RodZCoord += _prData.RodZVelocity * 0.1f;
                SetData(_prData);
                yield return new WaitForSeconds(0.1f);
            }
            _prData.RodZVelocity = 0;
            _prData.RodGrabState = 0;
            SetData(_prData);
            yield return new WaitForSeconds(0.1f);
            _prData.RodZVelocity = 20;
            SetData(_prData);
            yield return new WaitForSeconds(0.1f);

            while (_prData.RodZCoord<previousHeight)
            {
                _prData.RodZCoord += _prData.RodZVelocity * 0.1f;
                SetData(_prData);
                yield return new WaitForSeconds(0.1f);
            }
            _prData.RodZVelocity = 0;
            SetData(_prData);

            if(process!=null)
                StartCoroutine(process);
        }

        private IEnumerator Put(int height, IEnumerator process = null, int koef = 1)
        {
            int previousHeight = (int)_prData.RodZCoord;
            _prData.RodXVelocity = 0;
            _prData.RodZVelocity = -20 * Mathf.Abs(koef);
            //_prData.RodGrabState = 0;
            SetData(_prData);
            UpdateControllingParameters();
            while (_prData.RodZCoord > height)
            {
                _prData.RodZCoord += _prData.RodZVelocity * 0.1f;
                SetData(_prData);
                yield return new WaitForSeconds(0.1f);
            }
            _prData.RodZVelocity = 0;
            _prData.RodGrabState = 1;
            SetData(_prData);
            yield return new WaitForSeconds(0.1f);
            _prData.RodZVelocity = 20;
            SetData(_prData);
            yield return new WaitForSeconds(0.1f);

            while (_prData.RodZCoord < previousHeight)
            {
                _prData.RodZCoord += _prData.RodZVelocity * 0.1f;
                SetData(_prData);
                yield return new WaitForSeconds(0.1f);
            SetData(_prData);

            }
            _prData.RodZVelocity = 0;
            if (process != null)
                StartCoroutine(process);
        }

        private IEnumerator MoveRodLeft(int distance, IEnumerator process = null, int koef = 1)
        {           
            _prData.RodXVelocity = 20*Mathf.Abs(koef);
            _prData.RodZVelocity = 0;
            SetData(_prData);
            yield return new WaitForSeconds(0.1f);

            while (_prData.RodXCoord < distance)
            {
                _prData.RodXCoord += _prData.RodXVelocity * 0.1f;
                SetData(_prData);
                yield return new WaitForSeconds(0.1f);
            }
            _prData.RodXVelocity = 0;
            _prData.RodXCoord = distance;
            SetData(_prData);

            if (process != null)
                StartCoroutine(process);
        }

        private IEnumerator MoveRodRight(int distance, IEnumerator process = null, int koef = 1)
        {

            _prData.RodXVelocity = -20 * Mathf.Abs(koef);
            _prData.RodZVelocity = 0;
            SetData(_prData);
            yield return new WaitForSeconds(0.1f);

            while (_prData.RodXCoord > distance)
            {
                _prData.RodXCoord += _prData.RodXVelocity * 0.1f;
                SetData(_prData);
                yield return new WaitForSeconds(0.1f);
            }

            _prData.RodXVelocity = 0;
            _prData.RodXCoord = distance;
            SetData(_prData);

            if (process != null)
                StartCoroutine(process);
        }

        private IEnumerator TransportingTo()
        {
            //while(Math.Abs(Vector3.Distance(GrabHandRod.localPosition, LeftThresholder+(RightThresholder-LeftThresholder).normalized*0.105f)) > 0.002f)
            //{
            //    GrabHandRod.localPosition = Vector3.MoveTowards(GrabHandRod.localPosition, RightThresholder, 0.02f/10);
            //    yield return new WaitForSeconds(0.1f);
            //}

            //yield return new WaitForSeconds(2f);

            //while (Math.Abs(Vector3.Distance(GrabHandRod.localPosition, RightThreshold)) > 0.001f)
            //{
            //    GrabHandRod.localPosition = Vector3.MoveTowards(GrabHandRod.localPosition, RightThreshold, 0.02f / 10);
            //    yield return new WaitForSeconds(0.1f);
            //}

            _prData.RodXCoord = 140;
            _prData.RodZCoord = 200;
            _prData.RodXVelocity = -20;
            _prData.RodZVelocity = -20;
            _prData.RodGrabState = 1;

            SetData(_prData);
            UpdateControllingParameters();             
            

            while (true)
            {
                //if(_prData.RodXCoord>0)
                //{
                //    _prData.RodXCoord += _prData.RodXVelocity * 0.1f;
                //}

                if(_prData.RodZCoord>0)
                {
                    _prData.RodZCoord += _prData.RodZVelocity * 0.1f;
                }
                else
                {
                    _prData.RodGrabState = 0;
                    _prData.RodZVelocity = 0;
                    SetData(_prData);
                    break;
                }

                SetData(_prData);
                yield return new WaitForSeconds(0.1f);
            }

            _prData.RodZVelocity = 20;

            while(_prData.RodZCoord<200)
            {
                _prData.RodZCoord += _prData.RodZVelocity * 0.1f;
                SetData(_prData);
                yield return new WaitForSeconds(0.1f);
            }
            while(_prData.RodXCoord>0)
            {
                _prData.RodXCoord += _prData.RodXVelocity * 0.1f;
                SetData(_prData);
                yield return new WaitForSeconds(0.1f);
            }

            _prData.RodXVelocity = 0;
          
            _prData.RodZVelocity = -20;
            SetData(_prData);

            while(_prData.RodZCoord>140)
            {
                _prData.RodZCoord += _prData.RodZVelocity * 0.1f;
                SetData(_prData);
                yield return new WaitForSeconds(0.1f);
            }
            
            _prData.RodZVelocity = 0;
            SetData(_prData);


           
            //while (Math.Abs(Vector3.Distance(GrabHandRod.localPosition, LeftThreshold)) > 0.001f)
            //{
            //    GrabHandRod.localPosition = Vector3.MoveTowards(GrabHandRod.localPosition, LeftThreshold, 0.02f / 10);
            //    yield return new WaitForSeconds(0.1f);
            //}
        }

        private IEnumerator EndingProcess()
        {

            _prData.RodZVelocity = 20;

            while (_prData.RodZCoord < 200)
            {
                _prData.RodZCoord += _prData.RodZVelocity * 0.1f;
                SetData(_prData);
                yield return new WaitForSeconds(0.1f);
            }
            _prData.RodZVelocity = 0;
            _prData.RodXVelocity = 20;
            SetData(_prData);


            while (_prData.RodXCoord < 35)
            {
                _prData.RodXCoord += _prData.RodXVelocity * 0.1f;
                SetData(_prData);
                yield return new WaitForSeconds(0.1f);
            }

            _prData.RodXVelocity = 0;
            _prData.RodZVelocity = -20;
            SetData(_prData);

            while (_prData.RodZCoord > 140)
            {
                _prData.RodZCoord += _prData.RodZVelocity * 0.1f;
                SetData(_prData);
                yield return new WaitForSeconds(0.1f);
            }

            _prData.RodZVelocity = 0;
            _prData.RodGrabState = 1;
            SetData(_prData);

            _prData.RodZVelocity = 20;

            while (_prData.RodZCoord < 200)
            {
                _prData.RodZCoord += _prData.RodZVelocity * 0.1f;
                SetData(_prData);
                yield return new WaitForSeconds(0.1f);
            }
            _prData.RodZVelocity = 0;
            SetData(_prData);
        }

        private Vector3 GetDistance()
        {                   
            return PnevmoMailOrigin.localPosition - GrabHandRod.localPosition;            
        }

        private bool IsBigAngle = false;
        private bool IsSmallAngle = false;

        private void CalculateMultipleKoeff()
        {

            float curDist = Mathf.Abs(Vector3.Distance(GrabHandRod.localPosition, RightThreshold));
            if (curDist < 0.035f)
            {
                if (IsBigAngle)
                    return;
                //bigAngle = Vector3.Angle(GameCore.Instance.Center.InverseTransformPoint(p3.position) - GrabHand.localPosition, PnevmoMailOrigin.localPosition - GrabHand.localPosition);
                smallAngle = Vector3.Angle(p1.localPosition - GrabHand.localPosition, GameCore.Instance.Center.InverseTransformPoint(p3.position) - GrabHand.localPosition);
                Debug.Log(smallAngle);
                multiple = smallAngle / (0.035f*1000);
                IsBigAngle = true;
                IsSmallAngle = false;
                Debug.Log("Changed to small");
            }
            else
            {
                if (IsSmallAngle)
                    return;
                Debug.Log($"Angle:{Vector3.Angle(GameCore.Instance.Center.InverseTransformPoint(p3.position) - GrabHand.localPosition, PnevmoMailOrigin.localPosition - GrabHand.localPosition)}");
                if(Vector3.Angle(GameCore.Instance.Center.InverseTransformPoint(p3.position) - GrabHand.localPosition, PnevmoMailOrigin.localPosition - GrabHand.localPosition)<1f)
                {
                    bigAngle = Vector3.Angle(p2.localPosition - GrabHand.localPosition, GameCore.Instance.Center.InverseTransformPoint(p3.position) - GrabHand.localPosition);
                    Debug.Log((RightThreshold - LeftThreshold).normalized * 0.105f);
                    multiple = bigAngle /(Vector3.Distance(LeftThreshold + (RightThreshold-LeftThreshold).normalized *0.105f, LeftThreshold)*1000);
                }
                else
                {
                    bigAngle = Vector3.Angle(GameCore.Instance.Center.InverseTransformPoint(p3.position) - GrabHand.localPosition, PnevmoMailOrigin.localPosition - GrabHand.localPosition);
                    multiple = bigAngle / (Mathf.Abs(Vector3.Distance(GrabHandRod.localPosition, LeftThreshold)) * 1000);
                    Debug.Log("lflflf");
                }
                                
                Debug.Log(bigAngle);                
                IsSmallAngle = true;
                IsBigAngle = false;
                Debug.Log("Changed to big");
            }
            //Debug.Log($"Curr dist: {curDist}");
            //if (curDist < 0.036f)
            //{
            //    Debug.Log("S");

            //    if (IsSmallAngle)
            //        return;
                
            //    if(IsBigAngle)
            //    {
            //        realAngle = Vector3.Angle(p1.localPosition - GrabHand.localPosition, GameCore.Instance.Center.InverseTransformPoint(p3.position) - GrabHand.localPosition);
            //        multiple = realAngle / (Vector3.Angle(PnevmoMailOrigin.localPosition - GrabHandRod.localPosition, PnevmoMailOrigin.localPosition - RightThresholder));
            //        IsBigAngle = false;                    
            //    }
            //    else
            //    {
            //        realAngle = smallAngle;
            //        multiple = realAngle/ (Vector3.Angle(PnevmoMailOrigin.localPosition - (LeftThresholder + (PnevmoMailOrigin.localPosition - p1.localPosition).normalized*105/1000), PnevmoMailOrigin.localPosition - RightThresholder));
            //    }
            //    //realAngle = smallAngle;

            //    IsSmallAngle = true;

            //    //multiple = realAngle / (Vector3.Angle(PnevmoMailOrigin.localPosition - (LeftThresholder + (PnevmoMailOrigin.localPosition - p1.localPosition).normalized * Math.Abs((GrabHandRod.localPosition - LeftThresholder).magnitude)), PnevmoMailOrigin.localPosition -  RightThresholder));

            //}
            //else
            //{
            //    Debug.Log("B");

            //    if (IsBigAngle)
            //        return;

            //    if(IsSmallAngle)
            //    {
            //        realAngle = Vector3.Angle(GameCore.Instance.Center.InverseTransformPoint(p3.position) - GrabHand.localPosition, PnevmoMailOrigin.localPosition - GrabHand.localPosition);
            //        multiple = realAngle / (Vector3.Angle(PnevmoMailOrigin.localPosition - LeftThresholder, PnevmoMailOrigin.localPosition - (LeftThresholder + (PnevmoMailOrigin.localPosition - p1.localPosition).normalized * Math.Abs((GrabHandRod.localPosition - LeftThresholder).magnitude))));
            //        IsSmallAngle = false;
                    
            //    }
            //    else
            //    {
            //        realAngle = bigAngle;
            //        multiple = realAngle / (Vector3.Angle(PnevmoMailOrigin.localPosition - LeftThresholder, PnevmoMailOrigin.localPosition - (LeftThresholder + (PnevmoMailOrigin.localPosition - p1.localPosition).normalized * 105/1000)));
            //    }

            //    IsBigAngle = true;
                //realAngle = bigAngle;
                //realAngle = Vector3.Angle(GameCore.Instance.Center.InverseTransformPoint(p3.position) - GrabHand.localPosition, PnevmoMailOrigin.localPosition - GrabHand.localPosition);
                //multiple = realAngle / (Vector3.Angle(PnevmoMailOrigin.localPosition - GrabHand.localPosition, PnevmoMailOrigin.localPosition - (LeftThresholder + (PnevmoMailOrigin.localPosition - p1.localPosition).normalized * 105 / 1000)));
                //multiple = realAngle / (Vector3.Angle(PnevmoMailOrigin.localPosition - LeftThresholder, PnevmoMailOrigin.localPosition - (LeftThresholder + (PnevmoMailOrigin.localPosition - p1.localPosition).normalized * Math.Abs((GrabHandRod.localPosition - LeftThresholder).magnitude))));
            //}

        }

        private GrabHandData previousData;
        private double previousRodGrabState;


        public Transform GetGrabHandTrOrigin()
        {
            return VerticalRod;
        }

        public void SetData(Data dataToSet)
        {
            previousData = (GrabHandData)dataToSet;
        }

        public void UpdateControllingParameters()
        {
            float r = Time.realtimeSinceStartup;
            HorizontalRodMoving(r);
            VerticalRodMoving(r);            
            previousRealTime = r;
            if(previousRodGrabState!=previousData.RodGrabState)
            {
                GrabStateChanged?.Invoke(previousData.RodGrabState);                
            }
            previousRodGrabState = previousData.RodGrabState;
        }

        private void HorizontalRodMoving(float currentTime)
        {
            origRodDist = (140 - (float)previousData.RodXCoord) / 1000;
            //Debug.Log($"Orig:{origRodDist}");

            if (origRodDist < 0.105f)
            {
                workRotation = originRotation;
                rodDist = origRodDist;
                multiple = bigAngle / 105;
            }
            else
            {
                rodDist = origRodDist - 0.105f;
                workRotation = originRotation * Quaternion.AngleAxis(bigAngle, Vector3.up);
                multiple = smallAngle / 35;
            }
            
            targetRot = workRotation * Quaternion.AngleAxis(rodDist * multiple * 1000, Vector3.up);
            if (GrabHand.localRotation != targetRot)
            {
                GrabHand.localRotation = Quaternion.RotateTowards(GrabHand.localRotation, targetRot, Mathf.Abs((float)previousData.RodXVelocity)*multiple * (currentTime - previousRealTime));
                //GrabHand.localRotation = Quaternion.RotateTowards(GrabHand.localRotation, targetRot, 20 * multiple * (currentTime - previousRealTime));
            }             
            
            if(previousData.RodXVelocity == 0)
            {
                GrabHand.localRotation = targetRot;
            }
        }

        private void VerticalRodMoving(float currentTime)
        {            

            if (GameCore.Instance.IsLerp)
            {
                VerticalRod.localPosition = new Vector3(VerticalRod.localPosition.x, Mathf.Lerp(VerticalRod.localPosition.y, vertcalRodOriginY + (float)previousData.RodZCoord / 1000, Time.deltaTime), VerticalRod.localPosition.z);
            }
            else
            {                
                VerticalRod.localPosition = new Vector3(VerticalRod.localPosition.x,
                    Mathf.MoveTowards(VerticalRod.localPosition.y, vertcalRodOriginY + (float)previousData.RodZCoord / 1000, (Mathf.Abs((float)previousData.RodZVelocity) / 1000) * (currentTime - previousRealTime)),
                   VerticalRod.localPosition.z);
            }

            if (previousData.RodZVelocity == 0)
            {
                VerticalRod.localPosition = new Vector3(VerticalRod.localPosition.x, vertcalRodOriginY + (float)previousData.RodZCoord / 1000, VerticalRod.localPosition.z);
            }

        }

        public void DataUpdatedHandler()
        {
            
        }
    }
}
