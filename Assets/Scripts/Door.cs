﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Assets.Scripts.Interfaces;
using UnityEngine;
using Assets.Scripts.DataStructures;
using System.Collections;

namespace Assets.Scripts
{

    public class Door : MonoBehaviour, IController
    {

        public Transform DoorObj;        
        private DoorData previousData;

        private float previousRealTime;        

        public int ZCoord_Test;



        public void SetData(Data dataToSet)
        {
            DoorData data = dataToSet as DoorData;
            previousData = data;

        }

        //private IEnumerator UpdateData()
        //{
        //    while (true)
        //    {
        //        if (Mathf.Abs((float)_prD.ZCoord - ZCoord_Test) < 20)
        //        {
        //            _prD.ZVelocity = 0;
        //            _prD.ZCoord = ZCoord_Test;
        //        }
        //        else
        //        {
        //            _prD.ZVelocity = Mathf.Sign(ZCoord_Test - (float)_prD.ZCoord) * 20;
        //        }
              
                
        //        _prD.ZCoord+= _prD.ZVelocity/ 10;
        //        SetData(_prD);

        //        yield return new WaitForSeconds(0.1f);
        //    }
        //}

        private void Update()
        {
            //UpdateControllingParameters();
        }

        public void UpdateControllingParameters()
        {
            float r = Time.realtimeSinceStartup;
            TranslateObj(r);
            previousRealTime = r;
        }

        public void TranslateObj(float time)
        {
            //if (Mathf.Abs((float)previousData.ZVelocity - 10f) > 0.001f && (float)previousData.ZVelocity != 0)
            //{
            //    previousData.ZVelocity = 10;
            //}

            if (previousData == null)
            {
                return;
            }


            if (GameCore.Instance.IsLerp)
            {
                DoorObj.localPosition = new Vector3(DoorObj.localPosition.x, Mathf.Lerp(DoorObj.localPosition.y, (float)previousData.ZCoord / 1000, Time.deltaTime), DoorObj.localPosition.z);
            }
            else
            {

                DoorObj.localPosition = new Vector3(DoorObj.localPosition.x,
                    Mathf.MoveTowards(DoorObj.localPosition.y, (float)previousData.ZCoord / 1000, (Mathf.Abs((float)previousData.ZVelocity) / 1000) * (time - previousRealTime)),
                   DoorObj.localPosition.z);
            }            

            if (previousData.ZVelocity == 0)
            {
                DoorObj.localPosition = new Vector3(DoorObj.localPosition.x, (float)previousData.ZCoord / 1000, DoorObj.localPosition.z);
            }
        }

        public void DataUpdatedHandler()
        {            
            previousRealTime = Time.realtimeSinceStartup;
        }
    }
}
