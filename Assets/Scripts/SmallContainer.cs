﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Assets.Scripts.Interfaces;
using UnityEngine;
using Assets.Scripts.DataStructures;

namespace Assets.Scripts
{
    public class SmallContainer : MonoBehaviour, IController
    {

        private ContainerData previousData;

        public Transform smallContainerObj;        
        public Transform capObj;

        private float previousRealTime;
        private float rotateValue = 0.1f;
        private Quaternion originRotation;
        private Quaternion target;        

        private bool IsTranslationSelfControlling = true;
        private bool IsRotationSelfControlling = true;

        private void Start()
        {
            previousRealTime = Time.realtimeSinceStartup;
            originRotation = smallContainerObj.localRotation;            
        }

        public void SetData(Data dataToSet)
        {
            ContainerData actualData = (ContainerData)dataToSet;
            //Debug.Log($"{actualData.AxePosition}");                        
            previousData = actualData;
            //target = originRotation * Quaternion.AngleAxis(180 * (float)previousData.AxePosition, Vector3.left);
            //previousRealTime = Time.realtimeSinceStartup;
        }

        public void UpdateControllingParameters()
        {
            //Translate and rotate object by speed            
            float r = Time.realtimeSinceStartup;
            if(smallContainerObj!=null)
            {
                TranslateObject(smallContainerObj, r, target);
            }
            
            previousRealTime = r;
        }

        private void TranslateObject(Transform container, float r, Quaternion target)
        {

            

            if (IsTranslationSelfControlling)
            {

                if (GameCore.Instance.IsLerp)
                {
                    container.parent.localPosition = new Vector3(Mathf.Lerp(container.parent.localPosition.x, (float)previousData.XCoord / 1000, Time.deltaTime), Mathf.Lerp(container.parent.localPosition.y, (float)previousData.ZCoord / 1000, Time.deltaTime), Mathf.Lerp(container.parent.localPosition.z, (float)previousData.YCoord / 1000, Time.deltaTime));
                }
                else
                {
                    container.parent.localPosition = new Vector3(Mathf.MoveTowards(container.parent.localPosition.x, (float)previousData.XCoord / 1000, (Mathf.Abs((float)previousData.XVelocity) / 1000) * (r - previousRealTime)),
                    Mathf.MoveTowards(container.parent.localPosition.y, (float)previousData.ZCoord / 1000, (Mathf.Abs((float)previousData.ZVelocity) / 1000) * (r - previousRealTime)),
                    Mathf.MoveTowards(container.parent.localPosition.z, (float)previousData.YCoord / 1000, (Mathf.Abs((float)previousData.YVelocity) / 1000) * (r - previousRealTime)));
                }

               

            }

            if (IsRotationSelfControlling)
            {

                if (container.localRotation != target)
                {
                    container.localRotation = Quaternion.RotateTowards(container.localRotation, target, 180 * /*rotateValue*/ Mathf.Abs((float)previousData.RotatingVelocity) * (r - previousRealTime));
                }

            }

            //previousRealTime = r;

            if (previousData.XVelocity == 0)
            {
                container.parent.localPosition = new Vector3((float)previousData.XCoord / 1000, container.parent.localPosition.y, container.parent.localPosition.z);
            }
            if (previousData.YVelocity == 0)
            {
                container.parent.localPosition = new Vector3(container.parent.localPosition.x, container.parent.localPosition.y, (float)previousData.YCoord / 1000);
            }
            if (previousData.ZVelocity == 0)
            {
                container.parent.localPosition = new Vector3(container.parent.localPosition.x, (float)previousData.ZCoord / 1000, container.parent.localPosition.z);
            }

            if (previousData.RotatingVelocity == 0)
            {
                container.localRotation = target;
            }
        }

        public void DataUpdatedHandler()
        {
            target = originRotation * Quaternion.AngleAxis(180 * (float)previousData.AxePosition, Vector3.left);
            previousRealTime = Time.realtimeSinceStartup;
        }
    }
}
