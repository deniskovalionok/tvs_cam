﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Assets.Scripts.DataStructures;
using Assets.Scripts.Interfaces;
using UnityEngine;
using System.Collections;

namespace Assets.Scripts
{
    public class ClampController : MonoBehaviour, IController
    {

        public Transform FirstHand;
        public Transform SecondHand;

        private ClampsData previousData;
        private float previousRealTime;
        private float rotateValue = 10;
        private ClampsData _prD;

        private Quaternion firstOriginRot;
        private Quaternion secondOriginRot;
        private Quaternion firstTarget;
        private Quaternion secondTarget;

        public float AxePos_Test;

        private void Start()
        {
            firstOriginRot = FirstHand.localRotation;
            secondOriginRot = SecondHand.localRotation;
            previousRealTime = Time.realtimeSinceStartup;

            //_prD = new ClampsData("", "", -1);
            //_prD.AxePosition = 0;
            //AxePos_Test = (float)_prD.AxePosition;           
            //_prD.RotateVelocity = 0.1f;
            //SetData(_prD);
            //UpdateControllingParameters();
            //StartCoroutine(UpdateData());

        }

        private IEnumerator UpdateData()
        {
            while (true)
            {

                if (Mathf.Abs((float)_prD.AxePosition - AxePos_Test) < 0.01f)
                {
                    Debug.Log("oeoeoe2");
                    _prD.RotateVelocity = 0;
                    _prD.AxePosition = AxePos_Test;
                }
                else
                {
                    Debug.Log("oeoeoe1");
                    _prD.RotateVelocity = Mathf.Sign(AxePos_Test - (float)_prD.AxePosition) * 10;
                }

                //Debug.Log("kfkf");
                _prD.AxePosition += _prD.RotateVelocity / 10;
                Debug.Log(_prD.AxePosition);
                SetData(_prD);

                yield return new WaitForSeconds(0.1f);
            }
        }

        private void Update()
        {
            //UpdateControllingParameters();
        }

        public void SetData(Data dataToSet)
        {
            previousData = (ClampsData)dataToSet;
            //previousRealTime = Time.realtimeSinceStartup;
            //firstTarget = firstOriginRot * Quaternion.AngleAxis(20 * (1-(float)previousData.AxePosition), Vector3.right);
            //secondTarget = secondOriginRot * Quaternion.AngleAxis(20 * (1-(float)previousData.AxePosition), Vector3.left);
        }

        public void UpdateControllingParameters()
        {
            float r = Time.realtimeSinceStartup;
            RotateObj(r);
            previousRealTime = r;
        }

        private void RotateObj(float time)
        {

            if(previousData == null)
            {
                return;
            }

            if (FirstHand.localRotation != firstTarget)
            {
                FirstHand.localRotation = Quaternion.RotateTowards(FirstHand.localRotation, firstTarget, 20 * rotateValue/* Mathf.Abs((float)previousData.RotatingVelocity)*/* (time - previousRealTime));
            }

            if (SecondHand.localRotation != secondTarget)
            {
                SecondHand.localRotation = Quaternion.RotateTowards(SecondHand.localRotation, secondTarget, 20 * rotateValue/* Mathf.Abs((float)previousData.RotatingVelocity)*/* (time - previousRealTime));
            }
            
        }

        public void DataUpdatedHandler()
        {
            previousRealTime = Time.realtimeSinceStartup;
            firstTarget = firstOriginRot * Quaternion.AngleAxis(20 * (1 - (float)previousData.AxePosition), Vector3.right);
            secondTarget = secondOriginRot * Quaternion.AngleAxis(20 * (1 - (float)previousData.AxePosition), Vector3.left);
        }
    }
}
