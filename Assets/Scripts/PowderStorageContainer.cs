﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using Assets.Scripts.Interfaces;
using Assets.Scripts.DataStructures;
using System.Collections;

namespace Assets.Scripts
{
    public class PowderStorageContainer: MonoBehaviour, IController
    {

        public Transform containerTranslatingObj;
        public Transform containerRotatingObj;

        private ContainerData previousData;
        private float previousRealTime;

        private Quaternion originRotation;
        private Quaternion target;
        private float rotateValue = 10;

        private ContainerData _prD;

        public int XCoordValue_Test;
        public int YCoordValue_Test;

        private void Start()
        {
            previousRealTime = Time.realtimeSinceStartup;
            originRotation = containerRotatingObj.rotation;
            //_prD = new ContainerData("", "", "", "", "", "", "", "", -1);

            //_prD.XCoord = 5157;
            //_prD.YCoord = 150;
            //XCoordValue_Test = (int)_prD.XCoord;
            //YCoordValue_Test = (int)_prD.YCoord;
            //_prD.XVelocity = 0;
            //_prD.YVelocity = 0;
            //SetData(_prD);
            //UpdateControllingParameters();
            //StartCoroutine(UpdateData());
        }

        private IEnumerator UpdateData(bool direction)
        {
            //while (true)
            //{
            //    //if (Mathf.Abs((float)_prD.XCoord - XCoordValue_Test) < 20)
            //    //{
            //    //    _prD.XVelocity = 0;
            //    //    _prD.XCoord = XCoordValue_Test;
            //    //}
            //    //else
            //    //{
            //    //    _prD.XVelocity = Mathf.Sign(XCoordValue_Test - (float)_prD.XCoord) * 20;
            //    //}

            //    //if (Mathf.Abs((float)_prD.YCoord - YCoordValue_Test) < 20)
            //    //{
            //    //    _prD.YVelocity = 0;
            //    //    _prD.YCoord = YCoordValue_Test;
            //    //}
            //    //else
            //    //{
            //    //    _prD.YVelocity = Mathf.Sign(YCoordValue_Test - (float)_prD.YCoord) * 20;
            //    //}


            //    //_prD.XCoord += _prD.XVelocity / 10;
            //    //_prD.YCoord += _prD.YVelocity / 10;
            //    //SetData(_prD);

            //    yield return new WaitForSeconds(0.1f);
            //}

            if (direction)
            {
                _prD.RotatingVelocity = 0.089f;
                
            }
            else
            {
                _prD.RotatingVelocity = -0.089f;
            }

            DataUpdatedHandler();

            while (true)
            {               

                if (direction)
                {
                    if (_prD.AxePosition < 1)
                    {
                        _prD.AxePosition += 0.1f * 0.1f;
                    }
                    else
                    {                        
                        break;
                    }

                }
                else
                {
                    if (_prD.AxePosition > 0)
                    {
                        _prD.AxePosition += -0.1f * 0.1f;
                    }
                    else
                    {
                        break;
                    }
                }

                DataUpdatedHandler();
                yield return new WaitForSeconds(0.1f);
            }


            _prD.RotatingVelocity = 0;



            

        }

        private void Update()
        {
            //UpdateControllingParameters();

            //if(Input.GetKeyDown(KeyCode.A))
            //{
            //    StartCoroutine(UpdateData(true));
            //}

            //if (Input.GetKeyDown(KeyCode.B))
            //{
            //    StartCoroutine(UpdateData(false));
            //}
        }

        public void SetData(Data dataToSet)
        {
            previousData = (ContainerData)dataToSet;
            //previousRealTime = Time.realtimeSinceStartup;
            //target = originRotation * Quaternion.AngleAxis(180*(float)previousData.AxePosition, Vector3.forward);
        }

        public void UpdateControllingParameters()
        {
            float r = Time.realtimeSinceStartup;
            TranslateObj(r);
            RotateObj(r);
            previousRealTime = r;
        }

        public void TranslateObj(float time)
        {
            if(previousData == null)
            {
                return;
            }

            if (GameCore.Instance.IsLerp)
            {
                containerTranslatingObj.localPosition = new Vector3(Mathf.Lerp(containerTranslatingObj.localPosition.x, (float)previousData.XCoord / 1000, Time.deltaTime), Mathf.Lerp(containerTranslatingObj.localPosition.y, (float)previousData.YCoord / 1000, Time.deltaTime), containerTranslatingObj.localPosition.z);
            }
            else
            {
                containerTranslatingObj.localPosition = new Vector3(Mathf.MoveTowards(containerTranslatingObj.localPosition.x, (float)previousData.XCoord / 1000, (Mathf.Abs((float)previousData.XVelocity) / 1000) * (time - previousRealTime)),
                        Mathf.MoveTowards(containerTranslatingObj.localPosition.y, (float)previousData.YCoord / 1000, (Mathf.Abs((float)previousData.YVelocity) / 1000) * (time - previousRealTime)),
                        containerTranslatingObj.localPosition.z);
            }

            if (previousData.XVelocity == 0)
            {                
                containerTranslatingObj.localPosition = new Vector3((float)previousData.XCoord / 1000, containerTranslatingObj.localPosition.y, containerTranslatingObj.localPosition.z);
            }
            if (previousData.YVelocity == 0)
            {
                containerTranslatingObj.localPosition = new Vector3(containerTranslatingObj.localPosition.x, (float)previousData.YCoord / 1000, containerTranslatingObj.localPosition.z);
            }

        }

        public void RotateObj(float time)
        {
            if (containerRotatingObj.rotation!= target)
            {
                containerRotatingObj.rotation = Quaternion.RotateTowards(containerRotatingObj.rotation, target, 360 * /*rotateValue*/ Mathf.Abs((float)previousData.RotatingVelocity) * (time - previousRealTime));
            }

            if (previousData.RotatingVelocity == 0)
            {

                Debug.Log($"Current rot: {containerRotatingObj.rotation.eulerAngles}");
                Debug.Log($"Target rot: {target.eulerAngles}");
                Debug.Log(this.name);

                containerRotatingObj.rotation = target;
            }


        }

        public void DataUpdatedHandler()
        {
            previousRealTime = Time.realtimeSinceStartup;
            target = originRotation * Quaternion.AngleAxis(360 * (float)previousData.AxePosition, Vector3.forward);
        }
    }
}
