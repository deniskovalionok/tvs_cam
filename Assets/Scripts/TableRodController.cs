﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using Assets.Scripts.Interfaces;
using Assets.Scripts.DataStructures;

namespace Assets.Scripts
{
    public class TableRodController: MonoBehaviour, IController
    {

        private TableRodData previousData;

        public Transform rodEndPoint;
        public Transform coupledObjectRotOrigin;
        public Transform rodObj;
        public Transform tableEndPoint;
        public Transform table;
        private Vector3 distance;
        private Vector3 originPos;
        private float pusherDistance = 0;
        private float previousRealTime;
        private Quaternion originRotation;
        private Quaternion targetRotation;
        private float maxDistance = 184;
        private float multiple;

        private TableRodData _prData;

        public Action<double> RotatedByRod;

        private void Start()
        {
            originPos = rodObj.localPosition;
            originRotation = table.localRotation;
            targetRotation = originRotation;
            distance = CalculateDistance();
            previousRealTime = Time.realtimeSinceStartup;
            multiple = 60 / maxDistance;

            //targetRotation = originRotation * Quaternion.AngleAxis(20 * multiple, Vector3.up);
            //table.localRotation = targetRotation;
        }
       

        private void Update()
        {
            //table.localRotation = Quaternion.RotateTowards(table.localRotation, targetRotation, 100*Time.deltaTime);
            //if(Input.GetKeyDown(KeyCode.A))
            //{
            //    targetRotation = originRotation * Quaternion.AngleAxis(-60, Vector3.up);
            //    table.localRotation = targetRotation;
            //    originRotation = table.localRotation;
            //}
        }

        public int rotatingOnes;

        private IEnumerator RotateInDirection(int rotatingOnes, int direction, int koeff = 1)
        {
            _prData.TableRodZCoord = 0;            
            _prData.TableRodXCoord = direction;
            SetData(_prData);
            yield return new WaitForSeconds(0.1f);
            
            for (int i = 0; i < rotatingOnes; i++)
            {
                _prData.TableRodZVelocity = 20 * Mathf.Abs(koeff);
                SetData(_prData);
                while (_prData.TableRodZCoord < 184)
                {
                    _prData.TableRodZCoord += _prData.TableRodZVelocity * 0.1f;
                    SetData(_prData);
                    yield return new WaitForSeconds(0.1f);
                }
                _prData.TableRodZVelocity = 0;
                SetData(_prData);
                yield return new WaitForSeconds(1);
                _prData.TableRodZVelocity = -20 * Mathf.Abs(koeff);
                while (_prData.TableRodZCoord > 0)
                {
                    _prData.TableRodZCoord += _prData.TableRodZVelocity * 0.1f;
                    SetData(_prData);
                    yield return new WaitForSeconds(0.1f);
                }
                _prData.TableRodZVelocity = 0;

                //_prData.TableRodXCoord = _prData.TableRodXCoord == 1 ? 0 : 1;
                SetData(_prData);
                yield return new WaitForSeconds(1);

            }


        }

        private bool IsResetRodMoving = true;

        public void SetData(Data dataToSet)
        {

            //return;

            previousData = (TableRodData)dataToSet;
            //float distance = 0;
            //Vector3 axis = Vector3.up;

            //if (previousData.TableRodXCoord <= 0.09f)
            //{
            //    Debug.Log($"0 - {previousData.TableRodXCoord}!");
            //    distance = maxDistance - (float)previousData.TableRodZCoord;
            //    axis = Vector3.up;
            //    Debug.Log($"Dist: {distance}");
            //    if (previousData.TableRodZVelocity < 0 && IsResetRodMoving)
            //    {
            //        targetRotation = originRotation * Quaternion.AngleAxis(distance * multiple, axis);
            //    }
            //}
            //else
            //{

            //    Debug.Log($"1 - {previousData.TableRodXCoord}!");
            //    distance = (float)previousData.TableRodZCoord;
            //    axis = Vector3.down;

            //    if (previousData.TableRodZVelocity > 0 && IsResetRodMoving)
            //    {
            //        targetRotation = originRotation * Quaternion.AngleAxis(distance * multiple, axis);
            //    }
            //}
            //Конец полезной инфы

            //if (previousData.TableRodZVelocity>0 && IsResetRodMoving)
            //{
            //    float distance = 0;
            //    Vector3 axis = Vector3.up;
            //    //должны быть реальные координаты, число выбрано из реальной модели
            //    if (previousData.TableRodXCoord <= 0.09f)
            //    {
            //        Debug.Log($"0 - {previousData.TableRodXCoord}!");
            //        distance = maxDistance - (float)previousData.TableRodZCoord;
            //        axis = Vector3.up;
            //    }
            //    else
            //    {
            //        Debug.Log($"1 - {previousData.TableRodXCoord}!");
            //        distance = (float)previousData.TableRodZCoord;
            //        axis = Vector3.down;
            //    }
            //    ////должны быть реальные координаты
            //    //if (previousData.TableRodXCoord <= 1 & previousData.TableRodXCoord > 0.1)
            //    //{
                    
            //    //}
                //Debug.Log($"What {previousData.TableRodXCoord}?");
                //Debug.Log("Rotation calculated!");
                //targetRotation = originRotation * Quaternion.AngleAxis(distance * multiple, axis);
                //Debug.Log($"Dist: {distance}");
            //}            
        }

        public void UpdateControllingParameters()
        {
            //return;
            float r = Time.realtimeSinceStartup;
            RodMoving(r);
            TableRotating(r);
            previousRealTime = r;
        }

        private float pusherPreviousDistance;        

        private void RodMoving(float currentTime)
        {            

            Vector3 currentDistance = CalculateDistance();
            float angle = Vector3.SignedAngle(distance, currentDistance, Vector3.down);            
            coupledObjectRotOrigin.localRotation *= Quaternion.AngleAxis(angle, Vector3.down);
            distance = CalculateDistance();
            if(GameCore.Instance.IsLerp)
            {
                rodObj.localPosition = new Vector3(GameCore.Instance.Center.InverseTransformPoint(coupledObjectRotOrigin.TransformPoint(tableEndPoint.localPosition)).x,
                    rodObj.localPosition.y, Mathf.Lerp(rodObj.localPosition.z, originPos.z + (float)previousData.TableRodZCoord/1000, Time.deltaTime));
            }
            else
            {
                rodObj.localPosition = new Vector3(GameCore.Instance.Center.InverseTransformPoint(coupledObjectRotOrigin.TransformPoint(tableEndPoint.localPosition)).x,
                    rodObj.localPosition.y, Mathf.MoveTowards(rodObj.localPosition.z, originPos.z + (float)previousData.TableRodZCoord / 1000, (float)previousData.TableRodZVelocity/1000*(currentTime - previousRealTime)));
            }
            
            if(previousData.TableRodZVelocity==0)
            {
                rodObj.localPosition = new Vector3(GameCore.Instance.Center.InverseTransformPoint(coupledObjectRotOrigin.TransformPoint(tableEndPoint.localPosition)).x,
                    rodObj.localPosition.y, originPos.z + (float)previousData.TableRodZCoord / 1000);
            }


            if (previousData.TableRodXCoord <= 0.09f)
            {
                if (previousData.TableRodZCoord>=maxDistance)
                {
                    IsResetRodMoving = true;
                }
            }
            else
            {
                if(previousData.TableRodZCoord == 0)
                {
                    IsResetRodMoving = true;
                }
            }
            

        }

        private bool IsRotatedByRod = false;

        private void TableRotating(float currentTime)
        {
            
            if(table.localRotation!=targetRotation)
            {             
                table.localRotation = Quaternion.RotateTowards(table.localRotation, targetRotation, Math.Abs((float)previousData.TableRodZVelocity) * multiple * (currentTime - previousRealTime));
            }
            else
            {             
            }

            if((float)previousData.TableRodZVelocity == 0)
            {             
                table.localRotation = targetRotation;

                if(previousData.TableRodXCoord <= 0.09f)
                {
                    if (previousData.TableRodZCoord == 0)
                    {
                        originRotation = table.localRotation;
                        IsResetRodMoving = false;
                        if (!IsRotatedByRod)
                        {
                            RotatedByRod?.Invoke(previousData.TableRodXCoord);
                            IsRotatedByRod = true;
                        }
                    }
                    else
                    {
                        IsRotatedByRod = false;
                    }
                }
                else
                {
                    if (previousData.TableRodZCoord >= maxDistance)
                    {
                        originRotation = table.localRotation;
                        IsResetRodMoving = false;
                        if (!IsRotatedByRod)
                        {
                            RotatedByRod?.Invoke(previousData.TableRodXCoord);
                            IsRotatedByRod = true;
                        }
                    }
                    else
                    {
                        IsRotatedByRod = false;
                    }
                }                
            }
        }

        private Vector3 CalculateDistance()
        {
            return coupledObjectRotOrigin.position - rodEndPoint.position;
        }

        public void DataUpdatedHandler()
        {            
            float distance = 0;
            Vector3 axis = Vector3.up;

            if (previousData.TableRodXCoord <= 0.09f)
            {                
                distance = maxDistance - (float)previousData.TableRodZCoord;
                axis = Vector3.up;             
                if (previousData.TableRodZVelocity < 0 && IsResetRodMoving)
                {
                    targetRotation = originRotation * Quaternion.AngleAxis(distance * multiple, axis);
                }
            }
            else
            {                
                distance = (float)previousData.TableRodZCoord;
                axis = Vector3.down;
                if (previousData.TableRodZVelocity > 0 && IsResetRodMoving)
                {
                    targetRotation = originRotation * Quaternion.AngleAxis(distance * multiple, axis);
                }
            }
        }
    }
}
