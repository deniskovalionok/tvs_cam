﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using Assets.Scripts.Interfaces;

namespace Assets.Scripts
{
    public class ObjectsRefsHandler: MonoBehaviour
    {
        public HugeContainer hugeContainerObj01;
        public HugeContainer hugeContainerObj02;
        public HugeContainer hugeContainerObj03;
        public Manipulator manipulator;
        public Titler titler;
        public Door bigDoor;
        public SmallContainer smallContainerObj01;
        public SmallContainer smallContainerObj02;
        public SmallContainer smallContainerObj03;
        public PlutoGateway plutoGatewayObj;
        public FunnelDrive funnelDriveObj;
        public GlassCapCapture glassCapCaptureObj;
        public SmallTitler smallTitlerObj;
        public ViewSelector viewSelector;
        public LightController lightContoller;

        private List<IController> controllers = new List<IController>();

        

        private void Start()
        {
            controllers.Add(glassCapCaptureObj);
            controllers.Add(hugeContainerObj01);
            controllers.Add(hugeContainerObj02);
            controllers.Add(hugeContainerObj03);
            controllers.Add(manipulator);
            controllers.Add(titler);
            controllers.Add(bigDoor);
            controllers.Add(smallContainerObj01);
            controllers.Add(smallContainerObj02);
            controllers.Add(smallContainerObj03);
            controllers.Add(plutoGatewayObj);
            controllers.Add(funnelDriveObj);            
            controllers.Add(smallTitlerObj);
            controllers.Add(viewSelector);
            controllers.Add(lightContoller);
        }

        private void Update()
        {                        
            //hugeContainerObj01.transform.position = new Vector3(Mathf.Lerp(hugeContainerObj01.transform.position.x, pos.x, 20*Time.deltaTime/1000), hugeContainerObj01.transform.position.y, hugeContainerObj01.transform.position.z);
        }

        public void UpdateControllingObjectsPositions()
        {
            for (int i = 0; i < controllers.Count; i++)
            {                
                controllers[i].UpdateControllingParameters();             
            }
        }
        
    }
}
