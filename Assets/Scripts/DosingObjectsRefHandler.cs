﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using Assets.Scripts.Interfaces;
using Assets.Scripts.DataStructures;

namespace Assets.Scripts
{
    public class DosingObjectsRefHandler: RefsHandler
    {

        public HugeContainer bigContainer01;
        public HugeContainer bigContainer02;
        public HugeContainer bigContainer03;
        public SmallContainer smallContainer01;
        public SmallContainer smallContainer02;
        public SmallContainer smallContainer03;
        public Manipulator manipulator;
        public FunnelDrive funnelDrive;
        public PlutoGateway plutoGateway;
        public GlassCapCapture glassCapCapture;
        public LightController lightController;
        public ViewSelector viewController;
        public Titler titlerObj;
        public SmallTitler smallTitlerObj;
        public Door doorObj;

        private List<IController> controllersConnectedCheckList;
        

        //public void UpdateControllingObjectsPositions()
        //{
        //    for (int i = 0; i < allControllers.Count; i++)
        //    {
        //        allControllers[i].UpdateControllingParameters();
        //    }             
        //}


        //В интерфейс
        //public IController GetControllerByEntityID(int entityID)
        //{

        //    Type t = DosingMapper.numberTypes[entityID];

        //    for (int i = 0; i < controllersConnectedCheckList.Count; i++)
        //    {
        //        IController contr = controllersConnectedCheckList[i];

        //        if (contr.GetType() == t)
        //        {
        //            controllersConnectedCheckList.Remove(contr);
        //            return contr;
        //        }
        //    }

        //    return null;
        //}

        public override IEnumerable<IController> GetControllers()
        {
            List<IController> allControllers = new List<IController>();

            controllersConnectedCheckList = new List<IController>();            
            allControllers = new List<IController>();
            controllersConnectedCheckList.Add(bigContainer01);
            allControllers.Add(bigContainer01);
            controllersConnectedCheckList.Add(bigContainer02);
            allControllers.Add(bigContainer02);
            controllersConnectedCheckList.Add(bigContainer03);
            allControllers.Add(bigContainer03);
            controllersConnectedCheckList.Add(smallContainer01);
            allControllers.Add(smallContainer01);
            controllersConnectedCheckList.Add(smallContainer02);
            allControllers.Add(smallContainer02);
            controllersConnectedCheckList.Add(smallContainer03);
            allControllers.Add(smallContainer03);
            controllersConnectedCheckList.Add(manipulator);
            allControllers.Add(manipulator);
            controllersConnectedCheckList.Add(funnelDrive);
            allControllers.Add(funnelDrive);
            controllersConnectedCheckList.Add(glassCapCapture);
            allControllers.Add(glassCapCapture);
            controllersConnectedCheckList.Add(titlerObj);
            allControllers.Add(titlerObj);
            controllersConnectedCheckList.Add(smallTitlerObj);
            allControllers.Add(smallTitlerObj);
            controllersConnectedCheckList.Add(lightController);
            allControllers.Add(lightController);
            controllersConnectedCheckList.Add(viewController);
            allControllers.Add(viewController);
            controllersConnectedCheckList.Add(plutoGateway);
            allControllers.Add(plutoGateway);
            controllersConnectedCheckList.Add(doorObj);
            allControllers.Add(doorObj);

            return allControllers;
        }

        public override IController GetControllerByEntityID(int entityID)
        {
            Type t = DosingMapper.numberTypes[entityID];

            for (int i = 0; i < controllersConnectedCheckList.Count; i++)
            {
                IController contr = controllersConnectedCheckList[i];

                if (contr.GetType() == t)
                {
                    controllersConnectedCheckList.Remove(contr);
                    return contr;
                }
            }

            return null;
        }
    }
}
