﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Assets.Scripts.Interfaces;
using Assets.Scripts.DataStructures;
using UnityEngine;

namespace Assets.Scripts
{
    public class SmallTitler : MonoBehaviour, IController
    {

        public Transform smallTitlerObj;

        private TitlerData previousData;
        private float previousRealTime;
        private float rotateValue = 0.1f;
        private Quaternion target;
        private Quaternion originRotation;

        private bool IsSelfRotating = true;

        public void SetRotatingTo(bool isRotating)
        {
            IsSelfRotating = isRotating;
        }

        private void Start()
        {
            originRotation = smallTitlerObj.localRotation;            
        }

        public void SetData(Data dataToSet)
        {
            TitlerData data = dataToSet as TitlerData;
            previousData = data;
            //target = originRotation * Quaternion.AngleAxis(180 * (float)previousData.AxePosition, Vector3.left);
        }

        public void UpdateControllingParameters()
        {
            float r = Time.realtimeSinceStartup;

            if (smallTitlerObj.localRotation != target && IsSelfRotating)
            {
                smallTitlerObj.localRotation = Quaternion.RotateTowards(smallTitlerObj.localRotation, target, 180 * /*rotateValue*/ Mathf.Abs((float)previousData.RotatingVelocity) * (r - previousRealTime));
            }

            previousRealTime = r;

            if (previousData.RotatingVelocity == 0 && IsSelfRotating)
            {
                smallTitlerObj.localRotation = target;
            }
        }

        public void DataUpdatedHandler()
        {
            target = originRotation * Quaternion.AngleAxis(180 * (float)previousData.AxePosition, Vector3.left);
        }
    }
}
