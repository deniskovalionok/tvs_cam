﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.Animations;
using Assets.Scripts.Interfaces;
using Assets.Scripts.DataStructures;
using System.IO;
using System.Collections;
using UnityEngine.Networking.NetworkSystem;
using System.Diagnostics.Tracing;

namespace Assets.Scripts
{
     
    public class AnimationProcessHolder: MonoBehaviour, IController
    {
        public Animator animatorPlayer;        

        private AnimationData settedData;
        private float previousRealTime;
        private AnimatorClipInfo currentClipInfo;
        //private AnimationEventsHandler handler;

        private void Awake()
        {
            if (animatorPlayer == null)
            {
                animatorPlayer = this.gameObject.GetComponent<Animator>();
            }

            if(animatorPlayer == null)
            {
                Debug.Log("There is no Animator controller component on object!");
            }
         
            
        }

        public bool IsAnimationReverse { get { return settedData.AnimationSpeed < 0; } }

        private AnimationData _testData;

        private void Start()
        {
            AnimationInit();
            //handler = this.gameObject.GetComponent<AnimationEventsHandler>();
            //_testData = new AnimationData("", "", -1);
            //SetData(_testData);
            //_testData.AnimationSpeed = 0;
            //UpdateControllingParameters();
            //StartCoroutine(UpdateData());
        }

        public float AnimationDesiredStatePos;

        private IEnumerator UpdateData()
        {
            while (true)
            {

                if (Mathf.Abs((float)_testData.AnimationPosition - AnimationDesiredStatePos) <= Mathf.Abs((float)_testData.AnimationSpeed)/10)
                {
                    _testData.AnimationSpeed = 0;
                    _testData.AnimationPosition = AnimationDesiredStatePos;
                }
                else
                {
                    _testData.AnimationSpeed = Mathf.Sign(AnimationDesiredStatePos - (float)_testData.AnimationPosition) * 20;
                    Debug.Log($"Speed {_testData.AnimationSpeed} setted!");
                }

                _testData.AnimationPosition += _testData.AnimationSpeed / 10;
                DataUpdatedHandler();

                yield return new WaitForSeconds(0.1f);
            }
        }


        private IEnumerator ChangeAnimationState()
        {
            while(_testData.AnimationPosition.CompareTo(100)<0)
            {
                _testData.AnimationPosition += _testData.AnimationSpeed*0.1f;
                yield return new WaitForSeconds(0.1f);
            }
            _testData.AnimationSpeed = 0;
            
        }

        private void AnimationInit()
        {                
            currentClipInfo = animatorPlayer.GetCurrentAnimatorClipInfo(0)[0];
            animatorPlayer.speed = 0;
            AnimationEvent[] events = currentClipInfo.clip.events;
            for (int i = 0; i < events.Length; i++)
            {
                AnimationEventStates.Add(events[i], false);
                animEvents.Add(events[i]);
            }
        }

        bool jumper = false;

        private void Update()
        {

            //if(Input.GetKeyDown(KeyCode.Space))
            //{
            //    jumper = !jumper;
            //}
            //UpdateControllingParameters();
        }



        public void SetData(Data dataToSet)
        {            
            settedData = (AnimationData)dataToSet;
            previousRealTime = Time.realtimeSinceStartup;
        }

        public void DataUpdatedHandler()
        {
            
        }

        public void UpdateControllingParameters()
        {
            float realTime = Time.realtimeSinceStartup;
            if(!jumper)
            {
                SetAnimationFrame(realTime);
            }            
            previousRealTime = realTime;

        }

        private void SetAnimationFrame(float currentRealTime)
        { 
            if(settedData == null)
            {
                return;
            }
            
            float normalizedTime =  Mathf.MoveTowards(animatorPlayer.GetCurrentAnimatorStateInfo(0).normalizedTime, (float)settedData.AnimationPosition / 100, Math.Abs(((float)settedData.AnimationSpeed/100))*(currentRealTime - previousRealTime));
            //Debug.Log($"{animatorPlayer.GetCurrentAnimatorStateInfo(0).normalizedTime} : {(float)settedData.AnimationPosition / 100} : {normalizedTime}");
            //float normalizedTime = Mathf.Lerp(animatorPlayer.GetCurrentAnimatorStateInfo(0).normalizedTime, (float)settedData.AnimationPosition / 100, Time.deltaTime);
            //Debug.Log($"Animator: {currentClipInfo.clip} on gameobject: {this.gameObject.name} {this.enabled}");
            animatorPlayer.Play(currentClipInfo.clip.name, 0, normalizedTime);

            //if((float)settedData.AnimationSpeed == 0)
            //{
            //    animatorPlayer.Play(currentClipInfo.clip.name, 0, (float)settedData.AnimationPosition / 100);
            //}

            FireMissedEvents(normalizedTime);
            
        }

        private Dictionary<AnimationEvent, bool> AnimationEventStates = new Dictionary<AnimationEvent, bool>();
        private List<AnimationEvent> animEvents = new List<AnimationEvent>();

        private void FireMissedEvents(float normalizedTime)
        {
            

            for (int i = 0; i< animEvents.Count; i++)
            {

                if(settedData.AnimationSpeed>0)
                {
                    if (animEvents[i].time / currentClipInfo.clip.length < normalizedTime)
                    {
                        if (!animEvents[i].isFiredByAnimator && !AnimationEventStates[animEvents[i]])
                        {
                            //GameCore.Instance.animEventHandler.GetDelegateByName(animEvents[i].functionName).Invoke(this);
                            GameCore.Instance.animEventHandler.InvokeByName(animEvents[i].stringParameter, this);
                            AnimationEventStates[animEvents[i]] = true;
                            Debug.Log("Debug!!!");
                        }
                    }
                    else
                    {
                        AnimationEventStates[animEvents[i]] = false;
                    }
                }
                else if(settedData.AnimationSpeed < 0)
                {
                    if (animEvents[i].time / currentClipInfo.clip.length > normalizedTime)
                    {
                        if (/*!animEvents[i].isFiredByAnimator || */AnimationEventStates[animEvents[i]])
                        {                            
                            //GameCore.Instance.animEventHandler.GetDelegateByName(animEvents[i].functionName).Invoke(this);
                            GameCore.Instance.animEventHandler.InvokeByName(animEvents[i].stringParameter, this);
                            AnimationEventStates[animEvents[i]] = false;
                        }
                    }
                    else
                    {
                        AnimationEventStates[animEvents[i]] = true;
                    }
                }

               
            }            
        }


        public void TriggerAnimationEvent(string handlerName)
        {
            GameCore.Instance.animEventHandler.InvokeByName(handlerName, this);
        }


       
    }
}
