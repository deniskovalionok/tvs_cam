﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Assets.Scripts.Interfaces;
using UnityEngine;
using Assets.Scripts.DataStructures;

namespace Assets.Scripts
{
    public class GlassCapCapture : MonoBehaviour, IController
    {

        private CapHandlerData previousData;
        private Vector3 originPosition;
        private Quaternion originRotation;
        private float previousRealTime;

        private Quaternion target;
        private float rotateValue=0.1f;

        public Transform glassCapCaptureObj;        
        private Transform capObj;
        private Transform smallContainer;

        private void Start()
        {
            originPosition = glassCapCaptureObj.localPosition;
            originRotation = glassCapCaptureObj.localRotation;
        }

        public void SetData(Data dataToSet)
        {

            CapHandlerData data = dataToSet as CapHandlerData;
            previousData = data;
            //target = originRotation * Quaternion.AngleAxis(180 * (float)previousData.AxePosition, Vector3.forward);
            //previousRealTime = Time.realtimeSinceStartup;

            //IsDataSetted = true;
        }


        private void OnTriggerEnter(Collider other)
        {
            if(other.tag == "Cap")
            {
                capObj = other.transform;
            }

            if(other.tag == "SCont")
            {
                smallContainer = other.transform;
            }
        }

        private void OnTriggerExit(Collider other)
        {

            if (other.tag == "Cap")
            {
                capObj = null;
            }

            if (other.tag == "SCont")
            {
                smallContainer = null;
            }
        }

        public bool IsControllingByGlassCapCapture()
        {
            if (previousData == null)
                return false;
            return previousData.State == 1;
        }

        public Transform GetCurrentCap()
        {
            return capObj;
        }

        private bool IsDataSetted = false;

        public void UpdateControllingParameters()
        {

            if (!IsDataSetted)
                return;


            float r = Time.realtimeSinceStartup;


            if(GameCore.Instance.IsLerp)
            {
                glassCapCaptureObj.localPosition = new Vector3(glassCapCaptureObj.localPosition.x, Mathf.Lerp(glassCapCaptureObj.localPosition.y, (float)previousData.ZCoord / 1000, Time.deltaTime), glassCapCaptureObj.localPosition.z);
            }
            else
            {
                glassCapCaptureObj.localPosition = new Vector3(glassCapCaptureObj.localPosition.x,
               Mathf.MoveTowards(glassCapCaptureObj.localPosition.y, (float)previousData.ZCoord / 1000, (Mathf.Abs((float)previousData.ZVelocity) / 1000) * (r - previousRealTime)),
               glassCapCaptureObj.localPosition.z);
            }            
                       

            if (glassCapCaptureObj.localRotation != target)
            {
                glassCapCaptureObj.localRotation = Quaternion.RotateTowards(glassCapCaptureObj.localRotation, target, 180 * /*rotateValue*/ Mathf.Abs((float)previousData.RotateVel) * (r - previousRealTime));

                //glassCapCaptureObj.localRotation = Quaternion.Lerp(glassCapCaptureObj.localRotation, target, Time.deltaTime/0.5f); 
            }

            previousRealTime = r;

            if (previousData.ZVelocity == 0)
            {
                glassCapCaptureObj.localPosition = new Vector3(glassCapCaptureObj.localPosition.x, (float)previousData.ZCoord / 1000, glassCapCaptureObj.localPosition.z);
            }

            if (previousData.RotateVel == 0)
            {
                glassCapCaptureObj.localRotation = target;
            }

            if (previousData.State == 1)
            {
                if (capObj != null)
                {
                    capObj.parent=GameCore.Instance.Center;
                }
            }

            if(previousData.State == 0)
            {
                if (capObj != null && smallContainer!=null)
                {
                    capObj.parent = smallContainer.GetChild(0).GetChild(0);
                }
            }
        }

        public void DataUpdatedHandler()
        {
            target = originRotation * Quaternion.AngleAxis(180 * (float)previousData.AxePosition, Vector3.forward);
            previousRealTime = Time.realtimeSinceStartup;

            IsDataSetted = true;
        }
    }
}
