﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Scripts
{
    public class GrabSideBySideController: MonoBehaviour
    {

        public Manipulator ManipController;
        public SmallTitler SmallTitler;
        private Transform GetCurrentContainer { get; set; }        

        private bool IsBigGlassCaptured;

        private void Update()
        {
            if (GetCurrentContainer != null && ManipController.GetGrabVal() == 0)
            {
                //GetCurrentContainer.parent = this.transform;
                //SmallTitler.parent = GetCurrentContainer;
                SmallTitler.SetRotatingTo(false);
                SmallTitler.transform.parent = this.transform;
                IsBigGlassCaptured = true;
            }

            if (IsBigGlassCaptured && ManipController.GetGrabVal() == 1)
            {
                //SmallTitler.parent = GameCore.Instance.Center;
                //this.transform.GetChild(1).parent = SmallTitler;
                SmallTitler.transform.parent = GameCore.Instance.Center;
                SmallTitler.SetRotatingTo(true);
                IsBigGlassCaptured = false;
            }
        }


        private void OnTriggerEnter(Collider other)
        {
            if (other.transform.tag == "BigGlass")
            {
                GetCurrentContainer = other.transform;
                Debug.Log("Yeee2");
            }
        }

        private void OnTriggerExit(Collider other)
        {
            if (other.transform.tag == "BigGlass")
            {
                GetCurrentContainer = null;
                Debug.Log("Yeee2Null");
            }
        }
    }
}
