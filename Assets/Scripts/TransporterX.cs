﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using Assets.Scripts.Interfaces;
using Assets.Scripts.DataStructures;

namespace Assets.Scripts
{
    public class TransporterX : MonoBehaviour, IController
    {

        public Transform VerticalDrive;
        public Transform HorizontalDrive;
        public Transform GrabDrive;
        public Transform RightHand;
        public Transform LeftHand;

        public Transform transporterCoordOrigin;
        public Transform transporterCenter;

        private TransporterData previousData;
        private float previousRealTime;
        private float originYPosition;
        private Vector3 fromVerticalToGrabOffset;
        private Vector3 fromVerticalToHorizOffset;
        private Vector3 fromGrabToHorizOffset;
        private Vector3 _fromGrabToHorizOffset;


        public Direction movementDirection;
        private int directionKoeff = 0;

        //Rotation
        private Quaternion originLeftRotation;
        private Quaternion originRightRotation;
        private Quaternion leftTarget;
        private Quaternion rightTarget;
        private float rotateValue = 0.1f;

        //for test
        private TransporterData _test;

        private void Start()
        {
            originYPosition = VerticalDrive.localPosition.y;
            fromVerticalToGrabOffset = GrabDrive.localPosition - VerticalDrive.localPosition;
            fromVerticalToHorizOffset = HorizontalDrive.localPosition - VerticalDrive.localPosition;
            fromGrabToHorizOffset = HorizontalDrive.localPosition - GrabDrive.localPosition;
            _fromGrabToHorizOffset = fromGrabToHorizOffset;


            originLeftRotation = LeftHand.localRotation;
            originRightRotation = RightHand.localRotation;

            if (movementDirection == Direction.Forward)
            {
                directionKoeff = 1;
            }
            else
            {
                directionKoeff = -1;
            }

           // _test = new TransporterData("","","","","","",-1);

            //process = StartCoroutine(UpdateData());

            //length = fromGrabToHorizOffset.x;
        }

        private Coroutine process;


        private IEnumerator UpdateData()
        {
            _test.XCoord = 1248;

            _test.ZCoord = 0;
            _test.ZVelocity = 0;
                                   
            _test.XVelocity = -20;
            _test.AxePosition = 1;
            SetData(_test);
            //_test.RotateVel = 0.1f;
            while (true)
            {

                _test.XCoord += _test.XVelocity / 10;

                if(_test.XCoord<=0)
                {
                    _test.XVelocity = 0;
                    _test.AxePosition = 0;
                    SetData(_test);
                    break;
                }

                //_test.ZCoord += _test.ZVelocity;
                //if(_test.AxePosition<=1)
                //{
                //    _test.AxePosition += _test.RotateVel;
                //}


                SetData(_test);
                yield return new WaitForSecondsRealtime(0.1f);


            }
        }

        private float countTime;

        private void Update()
        {
            //if(this.gameObject.name == "TransporterVertical5")
            //{
            //    Debug.Log(previousData);
            //}

            //UpdateControllingParameters();
            if(previousData!=null)
            {
                //if (previousData.XVelocity != 0)
                //{

                //}
                //else
                //{
                //    Debug.Log(Time.realtimeSinceStartup - countTime);
                //    countTime = Time.realtimeSinceStartup;
                //}
            }

            

            //if (Input.GetKeyDown(KeyCode.Space))
            //{

            //    _test.ZVelocity *= (-1);
            //    //_test.AxePosition = 0.99f;
            //    b = true;
            //    _fromGrabToHorizOffset = HorizontalDrive.localPosition - GrabDrive.localPosition;
            //    Debug.Log(_test.RotateVel);
            //}

            //if (Input.GetKeyDown(KeyCode.S))
            //{
            //    _test.ZVelocity = 0;
            //}

            //if(Input.GetKeyDown(KeyCode.P))
            //{            
             
            //        _test.ZVelocity = 20;
            //}

        }

        public void SetData(Data dataToSet)
        {            

            previousData = dataToSet as TransporterData;            

            //Debug.Log($"ZVelocity of {this.gameObject.name}: {previousData.ZVelocity}");

            //leftTarget = originLeftRotation * Quaternion.AngleAxis(55 * (float)previousData.AxePosition, Vector3.back);
            //rightTarget = originRightRotation * Quaternion.AngleAxis(55 * (float)previousData.AxePosition, Vector3.forward);
            //previousRealTime = Time.realtimeSinceStartup;

        }

        int count = 0;

        bool k = false;
        bool b = false;

        private float time;

        private float length;

        public void UpdateControllingParameters()
        {

            //if (this.gameObject.name != "TransporterVertical0")
            //    return;


            
            float r = Time.realtimeSinceStartup;

            if (GameCore.Instance.IsLerp)
            { 
                VerticalDrive.localPosition = new Vector3(VerticalDrive.localPosition.x, Mathf.Lerp(VerticalDrive.localPosition.y, originYPosition + ((float)previousData.ZCoord / 1000), Time.deltaTime), VerticalDrive.localPosition.z);
                GrabDrive.localPosition = new Vector3(Mathf.Lerp(GrabDrive.localPosition.x, transporterCoordOrigin.localPosition.x + directionKoeff*((float)previousData.XCoord / 1000), Time.deltaTime), (VerticalDrive.localPosition + fromVerticalToGrabOffset).y, GrabDrive.localPosition.z);

                //Math.Abs(Vector3.Distance(GrabDrive.localPosition, HorizontalDrive.localPosition)) > Math.Abs(fromGrabToHorizOffset.magnitude) &&
                HorizontalDrive.localPosition = new Vector3(HorizontalDrive.localPosition.x, (VerticalDrive.localPosition + fromVerticalToHorizOffset).y, HorizontalDrive.localPosition.z);


                bool first = Math.Abs(Vector3.Distance(GrabDrive.localPosition, HorizontalDrive.localPosition)) > Math.Abs(fromGrabToHorizOffset.magnitude);                
                bool second = Math.Abs((HorizontalDrive.localPosition - transporterCenter.localPosition).x) > 0.002f;                
                //if (Math.Abs(Vector3.Distance(GrabDrive.localPosition, HorizontalDrive.localPosition)) > Math.Abs(fromGrabToHorizOffset.magnitude) || Math.Abs(Vector3.Distance(HorizontalDrive.localPosition, transporterCenter.localPosition))>0.001f)
                if(first||second)
                {
                //if(!b)
                //{
                //    fromGrabToHorizOffset = HorizontalDrive.localPosition - GrabDrive.localPosition;
                //_fromGrabToHorizOffset = fromGrabToHorizOffset;
                //Debug.Log("Changed");
                //    b = true;
                //}
                    if(!b)
                    {                       
                        fromGrabToHorizOffset = HorizontalDrive.localPosition - GrabDrive.localPosition;
                        b = true;
                    }
                
                    HorizontalDrive.localPosition = new Vector3((GrabDrive.localPosition+fromGrabToHorizOffset).x, (VerticalDrive.localPosition + fromVerticalToHorizOffset).y, HorizontalDrive.localPosition.z);
                }
                else
                {
                    if (b)
                    {
                        //time = Time.realtimeSinceStartup;                        
                        b = false;
                    }




                //Debug.Log($"kfkf:{k}");
                //count++;

                //HorizontalDrive.localPosition = new Vector3((GrabDrive.localPosition + _fromGrabToHorizOffset).x+((float)previousData.XVelocity*0.7f/1000*(Time.realtimeSinceStartup-time)*(-1)),
                //    (VerticalDrive.localPosition + fromVerticalToHorizOffset).y, HorizontalDrive.localPosition.z);
                }
            }
            else
            {

                VerticalDrive.localPosition = new Vector3(VerticalDrive.localPosition.x, Mathf.MoveTowards(VerticalDrive.localPosition.y, originYPosition + ((float)previousData.ZCoord / 1000), (Mathf.Abs((float)previousData.ZVelocity) / 1000) * (r - previousRealTime)), VerticalDrive.localPosition.z);
                GrabDrive.localPosition = new Vector3(Mathf.MoveTowards(GrabDrive.localPosition.x, transporterCoordOrigin.localPosition.x + directionKoeff*((float)previousData.XCoord / 1000), (Mathf.Abs((float)previousData.XVelocity) / 1000) * (r - previousRealTime)),
                    (VerticalDrive.localPosition + fromVerticalToGrabOffset).y, GrabDrive.localPosition.z);

                HorizontalDrive.localPosition = new Vector3(HorizontalDrive.localPosition.x, (VerticalDrive.localPosition + fromVerticalToHorizOffset).y, HorizontalDrive.localPosition.z);

                bool first = Math.Abs(Vector3.Distance(GrabDrive.localPosition, HorizontalDrive.localPosition)) > Math.Abs(fromGrabToHorizOffset.magnitude);
                bool second = Math.Abs(Vector3.Distance(HorizontalDrive.localPosition, transporterCenter.localPosition)) > 0.001f;


                if (first || second)
                {
                    if (!b)
                    {
                        fromGrabToHorizOffset = HorizontalDrive.localPosition - GrabDrive.localPosition;
                        b = true;
                    }

                    HorizontalDrive.localPosition = new Vector3((GrabDrive.localPosition + fromGrabToHorizOffset).x, (VerticalDrive.localPosition + fromVerticalToHorizOffset).y, HorizontalDrive.localPosition.z);
                }
                else
                {
                    if (b)
                    {
                        b = false;
                    }
                }
            }

            if (LeftHand.localRotation != leftTarget || RightHand.localRotation != rightTarget)
            {
                LeftHand.localRotation = Quaternion.RotateTowards(LeftHand.localRotation, leftTarget, 55 * Mathf.Abs((float)previousData.RotateVel) /*rotateValue*/ * (r - previousRealTime));
                RightHand.localRotation = Quaternion.RotateTowards(RightHand.localRotation, rightTarget, 55 * Mathf.Abs((float)previousData.RotateVel) /*rotateValue*/ * (r - previousRealTime));                
            }

            previousRealTime = r;


            if (previousData.RotateVel == 0)
            {
                LeftHand.localRotation = leftTarget;
                RightHand.localRotation = rightTarget;
            }

            if (previousData.ZVelocity == 0)
            {
                VerticalDrive.localPosition = new Vector3(VerticalDrive.localPosition.x, originYPosition + ((float)previousData.ZCoord / 1000), VerticalDrive.localPosition.z);                
            }
            if (previousData.XVelocity == 0)
            {
                GrabDrive.localPosition = new Vector3(transporterCoordOrigin.localPosition.x + directionKoeff*((float)previousData.XCoord / 1000), GrabDrive.localPosition.y, GrabDrive.localPosition.z);
                if (Math.Abs(Vector3.Distance(GrabDrive.localPosition, HorizontalDrive.localPosition)) > Math.Abs(fromGrabToHorizOffset.magnitude))
                {
                    //Debug.Log("lflfl");
                    Vector3 vec = (HorizontalDrive.localPosition - GrabDrive.localPosition).normalized * fromGrabToHorizOffset.magnitude;
                    HorizontalDrive.localPosition = new Vector3((GrabDrive.localPosition + vec).x, (VerticalDrive.localPosition + fromVerticalToHorizOffset).y, HorizontalDrive.localPosition.z);
                    fromGrabToHorizOffset = vec;
                }                
            }            
        }

        public void DataUpdatedHandler()
        {
            leftTarget = originLeftRotation * Quaternion.AngleAxis(55 * (float)previousData.AxePosition, Vector3.back);
            rightTarget = originRightRotation * Quaternion.AngleAxis(55 * (float)previousData.AxePosition, Vector3.forward);
            previousRealTime = Time.realtimeSinceStartup;
        }
    }
}
