﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Scripts.Interfaces
{
    public abstract class Data
    {

        private int _entityId;

        public Data(int entityId)
        {
            _entityId = entityId;
        }

        //Устанавливает по имени параметра значение
        public abstract void SetParameterValue(string paramName, object value);
        //Возвращает перечислитель всех имен параметров
        public abstract IEnumerable<string> GetAllDataParameterNames();
        //Возвращает ID объекта-данных
        public int GetDataId()
        {
            return _entityId;
        }

        public override string ToString()
        {
            return _entityId.ToString();
        }
    }
}
