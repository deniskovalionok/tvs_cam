﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Scripts.Interfaces
{
    public interface IController
    {
        //Регистрирует данные у контроллера
        void SetData(Data dataToSet);
        //Обработчик события обновления данных
        void DataUpdatedHandler();
        //Обновляет управляемые параметры
        void UpdateControllingParameters();
    }
}
