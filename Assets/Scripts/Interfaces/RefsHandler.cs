﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Scripts.Interfaces
{
    public abstract class RefsHandler: UnityEngine.MonoBehaviour
    {        
        private List<IController> controllers;
                
        private void Awake()
        {            
            //Сбор всех контроллеров конкретной реализации класса
            controllers = new List<IController>();
            foreach (var contr in GetControllers())
            {
                //UnityEngine.Debug.Log(contr);
                controllers.Add(contr);
            }            
        }

        //Возвращает перечислитель всех контроллеров
        public abstract IEnumerable<IController> GetControllers();
        //Возврщает контроллер, ассоциированный с переданным ID
        public abstract IController GetControllerByEntityID(int entityID);

        //Вызывает на всех контроллерах обработчик обновления данных
        public void UpdateControllingObjectsPositions()
        {            
            for (int i = 0; i < controllers.Count; i++)
            {
                controllers[i].UpdateControllingParameters();
            }
        }


    }
}
