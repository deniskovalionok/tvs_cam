﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Scripts
{
    public class DosingMapper
    {
        public static Dictionary<string, int> entitiesNumbers = new Dictionary<string, int>
        {
            {"HugeContainer",1},
            {"SmallContainer",2},
            {"FunnelDrive",3},
            {"GlassCapCapturer",4},
            {"PlutoGatewey",5},
            {"HugeTitler",6},
            {"SmallTitler",7},
            {"Manipulator",8},
            {"LightController",9},
            {"ViewController",10},
            {"Door",11}

        };


        public static Dictionary<int, Type> numberTypes = new Dictionary<int, Type>
        {
            {1,typeof(HugeContainer)},
            {2,typeof(SmallContainer)},
            {3,typeof(FunnelDrive)},
            {4,typeof(GlassCapCapture)},
            {5,typeof(PlutoGateway)},
            {6,typeof(Titler)},
            {7,typeof(SmallTitler)},
            {8,typeof(Manipulator)},
            {9,typeof(LightController)},
            {10,typeof(ViewSelector)},
            {11,typeof(Door)}
        };
    }
}
