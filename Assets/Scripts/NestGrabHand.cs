﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using Assets.Scripts.Interfaces;
using Assets.Scripts.DataStructures;
using System.Collections;

namespace Assets.Scripts
{
    public class NestGrabHand : MonoBehaviour, IController
    {
        private NestGrabHandData previousData;
        private float previousRealTime;

        public Transform nestGrabHandObj;

        private NestGrabHandData _prD;
        
        public int YCoordValue_Test;

        private void Start()
        {
            _prD = new NestGrabHandData("", "", "", -1);
            _prD.YCoord = 300;
            YCoordValue_Test = (int)_prD.YCoord;
            _prD.YVelocity = 20;
            //SetData(_prD);
            //UpdateControllingParameters();
            //StartCoroutine(UpdateData());
        }

        private void Update()
        {
            //UpdateControllingParameters();
        }

        private IEnumerator UpdateData()
        {
            while (true)
            {

                if (Mathf.Abs((float)_prD.YCoord - YCoordValue_Test) < 20)
                {
                    _prD.YVelocity = 0;
                    _prD.YCoord = YCoordValue_Test;
                }
                else
                {
                    _prD.YVelocity = Mathf.Sign(YCoordValue_Test - (float)_prD.YCoord) * 20;
                }
                
                _prD.YCoord += _prD.YVelocity / 10;
                SetData(_prD);

                yield return new WaitForSeconds(0.1f);
            }
        }

        public void SetData(Data dataToSet)
        {
            previousData = (NestGrabHandData)dataToSet;
            //Debug.Log($"Object {this.gameObject.name} has YCoord: {previousData.YCoord}!");
            //previousRealTime = Time.realtimeSinceStartup;
        }

        public void UpdateControllingParameters()
        {
            float r = Time.realtimeSinceStartup;
            TranslateObj(r);
            previousRealTime = r;
        }

        private void TranslateObj(float time)
        {

            if(previousData == null)
            {
                return;
            }

            if(GameCore.Instance.IsLerp)
            {
                nestGrabHandObj.localPosition = new Vector3(nestGrabHandObj.localPosition.x, Mathf.Lerp(nestGrabHandObj.localPosition.y, (float)previousData.YCoord / 1000, Time.deltaTime), nestGrabHandObj.localPosition.z);
            }
            else
            {
                nestGrabHandObj.localPosition = new Vector3(nestGrabHandObj.localPosition.x, Mathf.MoveTowards(nestGrabHandObj.localPosition.y, (float)previousData.YCoord / 1000, (time - previousRealTime)), nestGrabHandObj.localPosition.z);
            }

            if(previousData.YVelocity == 0)
            {
                nestGrabHandObj.localPosition = new Vector3(nestGrabHandObj.localPosition.x, (float)previousData.YCoord / 1000, nestGrabHandObj.localPosition.z);
            }
        }

        public void DataUpdatedHandler()
        {
            previousRealTime = Time.realtimeSinceStartup;
        }
    }
}
