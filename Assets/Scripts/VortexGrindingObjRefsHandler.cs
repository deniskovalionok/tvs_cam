﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Assets.Scripts.Interfaces;
using UnityEngine;

namespace Assets.Scripts
{
    public class VortexGrindingObjRefsHandler : RefsHandler
    {

        //private List<IController> controllersConnectedCheckList;      


        public TransporterX tr1;
        public TransporterX tr2;
        public TransporterX tr3;
        public TransporterX tr4;
        public TransporterX tr5;
        public TransporterX tr6;
        public TransporterX tr7;
        public TransporterX tr8;

        public TransporterZ tr9;
        public TransporterZ tr10;

        public TransportTitler titler;

        public GrindingContainer container1;
        public GrindingContainer container2;
        public GrindingContainer container3;
        public GrindingContainer container4;
        public GrindingContainer container5;
        public GrindingContainer container6;
        public GrindingContainer container7;
        public GrindingContainer container8;
        public GrindingContainer container9;

        public ViewSelector selector;
        public LightController lightContr;


        //private void SetControllersByTypes()
        //{
        //    controllersConnectedCheckList = new List<IController>();            
        //    allControllers = new List<IController>();
        //    allControllers.Add(container);
        //    allControllers.Add(tr1);
        //    allControllers.Add(tr2);
        //    allControllers.Add(tr3);
        //    allControllers.Add(tr4);
        //    allControllers.Add(tr9);            
        //    allControllers.Add(tr5);
        //    allControllers.Add(tr6);
        //    allControllers.Add(tr7);
        //    allControllers.Add(tr8);
        //    allControllers.Add(tr10);
        //    allControllers.Add(titler);
        //    allControllers.Add(lightContr);
        //    allControllers.Add(selector);
        //    VortexGrindingMapper.numberTypes.Add(1, container);
        //    VortexGrindingMapper.numberTypes.Add(2, tr1);
        //    VortexGrindingMapper.numberTypes.Add(3, tr2);
        //    VortexGrindingMapper.numberTypes.Add(4, tr3);
        //    VortexGrindingMapper.numberTypes.Add(5, tr4);
        //    VortexGrindingMapper.numberTypes.Add(6, tr9);
        //    VortexGrindingMapper.numberTypes.Add(7, tr5);
        //    VortexGrindingMapper.numberTypes.Add(8, tr6);
        //    VortexGrindingMapper.numberTypes.Add(9, tr7);
        //    VortexGrindingMapper.numberTypes.Add(10, tr8);
        //    VortexGrindingMapper.numberTypes.Add(11,tr10);
        //    VortexGrindingMapper.numberTypes.Add(12, titler);
        //    VortexGrindingMapper.numberTypes.Add(13, lightContr);
        //    VortexGrindingMapper.numberTypes.Add(14, selector);           
        //}

        //public void UpdateControllingObjectsPositions()
        //{
        //    for (int i = 0; i < allControllers.Count; i++)
        //    {
        //        allControllers[i].UpdateControllingParameters();
        //    }
        //}


        public override IEnumerable<IController> GetControllers()
        {
            List<IController> allControllers = new List<IController>();
            allControllers.Add(container1);
            allControllers.Add(tr1);
            allControllers.Add(tr2);
            allControllers.Add(tr3);
            allControllers.Add(tr4);
            allControllers.Add(tr9);
            allControllers.Add(tr5);
            allControllers.Add(tr6);
            allControllers.Add(tr7);
            allControllers.Add(tr8);
            allControllers.Add(tr10);
            allControllers.Add(titler);
            allControllers.Add(lightContr);
            allControllers.Add(selector);
            allControllers.Add(container2);
            allControllers.Add(container3);
            allControllers.Add(container4);
            allControllers.Add(container5);
            allControllers.Add(container6);
            allControllers.Add(container7);
            allControllers.Add(container8);
            allControllers.Add(container9);
            VortexGrindingMapper.numberTypes.Add(1, container1);
            VortexGrindingMapper.numberTypes.Add(2, tr1);
            VortexGrindingMapper.numberTypes.Add(3, tr2);
            VortexGrindingMapper.numberTypes.Add(4, tr3);
            VortexGrindingMapper.numberTypes.Add(5, tr4);
            VortexGrindingMapper.numberTypes.Add(6, tr9);
            VortexGrindingMapper.numberTypes.Add(7, tr5);
            VortexGrindingMapper.numberTypes.Add(8, tr6);
            VortexGrindingMapper.numberTypes.Add(9, tr7);
            VortexGrindingMapper.numberTypes.Add(10, tr8);
            VortexGrindingMapper.numberTypes.Add(11, tr10);
            VortexGrindingMapper.numberTypes.Add(12, titler);
            VortexGrindingMapper.numberTypes.Add(13, lightContr);
            VortexGrindingMapper.numberTypes.Add(14, selector);
            VortexGrindingMapper.numberTypes.Add(15, container2);
            VortexGrindingMapper.numberTypes.Add(16, container3);
            VortexGrindingMapper.numberTypes.Add(17, container4);
            VortexGrindingMapper.numberTypes.Add(18, container5);
            VortexGrindingMapper.numberTypes.Add(19, container6);
            VortexGrindingMapper.numberTypes.Add(20, container7);
            VortexGrindingMapper.numberTypes.Add(21, container8);
            VortexGrindingMapper.numberTypes.Add(22, container9);

            return allControllers;
        }

        public override IController GetControllerByEntityID(int entityID)
        {
            return VortexGrindingMapper.numberTypes[entityID];
        }
    }
}
