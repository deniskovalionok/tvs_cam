﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Scripts
{
    public class HandGrabber : MonoBehaviour
    {
        public BucksContainer container;
        public GrabHandController grabHandController;

        private ContainerPart? nearestPart;
        private long val;

        //private void Start()
        //{
        //    grabHandController.GrabStateChanged += GrabHandController_GrabStateChanged;
        //}

        //private void GrabHandController_GrabStateChanged(long state)
        //{
        //    if (state == 0)
        //    {
        //        container.SetParentToGrabHand(nearestPart.Value);
        //    }
        //    else
        //    {

        //    }
        //}

        private void OnTriggerEnter(Collider other)
        {
            if(container.IsInCenter() || container.IsCompleted())
            {
                return;
            }

            if(other.transform.CompareTag("ContCup"))
            {
               nearestPart = ContainerPart.ContainerCup;
               container.SetParentToGrabHand(nearestPart.Value);
            }

            if(other.transform.CompareTag("Cont"))
            {
               nearestPart = ContainerPart.Container;
               container.SetParentToGrabHand(nearestPart.Value);
            }

            if(other.transform.CompareTag("BucksCup"))
            {
               nearestPart = ContainerPart.BucksCup;
               container.SetParentToGrabHand(nearestPart.Value);
            }

            if(other.transform.CompareTag("Bucks"))
            {
               nearestPart = ContainerPart.Bucks;
               container.SetParentToGrabHand(nearestPart.Value);
            }

            
        }

        private void OnTriggerExit(Collider other)
        {            

            if (other.transform.CompareTag("ContCup") || other.transform.CompareTag("Cont")
                || other.transform.CompareTag("BucksCup") || other.transform.CompareTag("Bucks"))
            {
                nearestPart = null;
                container.SetParentToGrabHand(null);

            }
        }
    }
}
