﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Assets.Scripts.DataStructures;
using Assets.Scripts.Interfaces;

namespace Assets.Scripts
{
    public class PowderStorageBoxDataHandler: IDataProvider
    {        

        public IEnumerable<Data> GetDataObjects()   
        {            

            List<Data> dataStructures = new List<Data>();

            //Truck1
            TruckData truckData = new TruckData("Truck01XCoord", "Truck01YCoord", "Truck01XVelocity", "Truck01YVelocity", PowderStorageMapper.entitiesNumbers["Truck"]);
            //Container1
            ContainerData contData1 = new ContainerData("Cont01XVelocity", "Cont01YVelocity", "", "Cont01XCoord", "Cont01YCoord", "", "Cont01RotVelocity", "Cont01RotAxe", PowderStorageMapper.entitiesNumbers["Container1"]);
            //Container2
            ContainerData contData2 = new ContainerData("Cont02XVelocity", "Cont02YVelocity", "", "Cont02XCoord", "Cont02YCoord", "", "Cont02RotVelocity", "Cont02RotAxe", PowderStorageMapper.entitiesNumbers["Container2"]);
            //Container3
            ContainerData contData3 = new ContainerData("Cont03XVelocity", "Cont03YVelocity", "", "Cont03XCoord", "Cont03YCoord", "", "Cont03RotVelocity", "Cont03RotAxe", PowderStorageMapper.entitiesNumbers["Container3"]);
            //NestHand1
            NestGrabHandData nestHand1 = new NestGrabHandData("NestGrab01YCoord", "NestGrab01YVelocity", "NestGrab01State", PowderStorageMapper.entitiesNumbers["NestGrabHand1"]);
            //NestHand2
            NestGrabHandData nestHand2 = new NestGrabHandData("NestGrab02YCoord", "NestGrab02YVelocity", "NestGrab02State", PowderStorageMapper.entitiesNumbers["NestGrabHand2"]);
            //NestHand
            NestGrabHandData nestHand3 = new NestGrabHandData("NestGrab03YCoord", "NestGrab03YVelocity", "NestGrab03State", PowderStorageMapper.entitiesNumbers["NestGrabHand3"]);
            //NestHand4
            NestGrabHandData nestHand4 = new NestGrabHandData("NestGrab04YCoord", "NestGrab04YVelocity", "NestGrab04State", PowderStorageMapper.entitiesNumbers["NestGrabHand4"]);
            //GranulaClamp
            ClampsData granulaClamps = new ClampsData("GranulAxePosition", "", PowderStorageMapper.entitiesNumbers["GranulaClamps"]);
            //SamplingClamp
            ClampsData samplingClamps = new ClampsData("SamplingAxePosition", "", PowderStorageMapper.entitiesNumbers["SamplingClamps"]);
            //Gate
            DoorData doorData = new DoorData("Gate01YCoord", "Gate01YVelocity", PowderStorageMapper.entitiesNumbers["Gate"]);
            //GrabHand
            GrabHandData grabHandData = new GrabHandData("Rod03XCoord", "Rod04ZCoord", "Rod03XVelocity", "Rod04ZVelocity", "GrabHandState", PowderStorageMapper.entitiesNumbers["GrabHand"]);
            //TableRod
            TableRodData tableRodData = new TableRodData("Rod01ZCoord", "Rod02XCoord", "Rod01ZVelocity", "Rod02XVelocity", PowderStorageMapper.entitiesNumbers["TableRod"]);
            //RotatingTable
            RotatingTableData rotTableData = new RotatingTableData("LiftTableZCoord", "LiftTableZVelocity", "LiftTableRotVelocity", "LiftTableAxisPosition", PowderStorageMapper.entitiesNumbers["RotatingTable"]);
            //ZinkLifter
            ZinkContainerLifterData zinkLifterData = new ZinkContainerLifterData("LifterYCoord", "LifterYVelocity", PowderStorageMapper.entitiesNumbers["ZinkLifter"]);
            //SamplingCarriage
            SamplingCarriageData samplingCarData = new SamplingCarriageData("Rod05XCoord", "Rod05XVelocity", "Rod07YCoord", "Rod07YVelocity", "Rod06YCoord", "Rod06YVelocity", PowderStorageMapper.entitiesNumbers["SamplingCarriage"]);
            //Bucks
            BucksContainerData bucksContData = new BucksContainerData("BucksState", "BucksCupState", "ContainerState", "ContainerCupState", PowderStorageMapper.entitiesNumbers["BucksCont"]);
            //Light
            LightData lightData = new LightData("LightState", PowderStorageMapper.entitiesNumbers["Light"]);
            //Camera
            ViewData viewData = new ViewData("CameraNumber", PowderStorageMapper.entitiesNumbers["ViewSelector"]);

            dataStructures.Add(truckData);
            dataStructures.Add(contData1);
            dataStructures.Add(contData2);
            dataStructures.Add(contData3);
            dataStructures.Add(nestHand1);
            dataStructures.Add(nestHand2);
            dataStructures.Add(nestHand3);
            dataStructures.Add(nestHand4);
            dataStructures.Add(granulaClamps);
            dataStructures.Add(samplingClamps);
            dataStructures.Add(doorData);            

            dataStructures.Add(grabHandData);
            dataStructures.Add(tableRodData);
            dataStructures.Add(rotTableData);
            dataStructures.Add(zinkLifterData);
            dataStructures.Add(samplingCarData);
            dataStructures.Add(bucksContData);
            dataStructures.Add(lightData);
            dataStructures.Add(viewData);

            return dataStructures;

        }
    }
}
