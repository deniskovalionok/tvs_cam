﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Assets.Scripts.DataStructures;
using Assets.Scripts.Interfaces;
using UnityEngine;


namespace Assets.Scripts
{
    public class StorageTruck : MonoBehaviour, IController
    {

        private TruckData settedData;
        private float previousRealtime;

        public Transform Truck;
        public Transform Manipulator;
        public Transform Grab;
        public float TransportKoeff;

        private float truckOriginX;
        private float manipulatorOriginY;
        private float rotatingVelocity = 0.1f;
        private float manipulatorTruckOffsetX;
        private Quaternion originGrabRotation;
        private Quaternion targetRotation;

        public float XCoord;
        public float YCoord;
        public float RotState;

        private void Start()
        {
            previousRealtime = Time.realtimeSinceStartup;
            truckOriginX = Truck.localPosition.x;
            manipulatorOriginY = Manipulator.localPosition.y;
            originGrabRotation = Grab.localRotation;
            targetRotation = originGrabRotation;

            manipulatorTruckOffsetX = Manipulator.localPosition.x - Truck.localPosition.x;
            //_prData = new TruckData("", "", "", "", -1, "", "");         
            //SetData(_prData);
            //DataUpdatedHandler();
            //UpdateControllingParameters();
            //_prData.XCoord = XCoord;
            //_prData.YCoord = YCoord;
            //_prData.AxisState = RotState;
            //StartCoroutine(Rotate());
            //StartCoroutine(CheckCoordChanging());
        }

        private TruckData _prData;
        private bool k = false;

        private IEnumerator Rotate()
        {
            while(true)
            {
                if(_prData.AxisState!=RotState)
                {
                    if(!k)
                    {
                        if (_prData.AxisState < RotState)
                        {
                            _prData.RotVelocity = 0.1f;
                            k = true;
                        }
                        else
                        {
                            _prData.RotVelocity = -0.1f;
                            k = true;
                        }
                    }
                    

                    if (_prData.RotVelocity>0)
                    {                        

                        if(_prData.AxisState<RotState)
                        {
                            
                            _prData.AxisState += _prData.RotVelocity * 0.1f;
                        }
                        else
                        {                            
                            _prData.AxisState = RotState;
                            _prData.RotVelocity = 0;
                            k = false;
                        }
                    }
                    else
                    {
                        if (_prData.AxisState > RotState)
                        {
                            _prData.AxisState += _prData.RotVelocity * 0.1f;
                        }
                        else
                        {
                            _prData.AxisState = RotState;
                            _prData.RotVelocity = 0;
                            k = false;
                        }
                    }

                  


                }


                DataUpdatedHandler();
                yield return new WaitForSeconds(0.1f);
            }
          
        }

        private IEnumerator CheckCoordChanging()
        {
            while(true)
            {
                if (XCoord != _prData.XCoord)
                {
                    if (XCoord < _prData.XCoord)
                    {
                        _prData.XVelocity = -20;
                    }
                    else
                    {
                        _prData.XVelocity = 20;
                    }

                }

                if (YCoord != _prData.YCoord)
                {
                    if (YCoord < _prData.YCoord)
                    {
                        _prData.YVelocity = -20;
                    }
                    else
                    {
                        _prData.YVelocity = 20;
                    }

                }


                if (Mathf.Sign((float)_prData.XVelocity) > 0)
                {
                    if (_prData.XCoord < XCoord)
                    {
                        _prData.XCoord += _prData.XVelocity * 0.1f;
                    }
                    else
                    {
                        _prData.XVelocity = 0;
                    }
                }
                else if ((float)_prData.XVelocity < 0)
                {
                    if (_prData.XCoord > XCoord)
                    {
                        _prData.XCoord += _prData.XVelocity * 0.1f;
                    }
                    else
                    {
                        _prData.XVelocity = 0;
                    }

                }

                if (Mathf.Sign((float)_prData.YVelocity) > 0)
                {
                    if (_prData.YCoord < YCoord)
                    {
                        _prData.YCoord += _prData.YVelocity * 0.1f;
                    }
                    else
                    {
                        _prData.YVelocity = 0;
                    }

                }
                else if ((float)_prData.YVelocity < 0)
                {
                    if (_prData.YCoord > YCoord)
                    {
                        _prData.YCoord += _prData.YVelocity * 0.1f;
                    }
                    else
                    {
                        _prData.YVelocity = 0;
                    }

                }
                DataUpdatedHandler();
                yield return new WaitForSeconds(0.1f);
            }
            
        }


        private IEnumerator Testing()
        {
            _prData.XVelocity = 20;
            _prData.YVelocity = 20;
            _prData.RotVelocity = 0.1f;
            while(_prData.XCoord<5300)
            {
                _prData.XCoord += _prData.XVelocity * 0.1;
                _prData.YCoord += _prData.YVelocity * 0.1f;
                //if(_prData.AxisState<1)
                //{
                //    _prData.AxisState += _prData.RotVelocity * 0.1f;
                //}
                DataUpdatedHandler();
                yield return new WaitForSeconds(0.1f);
            }
        }

        private void Update()
        {
            //UpdateControllingParameters();

            //if(Input.GetKeyDown(KeyCode.A))
            //{
            //    YCoord = 2300;
            //}
            //if (Input.GetKeyDown(KeyCode.S))
            //{
            //    YCoord = 2000;
            //}
        }

        public void DataUpdatedHandler()
        {
            previousRealtime = Time.realtimeSinceStartup;
            targetRotation = originGrabRotation * Quaternion.AngleAxis(180 * (float)settedData.AxisState, Vector3.back);
        }

        public void SetData(Data dataToSet)
        {
            settedData = (TruckData)dataToSet;
        }

        public void UpdateControllingParameters()
        {
            float r = Time.realtimeSinceStartup;
            TransportingTruck(r);
            previousRealtime = r;
        }

        private void TransportingTruck(float currentTime)
        {
            if(GameCore.Instance.IsLerp)
            {
                Truck.localPosition = new Vector3(Mathf.Lerp(Truck.localPosition.x, truckOriginX + ((float)settedData.XCoord / 1000)/ TransportKoeff, Time.deltaTime), Truck.localPosition.y, Truck.localPosition.z);
                Manipulator.localPosition = new Vector3(Truck.localPosition.x + manipulatorTruckOffsetX, Mathf.Lerp(Manipulator.localPosition.y, manipulatorOriginY + (float)settedData.YCoord / 1000, Time.deltaTime), Manipulator.localPosition.z);
            }
            else
            {
                Truck.localPosition = new Vector3(Mathf.MoveTowards(Truck.localPosition.x, truckOriginX + ((float)settedData.XCoord / 1000)/ TransportKoeff, Math.Abs((float)settedData.XVelocity / 1000) * (currentTime - previousRealtime)),
                    Truck.localPosition.y,
                    Truck.localPosition.z);
                Manipulator.localPosition = new Vector3(Truck.localPosition.x + manipulatorTruckOffsetX,
                    Mathf.MoveTowards(Manipulator.localPosition.y, manipulatorOriginY + (float)settedData.YCoord / 1000, Math.Abs((float)settedData.YVelocity / 1000) * (currentTime - previousRealtime)),
                    Truck.localPosition.z);
            }

            if(Grab.localRotation!=targetRotation)
            {
                //Grab.localRotation = Quaternion.RotateTowards(Grab.localRotation, targetRotation, 180 * rotatingVelocity * (currentTime - previousRealtime));
                Grab.localRotation = Quaternion.RotateTowards(Grab.localRotation, targetRotation, 180 * Math.Abs((float)settedData.RotVelocity) * (currentTime - previousRealtime));
            }

            if(settedData.RotVelocity == 0)
            {
                Grab.localRotation = targetRotation;
            }

            if(settedData.XVelocity==0)
            {
                Truck.localPosition = new Vector3(truckOriginX + ((float)settedData.XCoord / 1000)/ TransportKoeff, Truck.localPosition.y, Truck.localPosition.z);
            }
            if(settedData.YVelocity == 0)
            {
                Manipulator.localPosition = new Vector3(Truck.localPosition.x + manipulatorTruckOffsetX, manipulatorOriginY + (float)settedData.YCoord / 1000, Manipulator.localPosition.z);
            }
        }
    }
}
