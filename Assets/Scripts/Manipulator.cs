﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using Assets.Scripts.Interfaces;
using Assets.Scripts.DataStructures;

namespace Assets.Scripts
{
    public class Manipulator : MonoBehaviour, IController
    {
        [SerializeField]
        private Transform GrabDownUp;
        [SerializeField]
        private Transform GrabForwardBack;
        [SerializeField]
        private Transform GrabSideBySide;
        

        public Transform GetCurrentContainer { get; private set; }

        public event Action ReachedContainerSpawning;
        public event Action LeavedContainerSpawning;
        public event Action ReachedTitler;
        public event Action LeavedTitler;

        public bool IsReachedContainerSpawn { get; private set; }
        public bool IsReachedTitler { get; private set; }

        private Vector3 containerSpawningPosition;
        private Vector3 titlerFirstPosition;
        private Vector3 titlerSecondPosition;

        private ManipulatorData previousData;

        private float previousRealTime;

        private Vector3 GrabSideBySideFromForwardDelta;
        private Vector3 GrabSideBySideFromDownUpDelta;
        private Vector3 GrabDownUpFromForwardDelta;

        private float manipulatorHeightOffset;
        private float heightOffset = 0;

        private void Start()
        {
            if(GrabForwardBack == null)
            {
                GrabForwardBack = this.transform;
            }

            GrabSideBySideFromForwardDelta = GrabSideBySide.localPosition - GrabForwardBack.localPosition;
            GrabSideBySideFromDownUpDelta = GrabSideBySide.localPosition - GrabDownUp.localPosition;
            GrabDownUpFromForwardDelta = GrabDownUp.localPosition - GrabForwardBack.localPosition;

            manipulatorHeightOffset = GrabDownUp.localPosition.y - 2.3f;

            previousRealTime = Time.realtimeSinceStartup;
        }

        private float previousGrabValue;

        //Должно проверять происходит ли процесс захвата или отпускания
        public bool IsGrabbingProcess()
        {
            return previousGrabValue - previousData.GrabValue > 0;
        }

        public float GetGrabVal()
        {
            if(previousData!=null)
            {
                return (float)previousData.GrabValue;
            }
            else
            {
                return -1;
            }
        }
        

        private void Update()
        {

            //if(IsSmallContainerWork())
            //{
            //    heightOffset = manipulatorHeightOffset;
            //}

            //heightOffset = IsSmallContainerWork() ? 0 : manipulatorHeightOffset;

          

            if (IsInPositionArea(containerSpawningPosition))
            {
                IsReachedContainerSpawn = true;
                //ReachedContainerSpawning?.Invoke();
            }
            else if(IsReachedContainerSpawn)
            {
                IsReachedContainerSpawn = false;
                //LeavedContainerSpawning?.Invoke();
            }

            if (IsInPositionArea(titlerFirstPosition))
            {
                IsReachedTitler = true;
                //ReachedContainerSpawning?.Invoke();
            }
            else if(IsReachedTitler)
            {
                IsReachedTitler = false;
                //LeavedTitler?.Invoke();
            }
            
        }

        private bool IsSmallContainerWork()
        {
            if(Math.Abs(GrabForwardBack.localPosition.x*1000-7280)<3|| Math.Abs(GrabForwardBack.localPosition.x*1000 - 7760) < 3|| Math.Abs(GrabForwardBack.localPosition.x*1000- 8210) < 3||GetCurrentContainer !=null/*||previousData?.GrabValue==1*/)
            {
                Debug.Log("Is small container work!");
                return true;
            }

            return false;

            
        }

        //Должно проверяться находиться ли объект в области указанной позиции
        private bool IsInPositionArea(Vector3 position)
        {
            return false;
        }
        

        public void SetData(Data dataToSet)
        {
            ManipulatorData manipData = dataToSet as ManipulatorData;
            if(previousData!=null)
            {
                previousGrabValue = (float)previousData.GrabValue;
            }            
            previousData = manipData;
            //Debug.Log($"ManipData:{previousData.XCoord}");
            ////grabDownUpPrevValue = GrabDownUp.localPosition.y / 1000;
            //previousRealTime = Time.realtimeSinceStartup;
        }

        private float grabDownUpPrevValue = 0;

        public Transform test;

        public void UpdateControllingParameters()
        {




            //GameCore.Instance.Center.InverseTransformDirection(GrabForwardBack.TransformDirection(GrabDownUp.localPosition)) 

            //Debug.Log("Update in Manipulator");
            //Vector3 v = GrabDownUp.InverseTransformPoint(GameCore.Instance.Center.TransformPoint(new Vector3(0, 0, (float)previousData.YCoord / 1000)));
            float r = Time.realtimeSinceStartup;
            //Логика движения            
            //GrabSideBySide.localPosition = new Vector3(GrabSideBySide.localPosition.x,
            //    Mathf.MoveTowards(GrabSideBySide.localPosition.y, v.y, (Mathf.Abs((float)previousData.YVelocity) / 1000) * (r - previousRealTime)),
            //    GrabSideBySide.localPosition.z);  
            //Must be decommented

            if (GameCore.Instance.IsLerp)
            {
                GrabSideBySide.localPosition = new Vector3((GrabForwardBack.localPosition + GrabSideBySideFromForwardDelta).x, (GrabDownUp.localPosition + GrabSideBySideFromDownUpDelta).y, Mathf.Lerp(GrabSideBySide.localPosition.z, (float)previousData.YCoord / 1000, Time.deltaTime));
                GrabDownUp.localPosition = new Vector3((GrabForwardBack.localPosition + GrabDownUpFromForwardDelta).x, Mathf.Lerp(GrabDownUp.localPosition.y, ((float)previousData.ZCoord / 1000) + heightOffset, Time.deltaTime), GrabDownUp.localPosition.z);
                GrabForwardBack.localPosition = new Vector3(Mathf.Lerp(GrabForwardBack.localPosition.x, (float)previousData.XCoord / 1000, Time.deltaTime), GrabForwardBack.localPosition.y, GrabForwardBack.localPosition.z);
            }
            else
            {
                GrabSideBySide.localPosition = new Vector3((GrabForwardBack.localPosition + GrabSideBySideFromForwardDelta).x, (GrabDownUp.localPosition + GrabSideBySideFromDownUpDelta).y,
                Mathf.MoveTowards(GrabSideBySide.localPosition.z, (float)previousData.YCoord / 1000, (Mathf.Abs((float)previousData.YVelocity) / 1000) * (r - previousRealTime)));
                               
                GrabDownUp.localPosition = new Vector3((GrabForwardBack.localPosition + GrabDownUpFromForwardDelta).x,
                    Mathf.MoveTowards(GrabDownUp.localPosition.y, ((float)previousData.ZCoord / 1000)+ heightOffset, (Mathf.Abs((float)previousData.ZVelocity) / 1000) * (r - previousRealTime)),
                    GrabDownUp.localPosition.z);
                GrabForwardBack.localPosition = new Vector3(Mathf.MoveTowards(GrabForwardBack.localPosition.x, (float)previousData.XCoord / 1000, (Mathf.Abs((float)previousData.XVelocity) / 1000) * (r - previousRealTime)), GrabForwardBack.localPosition.y, GrabForwardBack.localPosition.z);
            }

            
            //Must be decommented
            //GrabForwardBack.localPosition = new Vector3(Mathf.Lerp(GrabForwardBack.localPosition.x, (float)previousData.XCoord / 1000, Time.deltaTime), GrabForwardBack.localPosition.y, GrabForwardBack.localPosition.z);
            previousRealTime = r;

            if (previousData.YVelocity == 0)
            {
                //GrabSideBySide.localPosition = new Vector3(GrabSideBySide.localPosition.x, GrabDownUp.InverseTransformPoint(GameCore.Instance.Center.TransformPoint(new Vector3(0, 0, (float)previousData.YCoord / 1000))).y, GrabSideBySide.localPosition.z);
                GrabSideBySide.localPosition = new Vector3(GrabSideBySide.localPosition.x, GrabSideBySide.localPosition.y, (float)previousData.YCoord / 1000);
            }
            if (previousData.ZVelocity == 0)
            {
                //GrabDownUp.localPosition = new Vector3(GrabDownUp.localPosition.x, GrabForwardBack.InverseTransformDirection(GameCore.Instance.Center.TransformDirection(new Vector3(GrabDownUp.localPosition.y, (float)previousData.ZCoord / 1000, GrabDownUp.localPosition.z))).y, GrabDownUp.localPosition.z);
                GrabDownUp.localPosition = new Vector3(GrabDownUp.localPosition.x, ((float)previousData.ZCoord / 1000), GrabDownUp.localPosition.z);

            }
            if (previousData.XVelocity == 0)
            {
                GrabForwardBack.localPosition = new Vector3((float)previousData.XCoord / 1000, GrabForwardBack.localPosition.y, GrabForwardBack.localPosition.z);
            }

            //var vec = GrabDownUp.InverseTransformPoint(GameCore.Instance.Center.TransformPoint(new Vector3(0, 0, previousData.YCoord/1000)));                                    
            //GrabSideBySide.localPosition = new Vector3(GrabSideBySide.localPosition.x, Mathf.Lerp(GrabSideBySide.localPosition.y, vec.y, Time.deltaTime), GrabSideBySide.localPosition.z);

            //Vector3 v1 = new Vector3(GrabDownUp.localPosition.x, Mathf.Lerp(GrabForwardBack.TransformDirection(GrabDownUp.localPosition).y, previousData.ZCoord / 1000, Time.deltaTime), GrabDownUp.localPosition.z);
            //GrabDownUp.localPosition = new Vector3(GrabDownUp.localPosition.x, GrabForwardBack.InverseTransformDirection(v1).y, GrabDownUp.localPosition.z);

            //GrabForwardBack.localPosition = new Vector3(Mathf.Lerp(GrabForwardBack.localPosition.x, previousData.XCoord / 1000, Time.deltaTime), GrabForwardBack.localPosition.y, GrabForwardBack.localPosition.z);

            //GrabSideBySide.localPosition = new Vector3(GrabSideBySide.localPosition.x, GrabSideBySide.localPosition.y, Mathf.Lerp(GrabSideBySide.localPosition.z, previousData.YCoord / 1000, Time.deltaTime));

        }

        public void AttachChild(Transform child)
        {
            child.parent = GrabSideBySide;
        }

        public void DettachChild(Transform child)
        {
            child.parent = null;
        }

        public void DataUpdatedHandler()
        {                        
            previousRealTime = Time.realtimeSinceStartup;
        }
    }
}
