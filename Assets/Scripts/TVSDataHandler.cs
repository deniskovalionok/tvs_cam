﻿using Assets.Scripts.DataStructures;
using Assets.Scripts.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Scripts
{
    public class TVSDataHandler : IDataProvider
    {
        public IEnumerable<Data> GetDataObjects()
        {
            List<Data> dataObjects = new List<Data>();

            dataObjects.Add(new ViewData("CameraNumber", TVSMapper.entitiesNumbers["View"]));

            dataObjects.Add(new AnimationData("DockingNodePosition", "DockingNodeSpeed", TVSMapper.entitiesNumbers["DockingNode"]));
            dataObjects.Add(new AnimationData("DoorNodePosition", "DoorNodeSpeed", TVSMapper.entitiesNumbers["Door"]));
            dataObjects.Add(new AnimationData("LiveRollNodePosition", "LiveRollNodeSpeed", TVSMapper.entitiesNumbers["LiveRoll"]));
            dataObjects.Add(new AnimationData("MagazinDisassembleNodePosition", "MagazinDisassembleNodeSpeed", TVSMapper.entitiesNumbers["MagazinDisassemble"]));
            dataObjects.Add(new AnimationData("TvelPositioningNodePosition", "TvelPositioningNodeSpeed", TVSMapper.entitiesNumbers["TvelPositioning"]));
            dataObjects.Add(new AnimationData("TvelBeamAssembleNodePosition", "TvelBeamAssembleNodeSpeed", TVSMapper.entitiesNumbers["TvelBeamAssemble"]));
            dataObjects.Add(new AnimationData("AutooperatorNodePosition", "AutooperatorNodeSpeed", TVSMapper.entitiesNumbers["Autooperator"]));
            dataObjects.Add(new AnimationData("TitlerNodePosition", "TitlerNodeSpeed", TVSMapper.entitiesNumbers["Titler"]));

            dataObjects.Add(new AnimationData("HeadShorerNodePosition", "HeadShorerNodeSpeed", TVSMapper.entitiesNumbers["HeadShorer"]));
            dataObjects.Add(new AnimationData("CoordManipulatorNodePosition", "CoordManipulatorNodeSpeed", TVSMapper.entitiesNumbers["CoordManipulator"]));
            dataObjects.Add(new AnimationData("TailiDettachingNodePosition", "TailiDettachingNodeSpeed", TVSMapper.entitiesNumbers["TailiDettaching"]));
            dataObjects.Add(new AnimationData("CoordFacilityNodePosition", "CoordFacilityNodeSpeed", TVSMapper.entitiesNumbers["CoordFacility"]));
            dataObjects.Add(new AnimationData("TvelPusherNodePosition", "TvelPusherNodeSpeed", TVSMapper.entitiesNumbers["TvelPusher"]));
            dataObjects.Add(new AnimationData("TvelPullerNodePosition", "TvelPullerNodeSpeed", TVSMapper.entitiesNumbers["TvelPuller"]));
            dataObjects.Add(new AnimationData("TailiTransporterNodePosition", "TailiTransporterNodeSpeed", TVSMapper.entitiesNumbers["TailiTransporter"]));
            dataObjects.Add(new AnimationData("TransportFacilityNodePosition", "TransportFacilityNodeSpeed", TVSMapper.entitiesNumbers["TransportFacility"]));
            dataObjects.Add(new AnimationData("TruckTransmitingNodePosition", "TruckTransmitingNodeSpeed", TVSMapper.entitiesNumbers["TruckTransmiting"]));
            dataObjects.Add(new AnimationData("CraneManipulatorNodePosition", "CraneManipulatorNodeSpeed", TVSMapper.entitiesNumbers["CraneManipulator"]));

            return dataObjects;
        }
    }
}
