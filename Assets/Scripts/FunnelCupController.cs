﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Scripts
{
    public class FunnelCupController: MonoBehaviour
    {
        private Vector3 previousVec;

        private float YOffset;

        // Start is called before the first frame update
        void Start()
        {
            previousVec = target.up;
            //YOffset = this.transform.localPosition.y - target.localPosition.y;
        }

        public Transform target;
        public FunnelDrive funnelDriveObj;

        private bool KO;

        // Update is called once per frame
        void Update()
        {

            if (!funnelDriveObj.IsControllingByDrive())
            {
                previousVec = target.up;
                return;
            }

            if (this.transform.parent != GameCore.Instance.Center)
            {
                return;
            }
            else if (!KO)
            {
                YOffset = this.transform.localPosition.y - target.localPosition.y;
                KO = true;
            }

            Vector3 newUp = target.up;

            float angle = Vector3.SignedAngle(previousVec, newUp, Vector3.down);

            previousVec = newUp;

            //Debug.Log(angle);

            //Debug.Log($"YOffset: {YOffset}");

            this.transform.RotateAround(target.position, Vector3.down, angle);

            this.transform.localPosition = new Vector3(this.transform.localPosition.x, target.localPosition.y + YOffset, this.transform.localPosition.z);

        }

    }
}
