﻿using System.Text;
using UnityEngine;
using System;
using HybridWebSocket;



public partial class NetClient : MonoBehaviour
{

    static WebSocket ws;

    static string guid;
    static string suid;



    static void GuidInit()
    {
        if (!PlayerPrefs.HasKey("guid"))
        {
            guid = Guid.NewGuid().ToString();
            PlayerPrefs.SetString("guid", guid);
            PlayerPrefs.Save();
        }
        else
        {
            guid = PlayerPrefs.GetString("guid");
        }

    }


    static void CreateWSInstance()
    {
        if (ws != null)
            return;
        

        //create instance
        ws = WebSocketFactory.CreateInstance(WSOptions.ServerAddress);


        // Add OnOpen event listener
        ws.OnOpen += () =>
        {
            var state = ws.GetState();

            Debug.Log("WS connected!");
            Debug.Log("WS state: " + state.ToString());

            OnOpen?.Invoke(state);
        };

        // Add OnMessage event listener
        ws.OnMessage += (byte[] msg) =>
        {
            string data = Encoding.UTF8.GetString(msg);
            //Debug.Log("WS received message: " + data);

            OnMessage?.Invoke(msg);
            //ServeResponse(data);
        };

        // Add OnError event listener
        ws.OnError += (string errMsg) =>
        {
            Debug.Log("WS error: " + errMsg);

            OnError?.Invoke(errMsg);            
        };

        // Add OnClose event listener
        ws.OnClose += (WebSocketCloseCode code) =>
        {
            Debug.Log("WS closed with code: " + code.ToString());

            OnClose?.Invoke(code);
        };
    }


    static void ServeResponse(string data)
    {
        //parse data
        try
        {
            string[] data_list = data.Split('\t');
            string command = data_list[0];

            switch (command)
            {
                case "hi":
                    suid = data_list[1];
                    //OnHelloCallback?.Invoke(data_list);
                    break;
            }
        }
        catch
        {
            Debug.LogError("Wrong data packet...");
            return;
        }
    }


}