﻿using HybridWebSocket;



public delegate void OpenHandler(WebSocketState state);
public delegate void CloseHandler(WebSocketCloseCode code);

public delegate void ErrorHandler(string errMes);
public delegate void DataHandler(byte[] msg);

public delegate void CallbackEvent(params System.Object[] data);


public static class WSOptions
{
    public static string ServerAddress = "ws://127.0.0.1:8080/ws";
}
