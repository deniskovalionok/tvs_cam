﻿using System.Text;
using UnityEngine;
using System;
using HybridWebSocket;
using System.Collections;
using System.Threading.Tasks;
using System.Threading;
using Newtonsoft.Json;
using System.IO;
using System.Collections.Generic;
using Assets.Scripts;


public partial class NetClient : MonoBehaviour
{

    public static event OpenHandler    OnOpen;
    public static event CloseHandler   OnClose;
    public static event ErrorHandler   OnError;
    public static event DataHandler    OnMessage;

    private DataParser dataParser;
    public DataParser DataParser
    {
        get { return dataParser; }
        set
        {
            if(value!=null && value!=dataParser)
            {
                dataParser = value;
            }
        }
    }

    public void Connect()
    {            

        if (ws == null || IsReconnectionProcess)
        {
            GuidInit();
            CreateWSInstance();
        }

        try
        {
            ws?.Connect();
        }
        catch(Exception ex)
        {
            Debug.Log(ex.Message);
            Reconnect(2);
        }
        
    }

    //Start reconnection process
    private void Reconnect(float secondsToDelay)
    {
        StartCoroutine(ReconnectionWithDelay(secondsToDelay));
    }

    private IEnumerator ReconnectionWithDelay(float seconds)
    {
        yield return new WaitForSeconds(seconds);
        Connect();
    }


    public bool Connected
    {
        get
        {
            return ws.GetState() == WebSocketState.Open;
        }
    }


    public void Disconnect()
    {
        ws?.Close();
    }


    public void SendData(params string[] data)
    {
        try
        {
            string s = string.Join("\t", data);
            byte[] buffer = Encoding.ASCII.GetBytes(s);
            ws?.Send(buffer);
        } 
        catch (Exception e)
        {
            Debug.Log("Excpetion in SendData");
            Debug.LogError(e.Message);
        }
        
    }   

    void Start()
    {
        OnMessage += OnGetData;
        OnClose += NetClient_OnClose;
        OnError += NetClient_OnError;
        OnOpen += NetClient_OnOpen;        
        
    }

    void OnGetData(byte[] data)
    {
        string message = Encoding.UTF8.GetString(data);
        //Debug.Log(message);
        //return;
        try
        {
            DataParser.ExtractDataFromRaw(message);
        }
        catch (Exception e)
        {
            Debug.Log(e.Message);
        }
        
    }


    private bool IsReconnectionProcess;

    private void NetClient_OnOpen(WebSocketState state)
    {
        Debug.Log($"Socket opened in state: {state}");
        IsReconnectionProcess = false;
    }

    private void NetClient_OnError(string errMes)
    {
        Debug.Log($"Socket handles error: {errMes}");
        ws.Close(WebSocketCloseCode.Undefined, "for reconnection");
    }

    private void NetClient_OnClose(WebSocketCloseCode code)
    {
        Debug.Log($"Socket closed with code: {code}");
        IsReconnectionProcess = true;        
        UnityMainThreadDispatcher.Instance().Enqueue(() => Reconnect(2));
    }
}