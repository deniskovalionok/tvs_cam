﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Scripts
{
    public class Test: MonoBehaviour
    {
        public TMPro.TMP_InputField input;
        public TMPro.TMP_Dropdown comp;

        private void Start()
        {
            comp = this.gameObject.GetComponent<TMPro.TMP_Dropdown>();
            comp.onValueChanged.AddListener(new UnityEngine.Events.UnityAction<int>((x) => {

                input.text = comp.options[x].text;

            }));
        }

        private void Update()
        {
            if(Input.GetKeyDown(KeyCode.A))
            {
                input.Select();
                comp.Show();
            }
        }
    }
}
