﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using Assets.Scripts.Interfaces;
using Assets.Scripts.DataStructures;

namespace Assets.Scripts
{
    public class TruckController : MonoBehaviour, IController
    {

        private TruckData previousData;
        private float previousRealTime;
        private Vector3 TruckTrOffset;

        public Transform TruckTr;
        public Transform ForkTr;

        private Vector3 originForkPos;


        private TruckData _prD;

        private void Start()
        {
            TruckTrOffset = TruckTr.localPosition - ForkTr.localPosition;
            originForkPos = ForkTr.localPosition;
            _prD = new TruckData("", "", "", "", 1);
            _prD.XCoord = 5157;
            _prD.YCoord = 150;
            XCoordValue_Test = (int)_prD.XCoord;
            YCoordValue_Test = (int)_prD.YCoord;
            _prD.XVelocity = 0;
            _prD.YVelocity = 0;
            //SetData(_prD);
            //UpdateControllingParameters();
            //StartCoroutine(SimulateServerWork());
        }

        public int XCoordValue_Test;
        public int YCoordValue_Test;

        private void Update()
        {
            //UpdateControllingParameters();
        }

        private IEnumerator SimulateServerWork()
        {
            while (true)
            {
                if(Mathf.Abs((float)_prD.XCoord - XCoordValue_Test)<20)
                {
                    _prD.XVelocity = 0;
                    _prD.XCoord = XCoordValue_Test;
                }
                else
                {
                    _prD.XVelocity = Mathf.Sign(XCoordValue_Test - (float)_prD.XCoord) * 20;
                }

                if (Mathf.Abs((float)_prD.YCoord - YCoordValue_Test) < 20)
                {
                    _prD.YVelocity = 0;
                    _prD.YCoord = YCoordValue_Test;
                }
                else
                {
                    _prD.YVelocity = Mathf.Sign(YCoordValue_Test - (float)_prD.YCoord)*20;
                }


                _prD.XCoord+= _prD.XVelocity / 10;
                _prD.YCoord += _prD.YVelocity / 10;
                SetData(_prD);

                yield return new WaitForSeconds(0.1f);
            }


        }


        public void SetData(Data dataToSet)
        {
            Debug.Log(dataToSet);

            TruckData trData = (TruckData)dataToSet;
            previousData = trData;
            //previousRealTime = Time.realtimeSinceStartup;
        }

        public void UpdateControllingParameters()
        {
            float r = Time.realtimeSinceStartup;
            TranslateObject(r);
            previousRealTime = r;
        }

        private void TranslateObject(float time)
        {
            if(previousData==null)
            {
                return;
            }

            if(GameCore.Instance.IsLerp)
            {
                TruckTr.localPosition = new Vector3(Mathf.Lerp(TruckTr.localPosition.x, (float)previousData.XCoord / 1000, Time.deltaTime), TruckTr.localPosition.y, TruckTr.localPosition.z);
                ForkTr.localPosition = new Vector3(ForkTr.localPosition.x, Mathf.Lerp(ForkTr.localPosition.y, originForkPos.y + (float)previousData.YCoord / 1000, Time.deltaTime), ForkTr.localPosition.z);
            }
            else
            {
                TruckTr.localPosition = new Vector3(Mathf.MoveTowards(TruckTr.localPosition.x, (float)previousData.XCoord / 1000, Math.Abs((float)previousData.XVelocity/1000) * (time - previousRealTime)), TruckTr.localPosition.y, TruckTr.localPosition.z);
                ForkTr.localPosition = new Vector3(ForkTr.localPosition.x, Mathf.MoveTowards(ForkTr.localPosition.y, originForkPos.y + (float)previousData.YCoord / 1000, Math.Abs((float)previousData.YVelocity/1000) * (time - previousRealTime)), ForkTr.localPosition.z);
            }

            if(previousData.XVelocity == 0)
            {
                TruckTr.localPosition = new Vector3((float)previousData.XCoord/1000, TruckTr.localPosition.y, TruckTr.localPosition.z);                
            }
            if(previousData.YVelocity == 0)
            {
                ForkTr.localPosition = new Vector3(ForkTr.localPosition.x, originForkPos.y + (float)previousData.YCoord/1000, ForkTr.localPosition.z);
            }
        }

        public void DataUpdatedHandler()
        {
            previousRealTime = Time.realtimeSinceStartup;
        }
    }
}
