﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Assets.Scripts.Interfaces;
using Assets.Scripts.DataStructures;

namespace Assets.Scripts
{
    public class VortexGrindingDataHandler : IDataProvider
    {

        //public event Action DataAccepted;

        //private Dictionary<string, Data> dataStructuresByParams;        
        //private Dictionary<Data, IController> controllersByDataStructures;

        //private IRefsHandler refsHandler;        

        //public VortexGrindingDataHandler(/*IRefsHandler refsHandler*/)/*:base(refsHandler, null)*/
        //{           
        //}
 

        //public void ApplyDataChanges()
        //{
        //    //foreach (var item in controllersByDataStructures)
        //    //{
        //    //    UnityEngine.Debug.Log($"Key: {item.Key}; Value: {item.Value};");
        //    //}

        //    foreach (var item in controllersByDataStructures)
        //    {
        //        //UnityEngine.Debug.Log(item.Key == null);
        //        //UnityEngine.Debug.Log(item.Value);
        //        item.Value.SetData(item.Key);
        //    }
        //    //UnityEngine.Debug.Log("Accepted!!!");
        //    DataAccepted?.Invoke();
        //}

        //public void SetDataValue(string paramName, object paramValue)
        //{
        //    try
        //    {
        //        if (dataStructuresByParams.ContainsKey(paramName))
        //        {
        //            dataStructuresByParams[paramName].SetParameterValue(paramName, paramValue);
        //        }
        //    }
        //    catch(Exception e)
        //    {
        //        UnityEngine.Debug.Log(e);
        //    }

          
        //}

        ////Инициирует заполнение словарей
        //private void SetDictionaries()
        //{
        //    foreach (var item in dataStructures)
        //    {
        //        ConnectToDictionary(item);
        //        ConnectControllerToDataStructure(item);
        //    }
        //}

        ////Заполняет словарь уникальными ключами - названиями переменных и значениями - объектами-данными
        //private void ConnectToDictionary(Data structure)
        //{
        //    foreach (var item in structure.GetNextParamName())
        //    {
        //        dataStructuresByParams.Add(item, structure);
        //    }
        //}

        ////Заполняет словарь ключами - объектами-данными и значениями - контроллерами объектов на сцене
        //private void ConnectControllerToDataStructure(Data structure)
        //{
        //    controllersByDataStructures.Add(structure, refsHandler.GetControllerByEntityID(structure.GetDataId()));
        //}

        public IEnumerable<Data> GetDataObjects()
        {
            List<Data> dataStructures = new List<Data>();



            //Big container 01
            ContainerData container01Data = new ContainerData("Container01XVelocity", "Container01YVelocity", "Container01ZVelocity",
                "Container01XCoord", "Container01YCoord", "Container01ZCoord", "Container01RotateVel", "Container01AxePosition", VortexGrindingMapper.entitiesNumbers["Container1"]);

            ContainerData container02Data = new ContainerData("Container02XVelocity", "Container02YVelocity", "Container02ZVelocity",
                "Container02XCoord", "Container02YCoord", "Container02ZCoord", "Container02RotateVel", "Container02AxePosition", VortexGrindingMapper.entitiesNumbers["Container2"]);

            ContainerData container03Data = new ContainerData("Container03XVelocity", "Container03YVelocity", "Container03ZVelocity",
                "Container03XCoord", "Container03YCoord", "Container03ZCoord", "Container03RotateVel", "Container03AxePosition", VortexGrindingMapper.entitiesNumbers["Container3"]);

            ContainerData container04Data = new ContainerData("Container04XVelocity", "Container04YVelocity", "Container04ZVelocity",
                "Container04XCoord", "Container04YCoord", "Container04ZCoord", "Container04RotateVel", "Container04AxePosition", VortexGrindingMapper.entitiesNumbers["Container4"]);

            ContainerData container05Data = new ContainerData("Container05XVelocity", "Container05YVelocity", "Container05ZVelocity",
                "Container05XCoord", "Container05YCoord", "Container05ZCoord", "Container05RotateVel", "Container05AxePosition", VortexGrindingMapper.entitiesNumbers["Container5"]);

            ContainerData container06Data = new ContainerData("Container06XVelocity", "Container06YVelocity", "Container06ZVelocity",
                "Container06XCoord", "Container06YCoord", "Container06ZCoord", "Container06RotateVel", "Container06AxePosition", VortexGrindingMapper.entitiesNumbers["Container6"]);

            ContainerData container07Data = new ContainerData("Container07XVelocity", "Container07YVelocity", "Container07ZVelocity",
                "Container07XCoord", "Container07YCoord", "Container07ZCoord", "Container07RotateVel", "Container07AxePosition", VortexGrindingMapper.entitiesNumbers["Container7"]);

            ContainerData container08Data = new ContainerData("Container08XVelocity", "Container08YVelocity", "Container08ZVelocity",
                "Container08XCoord", "Container08YCoord", "Container08ZCoord", "Container08RotateVel", "Container08AxePosition", VortexGrindingMapper.entitiesNumbers["Container8"]);

            ContainerData container09Data = new ContainerData("Container09XVelocity", "Container09YVelocity", "Container09ZVelocity",
                "Container09XCoord", "Container09YCoord", "Container09ZCoord", "Container09RotateVel", "Container09AxePosition", VortexGrindingMapper.entitiesNumbers["Container9"]);

            TransporterData t01Data = new TransporterData("Transporter01XVelocity", "Transporter01ZVelocity", "Transporter01XCoord", "Transporter01ZCoord",
                "Transporter01RotVelocity", "Transporter01AxePos", VortexGrindingMapper.entitiesNumbers["Transporter1"]);

            TransporterData t02Data = new TransporterData("Transporter02XVelocity", "Transporter02ZVelocity", "Transporter02XCoord", "Transporter02ZCoord",
                "Transporter02RotVelocity", "Transporter02AxePos", VortexGrindingMapper.entitiesNumbers["Transporter2"]);

            TransporterData t03Data = new TransporterData("Transporter03XVelocity", "Transporter03ZVelocity", "Transporter03XCoord", "Transporter03ZCoord",
                "Transporter03RotVelocity", "Transporter03AxePos", VortexGrindingMapper.entitiesNumbers["Transporter3"]);

            TransporterData t04Data = new TransporterData("Transporter04XVelocity", "Transporter04ZVelocity", "Transporter04XCoord", "Transporter04ZCoord",
                "Transporter04RotVelocity", "Transporter04AxePos", VortexGrindingMapper.entitiesNumbers["Transporter4"]);

            TransporterData t05Data = new TransporterData("Transporter05XVelocity", "Transporter05ZVelocity", "Transporter05XCoord", "Transporter05ZCoord",
                "Transporter05RotVelocity", "Transporter05AxePos", VortexGrindingMapper.entitiesNumbers["Transporter5"]);

            TransporterData t06Data = new TransporterData("Transporter06XVelocity", "Transporter06ZVelocity", "Transporter06XCoord", "Transporter06ZCoord",
                "Transporter06RotVelocity", "Transporter06AxePos", VortexGrindingMapper.entitiesNumbers["Transporter6"]);

            TransporterData t07Data = new TransporterData("Transporter07XVelocity", "Transporter07ZVelocity", "Transporter07XCoord", "Transporter07ZCoord",
                "Transporter07RotVelocity", "Transporter07AxePos", VortexGrindingMapper.entitiesNumbers["Transporter7"]);

            TransporterData t08Data = new TransporterData("Transporter08XVelocity", "Transporter08ZVelocity", "Transporter08XCoord", "Transporter08ZCoord",
                "Transporter08RotVelocity", "Transporter08AxePos", VortexGrindingMapper.entitiesNumbers["Transporter8"]);

            TransporterData t09Data = new TransporterData("Transporter09XVelocity", "Transporter09ZVelocity", "Transporter09XCoord", "Transporter09ZCoord",
                "Transporter09RotVelocity", "Transporter09AxePos", VortexGrindingMapper.entitiesNumbers["Transporter9"]);

            TransporterData t10Data = new TransporterData("Transporter10XVelocity", "Transporter10ZVelocity", "Transporter10XCoord", "Transporter10ZCoord",
                "Transporter10RotVelocity", "Transporter10AxePos", VortexGrindingMapper.entitiesNumbers["Transporter10"]);

            TransportTitlerData trTitlerData = new TransportTitlerData("TitlerXVelocity", "TitlerZVelocity", "TitlerXCoord", "TitlerZCoord",
                "TitlerRotVelocity", "TitlerAxePos", "TitlerGrabRotVelocity", "TitlerGrabAxePos", VortexGrindingMapper.entitiesNumbers["Titler"]);
            //Camera
            ViewData viewData = new ViewData("CameraNumber", VortexGrindingMapper.entitiesNumbers["ViewController"]);
            //Light
            LightData lightData = new LightData("LightState", VortexGrindingMapper.entitiesNumbers["LightController"]);

            dataStructures.Add(container01Data);
            dataStructures.Add(t01Data);
            dataStructures.Add(t02Data);
            dataStructures.Add(t03Data);
            dataStructures.Add(t04Data);
            dataStructures.Add(t05Data);
            dataStructures.Add(t06Data);
            dataStructures.Add(t07Data);
            dataStructures.Add(t08Data);
            dataStructures.Add(t09Data);
            dataStructures.Add(t10Data);
            dataStructures.Add(viewData);
            dataStructures.Add(lightData);
            dataStructures.Add(trTitlerData);
            dataStructures.Add(container02Data);
            dataStructures.Add(container03Data);
            dataStructures.Add(container04Data);
            dataStructures.Add(container05Data);
            dataStructures.Add(container06Data);
            dataStructures.Add(container07Data);
            dataStructures.Add(container08Data);
            dataStructures.Add(container09Data);

            return dataStructures;

        }
    }
}
