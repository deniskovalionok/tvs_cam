﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using Assets.Scripts.Interfaces;
using Assets.Scripts.DataStructures;

namespace Assets.Scripts
{
    public class PlutoGateway : MonoBehaviour, IController
    {

        private PlutoGatewayData previousData;
        private Vector3 originPosition;
        private Quaternion originRotation;
        private float previousRealTime;

        public Transform plutoGatewayObj;        


        private void Start()
        {
            originPosition = plutoGatewayObj.localPosition;
            originRotation = plutoGatewayObj.localRotation;

            //_pd = new PlutoGatewayData("", "", "", "", 0);

            //_pd.ZCoord = 0;
            //_pd.ZVelocity = 20;

            //StartCoroutine(DelayInvoking(0.1f));
        }

        public void SetData(Data dataToSet)
        {
            PlutoGatewayData data = dataToSet as PlutoGatewayData;
            previousData = data;
            //target = originRotation * Quaternion.AngleAxis(180 * (float)previousData.AxePosition, Vector3.forward); - original code            
            //target = originRotation * Quaternion.AngleAxis(85 * (float)previousData.AxePosition, Vector3.forward);
            //previousRealTime = Time.realtimeSinceStartup;
        }

        private Quaternion target;
        private float rotateValue = 0.1f;

        public void UpdateControllingParameters()
        {
            float r = Time.realtimeSinceStartup;

            if (GameCore.Instance.IsLerp)
            {
                plutoGatewayObj.localPosition = new Vector3(plutoGatewayObj.localPosition.x, Mathf.Lerp(plutoGatewayObj.localPosition.y, originPosition.y + (float)previousData.ZCoord / 1000, Time.deltaTime), plutoGatewayObj.localPosition.z);
            }
            else
            {
               plutoGatewayObj.localPosition = new Vector3(plutoGatewayObj.localPosition.x,
               Mathf.MoveTowards(plutoGatewayObj.localPosition.y, originPosition.y + (float)previousData.ZCoord / 1000, (Mathf.Abs((float)previousData.ZVelocity) / 1000) * (r - previousRealTime)),
               plutoGatewayObj.localPosition.z);
            }                   
                   
            if (plutoGatewayObj.localRotation != target)
            {
                //Debug.Log("to target!");
                //Debug.Log(previousData.AxePosition);
                plutoGatewayObj.localRotation = Quaternion.RotateTowards(plutoGatewayObj.localRotation, target, 85 * /*rotateValue*/ Mathf.Abs((float)previousData.RotatingVelocity) * (r - previousRealTime));
            }

            previousRealTime = r;

            if (previousData.ZVelocity == 0)
            {
                plutoGatewayObj.localPosition = new Vector3(plutoGatewayObj.localPosition.x, originPosition.y + (float)previousData.ZCoord / 1000, plutoGatewayObj.localPosition.z);
            }

            if (previousData.RotatingVelocity == 0)
            {
                plutoGatewayObj.localRotation = target;
            }
        }

        public void DataUpdatedHandler()
        {
            target = originRotation * Quaternion.AngleAxis(85 * (float)previousData.AxePosition, Vector3.forward);
            previousRealTime = Time.realtimeSinceStartup;
        }


        //private void Update()
        //{
        //    UpdateControllingParameters();
        //}

        //private PlutoGatewayData _pd;

        //private IEnumerator DelayInvoking(float time)
        //{
        //    for(int i = 0; i<8; i++)
        //    {
        //        _pd.ZCoord += +_pd.ZVelocity;
        //        SetData(_pd);
        //        yield return new WaitForSeconds(time);
        //    }            
        //}
    }
}
