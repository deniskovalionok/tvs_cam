﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Assets.Scripts.Interfaces;
using System;
using Assets.Scripts.DataStructures;

public class Titler : MonoBehaviour, IController
{

    public Transform GetCurrentContainer { get; private set; }

    public Transform GetTitlerTransform { get; private set; }

    private TitlerData previousData;

    private float previousRealTime;

    private Vector3 forward;
    private Vector3 back;

    private void Start()
    {
        GetTitlerTransform = this.transform;
        savedTime = Time.time;

        //target = GetTitlerTransform.localRotation* Quaternion.AngleAxis(180 * rotateValue, Vector3.left);
        originRotation = GetTitlerTransform.localRotation;
        theAbsoluteRotation = originRotation * Quaternion.AngleAxis(180, Vector3.left);
        forward = GetTitlerTransform.TransformPoint(new Vector3(0,0,1));
        back = forward * (-1);
    }


    public void AttachChild(Transform child)
    {
        child.parent = GetTitlerTransform;
    }

    public void DettachChild(Transform child)
    {
        child.parent = null;
    }

    //Должно проверяться находиться ли объект в области указанной позиции
    private bool IsInPositionArea(Vector3 position)
    {
        return false;
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.transform.tag == "Container")
        {
            GetCurrentContainer = collision.transform;
        }            
    }

    private void OnCollisionExit(Collision collision)
    {
        if(collision.transform.tag == "Container")
        {
            GetCurrentContainer = null;
        }

        
    }

    private bool IsAxeChanging;

    public void SetData(Data dataToSet)
    {
        TitlerData data = dataToSet as TitlerData;
        //GetTitlerTransform.localRotation = target;
        //if(previousData != null)
        //{
        //    IsAxeChanging = Mathf.Abs((float)(data.AxePosition - previousData.AxePosition)) > 0.01f;
        //}        
        previousData = data;
        //Debug.Log(previousData.AxePosition);
        //target = originRotation * Quaternion.AngleAxis(180 * (float)previousData.AxePosition, Vector3.left);
        ////GetTitlerTransform.localRotation = target;        
        //previousRealTime = Time.realtimeSinceStartup;
    }

    private float rotateValue = 0.1f;
    private float savedTime;
    private Quaternion target;
    private Quaternion originRotation;
    private Quaternion theAbsoluteRotation;

    Vector3 v;

    public void UpdateControllingParameters()
    {


        //Debug.DrawLine(GetTitlerTransform.localPosition, forward, Color.red);
        //Debug.DrawLine(GetTitlerTransform.localPosition, back, Color.blue);

        float r = Time.realtimeSinceStartup;

        if (GetTitlerTransform.localRotation!=target)
        {
            GetTitlerTransform.localRotation = Quaternion.RotateTowards(GetTitlerTransform.localRotation, target, 180 * /*rotateValue*/ Mathf.Abs((float)previousData.RotatingVelocity) * (r - previousRealTime));
        }
        
        //GetTitlerTransform.localEulerAngles = new Vector3(Mathf.MoveTowardsAngle(GetTitlerTransform.localEulerAngles.x, target.eulerAngles.x, (originRotation * Quaternion.AngleAxis(180 * Math.Abs((float)previousData.RotatingVelocity), Vector3.right)).eulerAngles.x * (r-previousRealTime)), GetTitlerTransform.localEulerAngles.y, GetTitlerTransform.localEulerAngles.z);

        //GetTitlerTransform.localRotation = Quaternion.Lerp(GetTitlerTransform.localRotation, target, Time.deltaTime/0.5f); 
        
        //forward = Vector3.RotateTowards(forward, back, 180 * (float)previousData.RotatingVelocity * (r - previousRealTime)*Mathf.PI/180, 0.0f);
        
        
        //Debug.DrawLine(GetTitlerTransform.localPosition, v, Color.black);
        //GetTitlerTransform.LookAt(forward);

        previousRealTime = r;

        //if(IsAxeChanging)
        //{
        //    rotateValue = 0;
        //}
        //else
        //{
        //    rotateValue = 0.1f;
        //}

        if (previousData.RotatingVelocity == 0)
        {
            GetTitlerTransform.localRotation = target;
            //rotateValue = 0;
        }

    }

    public void DataUpdatedHandler()
    {
        target = originRotation * Quaternion.AngleAxis(180 * (float)previousData.AxePosition, Vector3.left);        
        previousRealTime = Time.realtimeSinceStartup;
    }
}