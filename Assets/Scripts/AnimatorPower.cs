﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Scripts
{
    public class AnimatorPower: MonoBehaviour
    {

        public Animator tvelLoading;
        public Animator tvsAssembling;

        public int TvelCount;

        private int currentTvel;

        private void Start()
        {
            tvelLoading.SetTrigger("TvelStart");
        }


        public void TvelStart()
        {
            if (tvelLoading.GetCurrentAnimatorStateInfo(0).speed > 0)
                return;

            tvelLoading.SetTrigger("TvelStart");
        }

        public void TvelLoaded()
        {
            if (tvelLoading.GetCurrentAnimatorStateInfo(0).speed < 0)
                return;

            currentTvel += 1;
            if(currentTvel==TvelCount)
            {
                tvelLoading.SetTrigger("TransportOut");
                currentTvel = 0;
            }
            else
            {
                tvelLoading.SetTrigger("TvelAgain");                
            }
        }


        public void TvelLoadingEnded()
        {
            if (tvelLoading.GetCurrentAnimatorStateInfo(0).speed < 0)
                return;

            tvsAssembling.SetTrigger("TVSAssemble");
        }

        public void ReverseLoading()
        {
            if (tvelLoading.GetCurrentAnimatorStateInfo(0).speed > 0)
                return;

            currentTvel += 1;
            if (currentTvel == TvelCount)
            {
                tvelLoading.SetTrigger("TvelEnded");
                currentTvel = 0;
            }
            else
            {
                tvelLoading.SetTrigger("TvelAgain");
            }

        }

    }
}
