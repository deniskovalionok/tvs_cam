﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Scripts
{
    public class TVSAssemblerController: MonoBehaviour
    {


        public Animator TvelLoading;
        public Animator TVSAssembling;



        public void ReverseAnimation()
        {
            if(TVSAssembling.GetCurrentAnimatorStateInfo(0).speed<0)
            {
                TvelLoading.SetTrigger("Reverse");
            }
            else
            {
                Debug.Log("Speed!");
            }
            
        }

    }
}
