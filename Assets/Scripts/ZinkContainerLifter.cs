﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using Assets.Scripts.Interfaces;
using Assets.Scripts.DataStructures;

namespace Assets.Scripts
{
    public class ZinkContainerLifter: MonoBehaviour, IController
    {

        private ZinkContainerLifterData previousData;
        private float previousRealTime;
        public Transform zinkLifter;
        private Vector3 zinkLifterOriginPos;

        private void Start()
        {
            zinkLifterOriginPos = zinkLifter.localPosition;
            _prData = new ZinkContainerLifterData("", "", -1);
            //StartCoroutine(Process());
        }


        public void SetData(Data dataToSet)
        {
            previousData = (ZinkContainerLifterData)dataToSet;            
            
        }

        private IEnumerator Process()
        {
            //_prData.Zvelocity = 20;
            //SetData(_prData);
            //UpdateControllingParameters();
            //while (_prData.ZCoord < 50)
            //{
            //    _prData.ZCoord += _prData.Zvelocity * 0.1f;
            //    yield return new WaitForSeconds(0.1f);
            //}
            //_prData.ZCoord= 50;
            //_prData.Zvelocity = 0;
            _prData.YVelocity = 20f;
            SetData(_prData);
            UpdateControllingParameters();
            while (_prData.YCoord < 1400)
            {
                _prData.YCoord += _prData.YVelocity * 0.1f;
                SetData(_prData);
                yield return new WaitForSeconds(0.1f);
            }
            _prData.YVelocity = 0;
            _prData.YCoord = 1400;
            SetData(_prData);
        }

        private void Update()
        {
            //UpdateControllingParameters();
        }

        private ZinkContainerLifterData _prData;

        public void UpdateControllingParameters()
        {
            float r = Time.realtimeSinceStartup;
            Moving(r);
            previousRealTime = r;
        }

        private void Moving(float currentTime)
        {

            if (GameCore.Instance.IsLerp)
            {
                zinkLifter.localPosition = new Vector3(zinkLifter.localPosition.x, Mathf.Lerp(zinkLifter.localPosition.y, zinkLifterOriginPos.y + (float)previousData.YCoord / 1000, Time.deltaTime), zinkLifter.localPosition.z);
            }
            else
            {
                zinkLifter.localPosition = new Vector3(zinkLifter.localPosition.x,
                    Mathf.MoveTowards(zinkLifter.localPosition.z, zinkLifterOriginPos.y + (float)previousData.YCoord / 1000, (Mathf.Abs((float)previousData.YVelocity) / 1000) * (currentTime - previousRealTime)),
                    zinkLifter.localPosition.z);
            }

            if(previousData.YVelocity == 0)
            {
                zinkLifter.localPosition = new Vector3(zinkLifter.localPosition.x, zinkLifterOriginPos.y + (float)previousData.YCoord / 1000, zinkLifter.localPosition.z);
            }
        }

        public void DataUpdatedHandler()
        {
            previousRealTime = Time.realtimeSinceStartup;
        }
    }
}
