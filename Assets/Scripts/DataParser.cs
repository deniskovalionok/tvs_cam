﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.IO;
using Assets.Scripts.Interfaces;
using Assets.Scripts.DataStructures;

namespace Assets.Scripts
{
    public class DataParser
    {                
        //Объект, работающий со всеми структурами, работающими с данными.
        private DataStructuresHandler dataHandler;

        public DataParser(DataStructuresHandler dataHandler)
        {            
            this.dataHandler = dataHandler;   
        }      

        /// <summary>
        /// Извлечение данных из JSON-сообщения по имени параметра.
        /// Применен ручной парсинг в целях оптимизации выполнения.
        /// </summary>
        /// <param name="jsonData">Данные, полученные от opc-сервера</param>
        public async void ExtractDataFromRaw(string jsonData)
        {

            //UnityEngine.Debug.Log(jsonData);

            using (JsonTextReader reader = new JsonTextReader(new StringReader(jsonData)))
            {

                string paramName = null;
                string paramValue = null;

                try
                {
                    while (reader.Read())
                    {                       

                        if (reader.Value != null)
                        {                            
                            if (paramName != null)
                            {                                
                                try
                                {                             
                                    dataHandler.SetDataValue(paramName, reader.Value);
                                }
                                catch (Exception ex)
                                {
                                    UnityEngine.Debug.Log(ex);
                                }

                            }

                            if (reader.TokenType == JsonToken.PropertyName)
                            {
                                paramName = reader.Value as string;
                            }
                            else
                            {
                                paramName = null;
                            }
                        }
                        else
                        {
                            paramName = null;
                        }
                    }
                }
                catch(Exception e)
                {
                    UnityEngine.Debug.Log(e);
                }
                            
            }

            //Вызов применения изменений на всех объектах в главном потоке
            UnityMainThreadDispatcher.Instance().Enqueue(() => dataHandler.NotifyDataChanged());
        }
    }
}
