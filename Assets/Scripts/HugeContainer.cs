﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using Assets.Scripts.DataStructures;
using Assets.Scripts.Interfaces;
using System.Collections;

namespace Assets.Scripts
{
    public class HugeContainer: MonoBehaviour, IController
    {

        private ContainerData previousData;

        public Transform containerObject;        
        
        //public Titler titlerController;
        //public Manipulator manipulatorController;

        private Vector3 originPos;

        private bool IsTranslationSelfControlling = true;
        private bool IsRotationSelfControlling = true;

        private bool IsAttachedToManipulator;
        private bool IsAttachedToTitler;

        private float previousRealTime;

        private float rotateValue = 0.1f;
        private Quaternion target;        
        private Quaternion originRotation;
       

        private void Start()
        {
            originPos = containerObject.localPosition;
            previousRealTime = Time.realtimeSinceStartup;
            targetPos = containerObject.localPosition;            

            if(containerObject!=null)
                originRotation = containerObject.localRotation;
           
        }

        private IEnumerator UpdateData()
        {
            _test.XCoord = 0;
            _test.XVelocity = 20;
            //_test.ZCoord = 0;
            //_test.ZVelocity = 0;
            //_test.AxePosition = 0;
            //_test.RotateVel = 0.1f;
            //_test.GrabRotateVel = 0.1f;
            //_test.GrabAxePosition = 0f;
            while (true)
            {
                if(_test.XCoord<1248)
                _test.XCoord += _test.XVelocity;

                //_test.ZCoord += _test.ZVelocity;
                //if (_test.AxePosition <= 1)
                //{
                //    _test.GrabAxePosition += _test.GrabRotateVel;
                //}


                SetData(_test);
                yield return new WaitForSecondsRealtime(1);
            }
        }

        //private void Update()
        //{
        //if(manipulatorController.IsReachedContainerSpawn && manipulatorController.IsGrabbingProcess())
        //{                                
        //    Transform cont = manipulatorController.GetCurrentContainer;
        //    if(cont!=null)
        //    {
        //        manipulatorController.AttachChild(cont);
        //        IsAttachedToManipulator = true;
        //        IsTranslationSelfControlling = false;
        //    }
        //}

        //if(IsAttachedToManipulator && !manipulatorController.IsGrabbingProcess())
        //{
        //    Transform cont = manipulatorController.GetCurrentContainer;
        //    if (cont != null)
        //    {
        //        manipulatorController.DettachChild(cont);
        //        IsAttachedToManipulator = false;
        //        IsTranslationSelfControlling = true;
        //    }
        //}

        //if(manipulatorController.IsReachedTitler && !manipulatorController.IsGrabbingProcess())
        //{
        //    Transform cont = titlerController.GetCurrentContainer;
        //    if(cont!=null)
        //    {
        //        titlerController.AttachChild(cont);
        //        IsAttachedToTitler = true;
        //        IsTranslationSelfControlling = false;
        //        IsRotationSelfControlling = false;
        //    }
        //}



        //}


        private ContainerData _test;

        private void Update()
        {


           // UpdateControllingParameters();



            //if (Input.GetKeyDown(KeyCode.Space))
            //{

            //    _test.XVelocity *= (-1);
            //    //_test.GrabAxePosition = 0.99f;
            //    //b = true;
            //    //_fromGrabToHorizOffset = HorizontalDrive.localPosition - GrabDrive.localPosition;
            //    //Debug.Log(_test.RotateVel);
            //}

            //if (Input.GetKeyDown(KeyCode.K))
            //{
            //    _test.XVelocity = 0;
            //}
        }

        private Vector3 targetPos;

        public void SetData(Data dataToSet)
        {
            ContainerData actualData = (ContainerData)dataToSet;
            //Debug.Log($"{actualData.YCoord}");
            previousData = actualData;
            //firstContainerObj.localPosition = new Vector3((float)previousData.XCoord / 1000, firstContainerObj.localPosition.y, firstContainerObj.localPosition.z);
            //target = originRotation * Quaternion.AngleAxis(180 * (float)previousData.AxePosition, Vector3.left);            
            //previousRealTime = Time.realtimeSinceStartup;
            //targetPos = firstContainerObj.localPosition + new Vector3((float)previousData.XVelocity/10000, 0, 0);
        }
        
        

        public void UpdateControllingParameters()
        {

            float r = Time.realtimeSinceStartup;

            //Translate and rotate object by speed

            if(containerObject!=null)
            {
                TranslateObject(containerObject, r, target);
            }
            
            previousRealTime = r;

        }

        private void TranslateObject(Transform container, float r, Quaternion target)
        {

            //firstContainerObj.localPosition = new Vector3(Mathf.Lerp(firstContainerObj.localPosition.x, previousData.XCoord / 1000, Time.deltaTime), Mathf.Lerp(firstContainerObj.localPosition.y, previousData.ZCoord / 1000, Time.deltaTime), Mathf.Lerp(firstContainerObj.localPosition.z, previousData.YCoord / 1000, Time.deltaTime));
            //if(Vector3.Distance(targetPos, firstContainerObj.localPosition)>0.001)
            //{

            //firstContainerObj.localPosition = Vector3.MoveTowards(firstContainerObj.localPosition, new Vector3((float)previousData.XCoord/1000, firstContainerObj.localPosition.y, firstContainerObj.localPosition.z), (Mathf.Abs((float)previousData.XVelocity) / 1000) * (r - previousRealTime));

            if (IsTranslationSelfControlling)
            {

                if(GameCore.Instance.IsLerp)
                {
                    container.parent.localPosition = new Vector3(Mathf.Lerp(container.parent.localPosition.x, (float)previousData.XCoord / 1000, Time.deltaTime), Mathf.Lerp(container.parent.localPosition.y, originPos.y+(float)previousData.ZCoord / 1000, Time.deltaTime), Mathf.Lerp(container.parent.localPosition.z, (float)previousData.YCoord / 1000, Time.deltaTime));
                }
                else
                {
                    container.parent.localPosition = new Vector3(Mathf.MoveTowards(container.parent.localPosition.x, (float)previousData.XCoord / 1000, (Mathf.Abs((float)previousData.XVelocity) / 1000) * (r - previousRealTime)),
                            Mathf.MoveTowards(container.parent.localPosition.y, originPos.y + (float)previousData.ZCoord / 1000, (Mathf.Abs((float)previousData.ZVelocity) / 1000) * (r - previousRealTime)),
                            Mathf.MoveTowards(container.parent.localPosition.z, (float)previousData.YCoord / 1000, (Mathf.Abs((float)previousData.YVelocity) / 1000) * (r - previousRealTime)));
                }



            }


            if (IsRotationSelfControlling)
            {
                if (container.localRotation != target)
                {
                    container.localRotation = Quaternion.RotateTowards(container.localRotation, target, 180 * /*rotateValue*/ Mathf.Abs((float)previousData.RotatingVelocity) * (r - previousRealTime));
                }               
              
            }

            if (previousData.XVelocity == 0)
            {
                container.parent.localPosition = new Vector3((float)previousData.XCoord / 1000, container.parent.localPosition.y, container.parent.localPosition.z);
            }
            if (previousData.YVelocity == 0)
            {
                container.parent.localPosition = new Vector3(container.parent.localPosition.x, container.parent.localPosition.y, (float)previousData.YCoord / 1000);
            }
            if (previousData.ZVelocity == 0)
            {
                container.parent.localPosition = new Vector3(container.parent.localPosition.x, originPos.y + (float)previousData.ZCoord / 1000, container.parent.localPosition.z);
            }

            if (previousData.RotatingVelocity == 0)
            {
                container.localRotation = target;
            }
            //if (secondContainerObj != null)
            //{
            //    secondContainerObj.localPosition = new Vector3(Mathf.Lerp(secondContainerObj.localPosition.x, previousData.XCoord / 1000, Time.deltaTime), Mathf.Lerp(secondContainerObj.localPosition.y, previousData.ZCoord / 1000, Time.deltaTime), Mathf.Lerp(secondContainerObj.localPosition.z, previousData.YCoord / 1000, Time.deltaTime));
            //}
            //if (thirdContainerObj != null)
            //{
            //    thirdContainerObj.localPosition = new Vector3(Mathf.Lerp(thirdContainerObj.localPosition.x, previousData.XCoord / 1000, Time.deltaTime), Mathf.Lerp(thirdContainerObj.localPosition.y, previousData.ZCoord / 1000, Time.deltaTime), Mathf.Lerp(thirdContainerObj.localPosition.z, previousData.YCoord / 1000, Time.deltaTime));
            //}


        }

        private void RotateObject()
        {            

            if(IsRotationSelfControlling)
            {
                //Rotating logic
            }

        }

        public void DataUpdatedHandler()
        {
            target = originRotation * Quaternion.AngleAxis(180 * (float)previousData.AxePosition, Vector3.left);
            previousRealTime = Time.realtimeSinceStartup;
        }
    }
}
