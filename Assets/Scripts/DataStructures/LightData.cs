﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Assets.Scripts.Interfaces;

namespace Assets.Scripts.DataStructures
{
    public class LightData : Data
    {
        public long LightTrigger;

        private string lightTriggerParamName;

        private List<string> parameters;

        public LightData(string lightTriggerParamName, int entityId) : base(entityId)
        {
            parameters = new List<string>();        
            this.lightTriggerParamName = lightTriggerParamName;
            parameters.Add(lightTriggerParamName);
        }

        public override IEnumerable<string> GetAllDataParameterNames()
        {
            foreach (var item in parameters)
            {
                yield return item;
            }
        }

        public override void SetParameterValue(string paramName, object value)
        {
            if(paramName == lightTriggerParamName)
            {
                LightTrigger = (long)value;
            }
        }
    }
}
