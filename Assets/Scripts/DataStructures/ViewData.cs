﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Assets.Scripts.Interfaces;

namespace Assets.Scripts.DataStructures
{
    public class ViewData : Data
    {

        public long CameraNumber;

        private string cameraNumberParamName;

        private List<string> parameters;

        public ViewData(string cameraNumberParamName, int entityId) : base(entityId)
        {
            parameters = new List<string>();

            this.cameraNumberParamName = cameraNumberParamName;
            parameters.Add(cameraNumberParamName);
        }

        public override IEnumerable<string> GetAllDataParameterNames()
        {
            foreach (var item in parameters)
            {
                yield return item;
            }
        }

        public override void SetParameterValue(string paramName, object value)
        {
            //return;

            if(paramName == cameraNumberParamName)
            {
                CameraNumber = (long)value;
            }
        }
    }
}
