﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Assets.Scripts.Interfaces;

namespace Assets.Scripts.DataStructures
{
    public class CapHandlerData : Data
    {
        public double ZCoord;
        public double ZVelocity;
        public double RotateVel;
        public double AxePosition;
        public long State;

        private string zCoordParamName;
        private string zVelocityParamName;
        private string rotateVelParamName;
        private string axePositionParamName;
        private string stateParamName;


        private List<string> parameters;

        public CapHandlerData(string zCoordParamName, string zVelocityParamName, string rotateVelParamName, string axePositionParamName, string stateParamName, int entityId) : base(entityId)
        {
            parameters = new List<string>();
            this.zCoordParamName = zCoordParamName;
            parameters.Add(zCoordParamName);
            this.zVelocityParamName = zVelocityParamName;
            parameters.Add(zVelocityParamName);
            this.rotateVelParamName = rotateVelParamName;
            parameters.Add(rotateVelParamName);
            this.axePositionParamName = axePositionParamName;
            parameters.Add(axePositionParamName);
            this.stateParamName = stateParamName;
            parameters.Add(stateParamName);
        }

        public override IEnumerable<string> GetAllDataParameterNames()
        {
            foreach (var item in parameters)
            {
                yield return item;
            }
        }

        public override void SetParameterValue(string paramName, object value)
        {

            try
            {
                if (paramName == zCoordParamName)
                {
                    ZCoord = (double)value;
                }
                if (paramName == zVelocityParamName)
                {
                    ZVelocity = (double)value;
                }
                if (paramName == rotateVelParamName)
                {
                    RotateVel = (double)value;
                }
                if (paramName == axePositionParamName)
                {
                    AxePosition = (double)value;
                }

                if (paramName == stateParamName)
                {
                    //UnityEngine.Debug.Log($"Parameter {stateParamName} is set to: {(long)value}");
                    State = (long)value;

                }

            }
            catch (Exception e)
            {
                UnityEngine.Debug.Log(e);
            }

         
            
        }

        public override string ToString()
        {
            return $"ZVelocity:{ZVelocity}; ZCoord:{ZCoord}; " +
                $"RotateVel:{RotateVel}; AxePosition{AxePosition}; State{State}.";
        }
    }
}
