﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Assets.Scripts.Interfaces;

namespace Assets.Scripts.DataStructures
{
    public class BucksContainerData : Data
    {

        public long BucksState;
        public long BucksCupState;
        public long InnerContainerState;
        public long InnerContainerCupState;

        private string bucksStateParamName;
        private string bucksCupStateParamName;
        private string innerContrainerStateParamName;
        private string innerContainerCupStateParamName;

        private List<string> parameters;
        
        public BucksContainerData(string bucksStateParamName, string bucksCupStateParamName, string innerContrainerStateParamName, string innerContainerCupStateParamName, int entityId):base(entityId)
        {
            parameters = new List<string>();
            this.bucksStateParamName = bucksStateParamName;
            this.bucksCupStateParamName = bucksCupStateParamName;
            this.innerContrainerStateParamName = innerContrainerStateParamName;
            this.innerContainerCupStateParamName = innerContainerCupStateParamName;
            parameters.Add(bucksStateParamName);
            parameters.Add(bucksCupStateParamName);
            parameters.Add(innerContrainerStateParamName);
            parameters.Add(innerContainerCupStateParamName);
            
        }

        public override IEnumerable<string> GetAllDataParameterNames()
        {
            return parameters;
        }

        public override void SetParameterValue(string paramName, object value)
        {
            if(paramName == bucksCupStateParamName)
            {
                BucksCupState = (long)value;
            }
            if (paramName == bucksStateParamName)
            {
                BucksState = (long)value;
            }
            if (paramName == innerContrainerStateParamName)
            {
                InnerContainerState = (long)value;
            }
            if (paramName == innerContainerCupStateParamName)
            {
                InnerContainerCupState = (long)value;
            }
        }

        public override string ToString()
        {
            return $"BucksState: {BucksState}; BucksCupState: {BucksCupState}; InnerContainerState: {InnerContainerState}; InnerContainerCupState: {InnerContainerCupState};";
        }
    }
}
