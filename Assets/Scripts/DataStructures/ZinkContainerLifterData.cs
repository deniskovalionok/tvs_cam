﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Assets.Scripts.Interfaces;

namespace Assets.Scripts.DataStructures
{
    public class ZinkContainerLifterData : Data
    {
        public double YCoord;
        public double YVelocity;

        private string yCoordParamName;
        private string yVelocityParamName;

        private List<string> parameters;


        public ZinkContainerLifterData(string yCoordParamName, string yVelocityParamName, int entityId):base(entityId)
        {
            parameters = new List<string>();

            this.yVelocityParamName = yVelocityParamName;
            this.yCoordParamName = yCoordParamName;
            parameters.Add(yVelocityParamName);
            parameters.Add(yCoordParamName);
        }

        public override IEnumerable<string> GetAllDataParameterNames()
        {
            foreach (var item in parameters)
            {
                yield return item;
            }
        }

        public override void SetParameterValue(string paramName, object value)
        {
            if(paramName == yCoordParamName)
            {
                YCoord = (double)value;
            }

            if (paramName == yVelocityParamName)
            {
                YVelocity = (double)value;
            }
        }

        public override string ToString()
        {
            return $"YCoord: {YCoord}; YVelocity: {YVelocity};";
        }
    }
}
