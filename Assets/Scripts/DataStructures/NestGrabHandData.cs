﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Assets.Scripts.Interfaces;

namespace Assets.Scripts.DataStructures
{
    public class NestGrabHandData : Data
    {
        public double YCoord;
        public double YVelocity;
        public double GrabState;

        private string yCoordParamName;
        private string yVelocityParamName;
        private string grabStateParamName;
        private List<string> parameters;

        public NestGrabHandData(string yCoordParamName, string yVelocityParamName, string grabStateParamName, int id):base(id)
        {
            parameters = new List<string>();
            this.yCoordParamName = yCoordParamName;
            this.yVelocityParamName = yVelocityParamName;
            this.grabStateParamName = grabStateParamName;
            parameters.Add(yCoordParamName);
            parameters.Add(yVelocityParamName);
            parameters.Add(grabStateParamName);
        }



        public override IEnumerable<string> GetAllDataParameterNames()
        {
            foreach (var item in parameters)
            {
                yield return item;
            }
        }

        public override void SetParameterValue(string paramName, object value)
        {
            if(paramName == yCoordParamName)
            {
                YCoord = (double)value;
            }
            if(paramName == yVelocityParamName)
            {
                YVelocity = (double)value;
            }
            if(paramName == grabStateParamName)
            {
                GrabState = (double)value;
            }
        }

        public override string ToString()
        {
            return $"YVelocity: {this.YVelocity}; YCoord: {this.YCoord}; GrabState: {this.GrabState}";
        }
    }
}
