﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Assets.Scripts.Interfaces;

namespace Assets.Scripts.DataStructures
{
    public class TruckData : Data
    {
        private List<string> parameters;

        public double XCoord;
        public double YCoord;
        public double XVelocity;
        public double YVelocity;
        public double RotVelocity;
        public double AxisState;

        private string xCoordParamName;
        private string yCoordParamName;
        private string xVelocityParamName;
        private string yVelocityParamName;
        private string rotVelocityParamName;
        private string axisStateParamName;

        public TruckData(string xCoordParamName, string yCoordParamName, string xVelocityParamName, string yVelocityParamName, int entityId, string rotVelocityParamName = "", string axisStateParamName = ""): base(entityId)
        {
            parameters = new List<string>();

            this.xCoordParamName = xCoordParamName;
            this.yCoordParamName = yCoordParamName;
            this.xVelocityParamName = xVelocityParamName;
            this.yVelocityParamName = yVelocityParamName;
            this.rotVelocityParamName = rotVelocityParamName;
            this.axisStateParamName = axisStateParamName;
            parameters.Add(xCoordParamName);
            parameters.Add(yCoordParamName);
            parameters.Add(xVelocityParamName);
            parameters.Add(yVelocityParamName);
            parameters.Add(rotVelocityParamName);
            parameters.Add(axisStateParamName);

        }

        public override IEnumerable<string> GetAllDataParameterNames()
        {
            foreach (var item in parameters)
            {
                yield return item;
            }
        }

        public override void SetParameterValue(string paramName, object value)
        {

            if(paramName == xCoordParamName)
            {

                XCoord = (double)value;
            }

            if(paramName == yCoordParamName)
            {
                YCoord = (double)value;
            }

            if(paramName == xVelocityParamName)
            {
                XVelocity = (double)value;
            }

            if(paramName == yVelocityParamName)
            {
                YVelocity = (double)value;
            }
            
            if(paramName == rotVelocityParamName)
            {
                RotVelocity = (double)value;
            }

            if(paramName == axisStateParamName)
            {
                AxisState = (double)value;
            }
        }

        public override string ToString()
        {
            return $"Xcoord: {XCoord}; YCoord: {YCoord}; XVelocity: {XVelocity}; YVelocity: {YVelocity}";
        }
    }
}
