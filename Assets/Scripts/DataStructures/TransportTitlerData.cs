﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Assets.Scripts.Interfaces;

namespace Assets.Scripts.DataStructures
{
    public class TransportTitlerData : Data
    {
        public double ZCoord;
        public double ZVelocity;
        public double XCoord;
        public double XVelocity;
        public double RotateVel;
        public double AxePosition;
        public double GrabRotateVel;
        public double GrabAxePosition;

        private string velocityXParamName;
        private string velocityZParamName;
        private string coordXParamName;
        private string coordZParamName;
        private string rotatingVelParamName;
        private string axePositionParamName;
        private string grabRotatingVelParamName;
        private string grabAxePositionParamName;

        private List<string> parameters;

        public TransportTitlerData(string velocityXParamName, string velocityZParamName,
                                string coordXParamName, string coordZParamName, string rotatingVelParamName, string axePositionParamName,
                                string grabRotatingVelParamName, string grabAxePositionParamName, int entityId) : base(entityId)
        {
            parameters = new List<string>();
            this.velocityXParamName = velocityXParamName;
            parameters.Add(velocityXParamName);
            this.velocityZParamName = velocityZParamName;
            parameters.Add(velocityZParamName);
            this.coordXParamName = coordXParamName;
            parameters.Add(coordXParamName);
            this.coordZParamName = coordZParamName;
            parameters.Add(coordZParamName);
            this.rotatingVelParamName = rotatingVelParamName;
            parameters.Add(rotatingVelParamName);
            this.axePositionParamName = axePositionParamName;
            parameters.Add(axePositionParamName);
            this.grabAxePositionParamName = grabAxePositionParamName;
            parameters.Add(grabAxePositionParamName);
            this.grabRotatingVelParamName = grabRotatingVelParamName;
            parameters.Add(grabRotatingVelParamName);
        }

        public override IEnumerable<string> GetAllDataParameterNames()
        {
            foreach (var item in parameters)
            {
                yield return item;
            }
        }

        public override void SetParameterValue(string paramName, object value)
        {
            if (paramName == velocityXParamName)
            {
                XVelocity = (double)value;
            }
            if (paramName == coordXParamName)
            {
                XCoord = (double)value;
            }
            if (paramName == velocityZParamName)
            {
                ZVelocity = (double)value;
            }
            if (paramName == coordZParamName)
            {
                ZCoord = (double)value;
            }
            if (paramName == rotatingVelParamName)
            {
                RotateVel = (double)value;
            }
            if (paramName == axePositionParamName)
            {
                AxePosition = (double)value;
            }
            if (paramName == grabRotatingVelParamName)
            {
                GrabRotateVel = (double)value;
            }
            if (paramName == grabAxePositionParamName)
            {
                GrabAxePosition = (double)value;
            }
        }
    }
}
