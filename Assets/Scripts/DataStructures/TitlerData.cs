﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Assets.Scripts.Interfaces;

namespace Assets.Scripts.DataStructures
{
    public class TitlerData: Data
    {
        public double RotatingVelocity;
        public double AxePosition;

        private string rotatingVelParamName;
        private string axePositionParamName;


        private List<string> parameters;

        public TitlerData(string rotatingVelParamName, string axePositionParamName, int entityId) : base(entityId)
        {
            parameters = new List<string>();

            this.rotatingVelParamName = rotatingVelParamName;
            parameters.Add(rotatingVelParamName);
            this.axePositionParamName = axePositionParamName;
            parameters.Add(axePositionParamName);
            this.AxePosition = 0;
            this.RotatingVelocity = 0;
        }

        public override IEnumerable<string> GetAllDataParameterNames()
        {
            foreach (var item in parameters)
            {
                yield return item;
            }
        }

        public override void SetParameterValue(string paramName, object value)
        {
            if (paramName == rotatingVelParamName)
            {
                RotatingVelocity = (double)value;
            }
            if (paramName == axePositionParamName)
            {
                AxePosition = (double)value;                                                
            }
        }

        public override string ToString()
        {
            return $"RotatingVelocity:{RotatingVelocity}; AxePosition:{AxePosition}.";
        }

    }
}
