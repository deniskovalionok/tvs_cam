﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Assets.Scripts.Interfaces;

namespace Assets.Scripts.DataStructures
{
    public class RotatingTableData : Data
    {

        public double ZCoord;
        public double Zvelocity;
        public double RotatingVelocity;
        public double AxisPosition;

        private string zCoordParamName;
        private string zVelocityParamName;
        private string rotatingVelocityParamName;
        private string axisPositionParamName;

        private List<string> parameters;

        public RotatingTableData(string zCoordParamName, string zVelocityParamName, string rotatingVelocityParamName, string axisPositionParamName, int entityId):base(entityId)
        {
            parameters = new List<string>();

            this.zCoordParamName = zCoordParamName;
            this.zVelocityParamName = zVelocityParamName;
            this.rotatingVelocityParamName = rotatingVelocityParamName;
            this.axisPositionParamName = axisPositionParamName;

            parameters.Add(zCoordParamName);
            parameters.Add(zVelocityParamName);
            parameters.Add(rotatingVelocityParamName);
            parameters.Add(axisPositionParamName);
        }

        public override IEnumerable<string> GetAllDataParameterNames()
        {
            foreach (var item in parameters)
            {
                yield return item;
            }
        }

        public override void SetParameterValue(string paramName, object value)
        {
            if(paramName == zCoordParamName)
            {
                ZCoord = (double)value;
            }
            if (paramName == zVelocityParamName)
            {
                Zvelocity = (double)value;
            }
            if (paramName == rotatingVelocityParamName)
            {
                RotatingVelocity = (double)value;
            }
            if (paramName == axisPositionParamName)
            {
                AxisPosition = (double)value;
            }
        }
    }
}
