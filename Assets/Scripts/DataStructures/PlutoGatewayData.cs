﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Assets.Scripts.Interfaces;

namespace Assets.Scripts.DataStructures
{
    public class PlutoGatewayData : Data
    {

        public double ZCoord;
        public double ZVelocity;
        public double RotatingVelocity;
        public double AxePosition;

        private string velocityZParamName;
        private string coordZParamName;
        private string rotatingVelParamName;
        private string axePositionParamName;

        private List<string> parameters;

        public PlutoGatewayData(string velocityZParamName, string coordZParamName, string rotatingVelParamName, string axePositionParamName, int entityId) : base(entityId)
        {
            parameters = new List<string>();

            this.velocityZParamName = velocityZParamName;
            parameters.Add(velocityZParamName);
            this.coordZParamName = coordZParamName;
            parameters.Add(coordZParamName);
            this.rotatingVelParamName = rotatingVelParamName;
            parameters.Add(rotatingVelParamName);
            this.axePositionParamName = axePositionParamName;
            parameters.Add(axePositionParamName);
        }

        public override IEnumerable<string> GetAllDataParameterNames()
        {
            foreach (var item in parameters)
            {
                yield return item;
            }
        }

        public override void SetParameterValue(string paramName, object value)
        {
            if (paramName == coordZParamName)
            {
                ZCoord = (double)value;
            }
            if (paramName == velocityZParamName)
            {
                ZVelocity = (double)value;
            }
            if(paramName == rotatingVelParamName)
            {
                RotatingVelocity = (double)value;
            }
            if(paramName == axePositionParamName)
            {
                AxePosition = (double)value;
            }

        }

        public override string ToString()
        {
            return $"ZVelocity:{ZVelocity}; ZCoord:{ZCoord}; " +
                $"RotatingVelocity:{RotatingVelocity}; AxePosition:{AxePosition}.";
        }
    }
}
