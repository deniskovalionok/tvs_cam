﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Assets.Scripts.Interfaces;

namespace Assets.Scripts.DataStructures
{
    public class ManipulatorData : Data
    {
        public double GrabValue;
        public double XVelocity;
        public double XCoord;
        public double YVelocity;
        public double YCoord;
        public double ZVelocity;
        public double ZCoord;

        private string velocityXParamName;
        private string velocityYParamName;
        private string velocityZParamName;
        private string coordXParamName;
        private string coordYParamName;
        private string coordZParamName;
        private string grabValueParamName;

        private List<string> parameters;

        public ManipulatorData(string velocityXParamName, string velocityYParamName, string velocityZParamName,
                                string coordXParamName, string coordYParamName, string coordZParamName, string grabValueParamName, int entityId) : base(entityId)
        {
            parameters = new List<string>();

            this.velocityXParamName = velocityXParamName;
            parameters.Add(velocityXParamName);
            this.velocityYParamName = velocityYParamName;
            parameters.Add(velocityYParamName);
            this.velocityZParamName = velocityZParamName;
            parameters.Add(velocityZParamName);
            this.coordXParamName = coordXParamName;
            parameters.Add(coordXParamName);
            this.coordYParamName = coordYParamName;
            parameters.Add(coordYParamName);
            this.coordZParamName = coordZParamName;
            parameters.Add(coordZParamName);
            this.grabValueParamName = grabValueParamName;
            parameters.Add(grabValueParamName);

            this.XVelocity = 0;
            this.YVelocity = 0;
            this.ZVelocity = 0;
            this.XCoord = 0;
            this.YCoord = 0;
            this.ZCoord = 0;
            this.GrabValue = 0;
        }

        public override string ToString()
        {
            return $"XVelocity:{XVelocity}; XCoord:{XCoord}; YVelocity:{YVelocity}; " +
                $"YCoord:{YCoord}; ZVelocity:{ZVelocity}; ZCoord:{ZCoord}; " +
                $"GrabValue:{GrabValue}.";
        }

       

        public override void SetParameterValue(string paramName, object value)
        {

            //UnityEngine.Debug.Log($"Acquired value: {value}");

            if (paramName == velocityXParamName)
            {
                XVelocity = (double)value;
            }
            if (paramName == coordXParamName)
            {
                XCoord = (double)value;
            }

            if (paramName == velocityYParamName)
            {
                YVelocity = (double)value;
            }
            if (paramName == coordYParamName)
            {
                YCoord = (double)value;
            }

            if (paramName == velocityZParamName)
            {
                ZVelocity = (double)value;
            }
            if (paramName == coordZParamName)
            {
                ZCoord = (double)value;
            }

            if (paramName == grabValueParamName)
            {
                GrabValue = (double)value;
            }
        }

        public override IEnumerable<string> GetAllDataParameterNames()
        {
            foreach (var item in parameters)
            {
                yield return item;
            }
        }
    }
}
