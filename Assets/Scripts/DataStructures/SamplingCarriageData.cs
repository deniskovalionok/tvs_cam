﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Assets.Scripts.Interfaces;

namespace Assets.Scripts.DataStructures
{
    public class SamplingCarriageData : Data
    {

        public double XCoord;
        public double XVelocity;
        public double IntakeYCoord;
        public double IntakeYVelocity;
        public double RodYCoord;
        public double RodYVelocity;

        private string xCoordParamName;
        private string xVelocityParamName;
        private string intakeYCoordParamName;
        private string intakeYVelocityParamName;
        private string rodYCoordParamName;
        private string rodYVelocityParamName;

        private List<string> parameters;

        public SamplingCarriageData(string xCoordParamName, string xVelocityParamName,
           string intakeYCoordParamName, string intakeYVelocityParamName,
           string rodYCoordParamName, string rodYVelocityParamName, int entityId):base(entityId)
        {
            parameters = new List<string>();

            this.xCoordParamName = xCoordParamName;
            this.xVelocityParamName = xVelocityParamName;
            this.intakeYCoordParamName = intakeYCoordParamName;
            this.intakeYVelocityParamName = intakeYVelocityParamName;
            this.rodYCoordParamName = rodYCoordParamName;
            this.rodYVelocityParamName = rodYVelocityParamName;

            parameters.Add(xCoordParamName);
            parameters.Add(xVelocityParamName);
            parameters.Add(intakeYCoordParamName);
            parameters.Add(intakeYVelocityParamName);
            parameters.Add(rodYCoordParamName);
            parameters.Add(rodYVelocityParamName);
        }


        public override IEnumerable<string> GetAllDataParameterNames()
        {
            foreach (var item in parameters)
            {
                yield return item;
            }
        }

        public override void SetParameterValue(string paramName, object value)
        {
            if(paramName == xCoordParamName)
            {
                XCoord = (double)value;
            }
            if (paramName == xVelocityParamName)
            {
                XVelocity = (double)value;
            }
            if (paramName == intakeYCoordParamName)
            {
                IntakeYCoord = (double)value;
            }
            if (paramName == intakeYVelocityParamName)
            {
                IntakeYVelocity = (double)value;
            }
            if (paramName == rodYCoordParamName)
            {
                RodYCoord = (double)value;
            }
            if (paramName == rodYVelocityParamName)
            {
                RodYVelocity = (double)value;
            }
        }
    }
}
