﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Assets.Scripts.Interfaces;

namespace Assets.Scripts.DataStructures
{
    public class DoorData : Data
    {
        public double ZCoord;
        public double ZVelocity;

        private string zCoordParamName;
        private string zVelocityParamName;

        private List<string> parameters;


        public DoorData(string zCoordParamName, string zVelocityParamName, int entityId):base(entityId)
        {
            parameters = new List<string>();

            this.zCoordParamName = zCoordParamName;
            parameters.Add(zCoordParamName);
            this.zVelocityParamName = zVelocityParamName;
            parameters.Add(zVelocityParamName);
        }


        public override IEnumerable<string> GetAllDataParameterNames()
        {
            foreach (var item in parameters)
            {
                yield return item;
            }
        }

        public override void SetParameterValue(string paramName, object value)
        {
            if(paramName == zCoordParamName)
            {
                ZCoord = (double)value;
            }
            if(paramName == zVelocityParamName)
            {
                ZVelocity = (double)value;
            }
        }

        public override string ToString()
        {
            return $"ZCoord:{ZCoord}; ZVelocity:{ZVelocity}.";
        }
    }
}
