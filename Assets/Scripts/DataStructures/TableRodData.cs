﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Assets.Scripts.Interfaces;

namespace Assets.Scripts.DataStructures
{
    public class TableRodData : Data
    {

        public double TableRodZCoord;
        public double TableRodXCoord;
        public double TableRodXVelocity;
        public double TableRodZVelocity;

        private string tableRodZCoordParamName;
        private string tableRodXCoordParamName;
        private string tableRodZVelocityParamName;
        private string tableRodXVelocityParamName;

        private List<string> parameters;

        public TableRodData(string tableRodZCoordParamName, string tableRodXCoordParamName, string tableRodZVelocityParamName, string tableRodXVelocityParamName, int entityData):base(entityData)
        {
            parameters = new List<string>();
            this.tableRodXCoordParamName = tableRodXCoordParamName;
            this.tableRodZCoordParamName = tableRodZCoordParamName;
            this.tableRodXVelocityParamName = tableRodXVelocityParamName;
            this.tableRodZVelocityParamName = tableRodZVelocityParamName;
            parameters.Add(tableRodXCoordParamName);
            parameters.Add(tableRodZCoordParamName);
            parameters.Add(tableRodXVelocityParamName);
            parameters.Add(tableRodZVelocityParamName);
        }


        public override IEnumerable<string> GetAllDataParameterNames()
        {
            foreach (var param in parameters)
            {
                yield return param;
            }
        }

        public override void SetParameterValue(string paramName, object value)
        {
            if(paramName == tableRodXCoordParamName)
            {
                TableRodXCoord = (double)value;
            }
            if (paramName == tableRodZCoordParamName)
            {
                TableRodZCoord = (double)value;
            }
            if (paramName == tableRodXVelocityParamName)
            {
                TableRodXVelocity = (double)value;
            }
            if (paramName == tableRodZVelocityParamName)
            {
                TableRodZVelocity = (double)value;
            }
        }
    }
}
