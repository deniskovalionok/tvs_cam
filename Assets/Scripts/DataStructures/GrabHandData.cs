﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Assets.Scripts.Interfaces;

namespace Assets.Scripts.DataStructures
{
    public class GrabHandData : Data
    {

        public double RodXCoord;
        public double RodZCoord;
        public double RodXVelocity;
        public double RodZVelocity;
        public double RodGrabState;

        private string rodXCoordParamName;
        private string rodZCoordParamName;
        private string rodXVelocityParamName;
        private string rodZVelocityParamName;
        private string rodGrabStateParamName;

        private List<string> parameters;

        public GrabHandData(string rodXCoordParamName, string rodZCoordParamName, string rodXVelocityParamName,
            string rodZVelocityParamName, string rodGrabStateParamName, int entityId):base(entityId)
        {
            parameters = new List<string>();

            this.rodXCoordParamName = rodXCoordParamName;
            this.rodZCoordParamName = rodZCoordParamName;
            this.rodXVelocityParamName = rodXVelocityParamName;
            this.rodZVelocityParamName = rodZVelocityParamName;
            this.rodGrabStateParamName = rodGrabStateParamName;

            parameters.Add(rodXCoordParamName);
            parameters.Add(rodZCoordParamName);
            parameters.Add(rodXVelocityParamName);
            parameters.Add(rodZVelocityParamName);
            parameters.Add(rodGrabStateParamName);
        }

        public override IEnumerable<string> GetAllDataParameterNames()
        {
            foreach (var item in parameters)
            {
                yield return item;
            }
        }

        public override void SetParameterValue(string paramName, object value)
        {
            if(paramName == rodXVelocityParamName)
            {
                RodXVelocity = (double)value;
            }
            if (paramName == rodZVelocityParamName)
            {
                RodZVelocity = (double)value;
            }
            if (paramName == rodXCoordParamName)
            {
                RodXCoord = (double)value;
            }
            if (paramName == rodZCoordParamName)
            {
                RodZCoord = (double)value;
            }
            if (paramName == rodGrabStateParamName)
            {
                RodGrabState = (double)value;
            }
        }

        public override string ToString()
        {
            return $"RodXCoord: {RodXCoord}; RodZCoord: {RodZCoord}; RodXVelocity: {RodXVelocity}; RodZVelocity: {RodZVelocity}; RodGrabState: {RodGrabState}.";
        }
    }
}
