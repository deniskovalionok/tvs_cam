﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Assets.Scripts.Interfaces;

namespace Assets.Scripts.DataStructures
{
    public class ClampsData : Data
    {
        public double AxePosition;
        public double RotateVelocity;

        private string axePositionParamName;
        private string rotateVelocityParamName;

        private List<string> parameters;

        public ClampsData(string axePositionParamName, string rotateVelocityParamName, int id): base(id)
        {
            parameters = new List<string>();
            this.axePositionParamName = axePositionParamName;
            this.rotateVelocityParamName = rotateVelocityParamName;
            parameters.Add(axePositionParamName);
            parameters.Add(rotateVelocityParamName);
        }

        public override IEnumerable<string> GetAllDataParameterNames()
        {
            foreach (var item in parameters)
            {
                yield return item;
            }
        }

        public override void SetParameterValue(string paramName, object value)
        {
            if(paramName == axePositionParamName)
            {
                AxePosition = (double)value;
            }
            if(paramName == rotateVelocityParamName)
            {
                RotateVelocity = (double)value;
            }
        }

        public override string ToString()
        {
            return $"AxePosition: {AxePosition}; RotateVelocity: {RotateVelocity}";
        }
    }
}
