﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Assets.Scripts.Interfaces;

namespace Assets.Scripts.DataStructures
{
    public class AnimationData : Data
    {

        public double AnimationPosition;
        public double AnimationSpeed;

        private string animPositionName;
        private string animSpeedName;

        private List<string> parameterNames;

        public AnimationData(string animPositionName, string animSpeedName, int entiyId):base(entiyId)
        {
            parameterNames = new List<string>();

            this.animPositionName = animPositionName;
            this.animSpeedName = animSpeedName;

            parameterNames.Add(this.animSpeedName);
            parameterNames.Add(this.animPositionName);

        }

        public override IEnumerable<string> GetAllDataParameterNames()
        {
            return parameterNames;
        }

        public override void SetParameterValue(string paramName, object value)
        {
            if(paramName == animPositionName)
            {
                AnimationPosition = (double)value;
            }

            if(paramName == animSpeedName)
            {
                AnimationSpeed = (double)value;
            }
        }
    }
}
