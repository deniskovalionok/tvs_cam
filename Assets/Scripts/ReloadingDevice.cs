﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using Assets.Scripts.DataStructures;
using Assets.Scripts.Interfaces;
using System.Collections;

namespace Assets.Scripts
{
    public class ReloadingDevice : MonoBehaviour, IController
    {

        private float previousRealtime;
        private float reloadingOriginZ;
        private DoorData settedData;
        public Transform ReloadingObj;

        private void Start()
        {
            previousRealtime = Time.realtimeSinceStartup;
            reloadingOriginZ = ReloadingObj.localPosition.z;

            //_prData = new DoorData("", "", -1);
            //SetData(_prData);
            //DataUpdatedHandler();
            //UpdateControllingParameters();
            //StartCoroutine(Testing());
        }


        private DoorData _prData;

        private IEnumerator Testing()
        {
            _prData.ZVelocity = 20;            
            while (_prData.ZCoord < 2300)
            {
                _prData.ZCoord += _prData.ZVelocity * 0.1;                
                DataUpdatedHandler();
                yield return new WaitForSeconds(0.1f);
            }
        }


        private void Update()
        {
            //UpdateControllingParameters();
        }

        public void DataUpdatedHandler()
        {
            previousRealtime = Time.realtimeSinceStartup;
        }

        public void SetData(Data dataToSet)
        {
            settedData = (DoorData)dataToSet;
        }

        public void UpdateControllingParameters()
        {
            float currentTime = Time.realtimeSinceStartup;
            Transporting(currentTime);
            previousRealtime = currentTime;
        }

        private void Transporting(float currentTime)
        {
            if(GameCore.Instance.IsLerp)
            {
                ReloadingObj.localPosition = new Vector3(ReloadingObj.localPosition.x, ReloadingObj.localPosition.y, Mathf.Lerp(ReloadingObj.localPosition.z, reloadingOriginZ + (float)settedData.ZCoord / 1000, Time.deltaTime));
            }
            else
            {
                ReloadingObj.localPosition = new Vector3(ReloadingObj.localPosition.x, ReloadingObj.localPosition.y,
                    Mathf.MoveTowards(ReloadingObj.localPosition.z, reloadingOriginZ + (float)settedData.ZCoord / 1000, Mathf.Abs((float)settedData.ZVelocity / 1000) * (currentTime - previousRealtime)));
            }

            if(settedData.ZVelocity == 0)
            {
                ReloadingObj.localPosition = new Vector3(ReloadingObj.localPosition.x, ReloadingObj.localPosition.y, reloadingOriginZ + (float)settedData.ZCoord / 1000);
            }
        }
    }
}
