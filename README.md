# Unity-клиент - эмуляция промышленного телевидения с IP-камер.

## Цель данного репозитория

Данный репозиторий представляет собой часть проекта, предназначенного для видеоотображения физической симуляции реальных процессов на примере процессов, предоставленных компанией Росатом.
Использована версия Unity 2018.4.13f1.

## Общее описание процесса работы проекта и место Unity-клиента в этом проекте

Enicad-модель регистрирует opc-сервер на целевой машине, регистрирует на этом сервере переменные и изменяет их значения по протоколу OPC-DA.
Python-сервер получает значения переменных от OPC-сервера по протоколу OPC-DA и передает их по WebSocket в Unity, где значения параметров визуализируются на 3D-моделях.
В данном репозитории представлен только Unity-клиент.

## Пример описания нового этапа

1. Описать реализацию абстрактного класса Data для каждого контроллера (Пример: DoorData)

~~~

    //Идентификатор, связывающий объект-данные с контроллером
    public ConcreteData(int entityId):base(entityId)
    
    //Изменяемый параметр
    public float Velocity;
    //Имя параметра, передаваемого Python-сервером (имена параметров описываются на стороне Python-сервера)
    private string ParameterName

    public override IEnumerable<string> GetAllDataParameterNames()
    {
        //Вернуть перечислитель всех имен параметров
        return new List<string>{ParameterName};
    }

    public override void SetParameterValue(string paramName, object value)
    {
  	    //По имени параметра найти нужное поле
	    if(ParameterName == paramName)
	    {
	        Velocity = (float)value;
	    }
    }
        	
~~~

2. Описать изменение Transform-а объекта в соответствии с приходящими данными, реализовав IController (Пример: Door)

~~~

    public void SetData(Data dataToSet)
    {
        //Передача контроллеру объекта, отвечающего за изменение данных
        DoorData data = dataToSet as DoorData;
    }

    public void UpdateControllingParameters()
    {
        //Вызов изменения Transform-а
    }

    public void DataUpdatedHandler()
    {
        //Обработчик, вызываемый каждый раз, когда приходят новые данные
    }
~~~

3. Описать реализацию IDataProvider, отвечающего за передачу всех объектов-данных (Пример: PowderStorageBoxDataHandler)

~~~
    public IEnumerable<Data> GetDataObjects()
    {
        //Обеспечивает передачу всех созданных на этапе объектов-данных указанным контроллерам
        ConcreteData data = new ConcreteData();
        return new List<Data>{data};
    }
~~~

4. Описать реализацию абстрактного класса RefsHandler (Пример: PowderStorageBoxObjRefsHandler)

~~~
    public override IController GetControllerByEntityID(int entityID)
    {
        //Вернуть контроллер, ассоциированный с полученным ID
    }

    public override IEnumerable<IController> GetControllers()
    {
        //Обеспечивает передачу всех контроллеров, используемых на этапе
    }
~~~

5. Передать в конструктор класса DataStructuresHandler необходимые параметры (В методе Start() класса GameCore)

~~~
...
    DataStructuresHandler dataHandler = new DataStructuresHandler(<ссылка на реализацию RefsHandler>, <ссылка на реализацию IDataProvider>);
...

    private void Update()
    {
        if (IsFirstDataSetted)
        {
            //Вызов изменения Transform-а на контроллерах
            <ссылка на реализацию RefsHandler>.UpdateControllingObjectsPositions();
        }

    }
~~~

## Настройки подключения по WebSocket

Файл config.cfg

## После приема нового участка в работу необходимо создать новую ветку и продолжать работу в ней